package com.service.repository.user;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.user.User;

@Repository
public interface UserDao extends JpaRepository<User, Integer> {
	
	@Query(value = "SELECT * FROM USERS AS U WHERE U.USER_NAME=?1 ", nativeQuery = true)
	public User getUser(String userName);
	
}