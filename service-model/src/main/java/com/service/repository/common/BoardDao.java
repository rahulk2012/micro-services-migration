package com.service.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.model.common.Board;

@Repository
public interface BoardDao extends JpaRepository<Board, Integer> {

}
