package com.service.repository.common;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.common.Events;

@Repository
public interface EventsDao extends JpaRepository<Events, Integer> {
	@Query(value = "SELECT * FROM EVENTS WHERE VENDOR_ID=?1", nativeQuery = true)
	public List<Events> getEvendsByVendorId(String vendorId);
	
	@Query(value = "SELECT * FROM EVENTS WHERE EVENT_ID=?1", nativeQuery = true)
	public Events getEvendId(String vendorId);
}