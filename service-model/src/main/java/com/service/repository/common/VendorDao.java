package com.service.repository.common;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.common.Vendor;

@Repository
public interface VendorDao extends JpaRepository<Vendor, Integer> {
	@Query(value = "SELECT * FROM VENDOR WHERE VENDOR_ID=?1", nativeQuery = true)
	public Vendor getVendorById(String vendorId);
	
}