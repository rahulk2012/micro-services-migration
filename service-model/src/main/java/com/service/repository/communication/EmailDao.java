package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.model.communication.Email;

@Repository
public interface EmailDao extends JpaRepository<Email, Integer> {

}