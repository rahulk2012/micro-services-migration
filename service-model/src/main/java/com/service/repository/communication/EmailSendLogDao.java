package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.model.communication.EmailSendLog;

@Repository
public interface EmailSendLogDao extends JpaRepository<EmailSendLog, Integer>{
	
}