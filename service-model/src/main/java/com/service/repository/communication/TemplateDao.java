package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.Template;

@Repository
public interface TemplateDao extends JpaRepository<Template, Integer> {
	@Query(value = "SELECT * FROM TEMPLATE WHERE TEMPLATE_ID=?1", nativeQuery = true)
	public Template getTemplateById(String templateId);
}