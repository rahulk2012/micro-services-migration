package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.CommunicationSettings;

@Repository
public interface CommunicationSettingsDao extends JpaRepository<CommunicationSettings, Integer> {
	
	@Query(value = "SELECT * FROM `COMMUNICATION_SETTINGS` WHERE VENDOR_ID=?1", nativeQuery = true)
	public CommunicationSettings findByVendorId(String vendorId);

}