package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.Crons;

@Repository
public interface CronsDao extends JpaRepository<Crons, Integer> {
	
	@Query(value = "SELECT * FROM CRONS WHERE ID=?1", nativeQuery = true)
	public Crons findByCId(String Id);
	
}