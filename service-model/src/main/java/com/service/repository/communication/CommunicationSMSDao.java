package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.model.communication.CommunicationSMS;

@Repository
public interface CommunicationSMSDao extends JpaRepository<CommunicationSMS, Integer> {

}