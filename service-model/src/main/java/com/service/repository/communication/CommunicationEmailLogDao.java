package com.service.repository.communication;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.CommunicationEmailLog;

@Repository
public interface CommunicationEmailLogDao extends JpaRepository<CommunicationEmailLog, Integer> {

	@Query(value = "SELECT * FROM COMMUNICATION_EMAIL_LOG WHERE ID=?1", nativeQuery = true)
	public CommunicationEmailLog findByEId(String Id);
	
	@Query(value="SELECT * FROM `COMMUNICATION_EMAIL_LOG` WHERE VENDOR_ID=?1 AND EVENT_ID=?2 AND IS_MAIL_SEND='N' ", nativeQuery = true)
	public List<CommunicationEmailLog> emailSendList(String vendorId, String eventId);
	
	@Query(value="SELECT DISTINCT(VENDOR_ID) FROM `COMMUNICATION_EMAIL_LOG` WHERE IS_MAIL_SEND='N'", nativeQuery= true)
	public List<Object> distinctVendorList(); 
	
	@Query(value="SELECT DISTINCT(EVENT_ID) FROM `COMMUNICATION_EMAIL_LOG` WHERE VENDOR_ID=?1 AND IS_MAIL_SEND='N'", nativeQuery= true)
	public List<Object> distinctEventList(String vendorId); 
}