package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.CommunicationChannel;

@Repository
public interface CommunicationChanelDao extends JpaRepository<CommunicationChannel, Integer> {

	@Query(value = "SELECT * FROM COMMUNICATION_CHANNEL  WHERE CHANNEL_ID=?1 ", nativeQuery = true)
	public CommunicationChannel findChannelId(String channelId);
}