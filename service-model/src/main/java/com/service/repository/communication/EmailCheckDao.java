package com.service.repository.communication;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.EmailCheck;

@Repository
public interface EmailCheckDao extends JpaRepository<EmailCheck, Integer> {
	@Query(value = "SELECT * FROM EMAIL_CHECK ORDER BY ID DESC", nativeQuery = true)
	public List<EmailCheck> getAllEmailCheckList();

	@Query(value = "SELECT * FROM EMAIL_CHECK WHERE EMAIL=?1", nativeQuery = true)
	public EmailCheck findByEmailId(String emailId);
}
