package com.service.repository.communication;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.communication.Crons;
import com.service.model.communication.CronsExecution;

@Repository
public interface CronsExecutionDao extends JpaRepository<CronsExecution, Integer> {
	@Query(value = "SELECT * FROM CRONS_EXECUTION WHERE ID=?1", nativeQuery = true)
	public CronsExecution findByCEId(String Id);
	
	@Query(value = "SELECT * FROM CRONS_EXECUTION WHERE CRON_ID=?1", nativeQuery = true)
	public CronsExecution findByCronId(String Id);
	
}