package com.service.repository.fs;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.model.fs.Answers;


@Repository
public interface AnswerRepository extends JpaRepository<Answers, Integer> {
	
	
	
}