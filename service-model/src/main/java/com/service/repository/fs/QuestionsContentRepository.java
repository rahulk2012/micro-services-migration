package com.service.repository.fs;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.service.model.fs.QuestionsContent;


@Repository
public interface QuestionsContentRepository extends JpaRepository<QuestionsContent, Integer> {
	
	@Query(value = "SELECT * FROM QUESTION_CONTENT WHERE QUESTION_ID = ?1", nativeQuery = true)
	public List<QuestionsContent> getQuestionContentList(Integer questionId);
	
	@Modifying
	@Transactional
	@Query(value="DELETE FROM `QUESTION_CONTENT` WHERE QUESTION_ID =?1",nativeQuery=true)
	public void deleteQuestionContentData(Integer questionId);
	
}