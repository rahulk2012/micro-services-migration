package com.service.repository.fs;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.fs.FeedbackDetails;


@Repository
public interface FeedbackDetailsRepository extends JpaRepository<FeedbackDetails, Integer> {
	
	
	@Query(value = "SELECT * FROM FEEDBACK_DETAILS WHERE VENDOR_ID = ?1", nativeQuery = true)
	public List<FeedbackDetails> getAllFeedbackOnVendorIdList(Integer vendorId);
	
	@Query(value = "SELECT * FROM `FEEDBACK_DETAILS` WHERE ID=?1", nativeQuery = true)
	public FeedbackDetails getFeedbackList(String id);
}