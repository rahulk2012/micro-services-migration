package com.service.repository.fs;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.service.model.fs.Elements;


@Repository
public interface ElementsRepository extends JpaRepository<Elements, Integer> {
	
	
	
}