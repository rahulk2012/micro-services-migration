package com.service.repository.fs;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.service.model.fs.Questions;


@Repository
public interface QuestionsRepository extends JpaRepository<Questions, Integer> {
	
	@Query(value = "SELECT QS.ID, ET.EVENT_NAME, QS.QUESTION, EL.ELEMENT, QS.MENDETORY FROM QUESTIONS QS "
			+ " INNER JOIN EVENTTYPE ET ON (ET.ID = QS.EVENT_ID AND ET.ACTIVATED=1 AND ET.DELETED=0) " 
			+ " INNER JOIN ELEMENTS EL ON (EL.ID = QS.ELEMENT_ID AND EL.ACTIVATED=1 AND EL.DELETED=0) "  
			+ " WHERE ET.EVENT_NAME = ?1", nativeQuery = true)
	public List<Object[]> getQuestionList(String eventType);
	
	@Query(value = "SELECT * FROM QUESTIONS WHERE ACTIVATED=1 AND  DELETED=0 AND VENDOR_ID=?1 AND EVENT_ID=?2 LIMIT ?3 , ?4", nativeQuery = true)
	public List<Questions> getALLActiveQuestionsLimit(String vendorId, String eventId, Integer startLimit, Integer endLimit);
	
	@Query(value = "SELECT * FROM QUESTIONS WHERE ACTIVATED=1 AND  DELETED=0 AND VENDOR_ID=?1 AND EVENT_ID=?2", nativeQuery = true)
	public List<Questions> getALLActiveQuestions(String vendorId, String eventId);
	
}