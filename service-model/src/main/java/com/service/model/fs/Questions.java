package com.service.model.fs;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "QUESTIONS")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@DynamicUpdate
public class Questions implements Serializable {

	private static final long serialVersionUID = -6910749550646281736L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID")
	private Integer ID;

	@Column(name = "VENDOR_ID")
	private String vendorId;
	
	@Column(name = "EVENT_ID")
	private String eventId;
	
	@Column(name = "QUESTION")
	private String question;

	@Column(name = "ELEMENT_ID")
	private Integer elementId;
	
	@Column(name = "QUESTION_TYPE")
	private Integer questionType;
	
	@Column(name = "MENDETORY")
	private Integer mendetory;
	
	@Column(name = "PARENT_ID")
	private Integer parentId;
	
	@Column(name = "EMOJI_CLASS")
	private String emojiClass;
	
	@Column(name = "ACTIVATED")
	private Integer activated;

	@Column(name = "DELETED")
	private Integer deleted;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;

}
