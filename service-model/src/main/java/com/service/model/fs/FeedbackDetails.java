package com.service.model.fs;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "FEEDBACK_DETAILS")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
@DynamicUpdate
public class FeedbackDetails implements Serializable {

	private static final long serialVersionUID = -6281416529079368410L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;
	
	@Column(name = "VENDOR_ID")
	private String vendorId;
	
	@Column(name = "EVENT_ID")
	private String eventId;
	
	@Column(name = "EMAIL_ID")
	private String emailId;

	@Column(name = "IS_MAIL_SEND")
	private String isMailSend;
	
	@Column(name ="MAIL_SEND_DATE")
	private Date mailSendDate;

	@Column(name = "IS_CLICKED")
	private String isClicked;
	
	@Column(name = "CLICKED_DATE")
	private Date isClickedDate;
	
	@Column(name = "IS_REPLIED")
	private String isReplied;
	
	@Column(name = "REPLY_DATE")
	private Date replyDate;

	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name = "STATUS")
	private String status;

}
