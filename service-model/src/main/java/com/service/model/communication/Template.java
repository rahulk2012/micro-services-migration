package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "TEMPLATE")
@ToString
@DynamicUpdate
@Setter
@Getter
public class Template implements Serializable {
	private static final long serialVersionUID = -8189548669588552714L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "TEMPLATE_ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String templateId;

	@Column(name = "TEMPLATE_TYPE", columnDefinition = "INT(1) NOT NULL")
	private Integer templateType;

	@Column(name = "VENDOR_ID", columnDefinition = "VARCHAR(36) NOT NULL")
	private String vendorId;
	
	@Column(name = "EVENT_ID", columnDefinition = "VARCHAR(36) NOT NULL")
	private String eventId;

	@Column(name = "TEMPLATE_CONTENT", columnDefinition = "VARCHAR(4000) NOT NULL")
	private String templateContent;
	
	@Column(name = "TEMPLATE_SUBJECT", columnDefinition = "VARCHAR(200) NOT NULL")
	private String templateSubject;

	@Column(name = "CREATED_DATE", columnDefinition = "DATETIME NOT NULL")
	private Date createdDate;

	@Column(name = "UPDATED_DATE", columnDefinition = "DATETIME NOT NULL")
	private Date updatedDate;

	@Column(name = "ACTIVE", columnDefinition = "VARCHAR(1) NOT NULL DEFAULT 'Y'")
	private String active;

	@Column(name = "DELETED", columnDefinition = "VARCHAR(1) NOT NULL DEFAULT 'N'")
	private String deleted;
}