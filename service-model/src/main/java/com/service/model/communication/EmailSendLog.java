package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "EMAIL_SEND_LOG")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@DynamicUpdate
public class EmailSendLog implements Serializable {

	private static final long serialVersionUID = -3487534472189981352L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;

	@Column(name = "TO_MAIL")
	private String toMail;
	
	@Column(name = "CC_MAIL")
	private String ccMail;

	@Column(name = "BCC_MAIL")
	private String bccMail;

	@Column(name = "MAIL_SUBJECT")
	private String mailSubject;
    
	@Column(name = "ATTACHEMENTS")
	private String attachments;
	
	@Column(name = "SEND_MAIL_VIA")
	private String sendMailVia;
	
	@Column(name = "CREATED_AT")
	private Date createdAt;
	
}