package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "CRONS_EXECUTION")
@ToString
@DynamicUpdate
@Setter
@Getter
public class CronsExecution implements Serializable {

	private static final long serialVersionUID = -8567322615629070758L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;
	
	@Column(name = "CRON_ID")
	private String cronId;

	@Column(name = "CRON_MANUALLY")
	private String cronManually;
	
	@Column(name = "START_DATE_TIME")
	private Date startDateTime;
	
	@Column(name = "END_DATE_TIME")
	private Date endDateTime;
	
}