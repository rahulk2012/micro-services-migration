package com.service.model.communication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "COMMUNICATION_SETTINGS")
@ToString
@DynamicUpdate
@Setter
@Getter
public class CommunicationSettings implements Serializable {

	private static final long serialVersionUID = -640831895694555537L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "SETTING_ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String SETTING_ID;

	@Column(name = "VENDOR_ID")
	private String vendorId;
	
	@Column(name = "CHANNEL_ID")
	private String channelId;
	
	@Column(name = "SEND_BY")
	private String sendBy;
	
	@Column(name = "SEND_ALIAS")
	private String sendAlias;
	
	@Column(name = "MAIL_BYPASS")
	private String mailBypass;
	
	@Column(name = "MAIL_VERIFY_BY_THIRD_PARTY")
	private String mailVerifyByThirdParty;
	
	@Column(name = "BASE_PATH_UI")
	private String basePathUI;
}