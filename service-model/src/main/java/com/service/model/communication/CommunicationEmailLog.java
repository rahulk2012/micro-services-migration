package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "COMMUNICATION_EMAIL_LOG")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@DynamicUpdate
public class CommunicationEmailLog implements Serializable {

	private static final long serialVersionUID = 4012896848098111171L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;

	@Column(name = "SEND_TO")
	private String sendTo;
	
	@Column(name = "SEND_TO_CC")
	private String sendToCC;

	@Column(name = "SEND_TO_BCC")
	private String sendToBCC;

	@Column(name = "VENDOR_ID")
	private String vendorId;
    
	@Column(name = "EVENT_ID")
	private String eventId;
	
	@Column(name = "ENCRYPTED_ID")
	private String encryptedId;
	
	@Column(name = "IS_MAIL_SEND")
	private String isMailSend;
	
	@Column(name = "MAIL_SEND_DATE")
	private Date mailSendDate;
	
	@Column(name = "CREATED_DATE")
	private Date createdDate;
	
	@Column(name = "UPDATED_DATE")
	private Date updatedDate;
	
	@Column(name = "API_CALL")
	private String apiCall;
	
	@Column(name = "API_CALL_RESPONSE")
	private String apiCallResponse;
	
}