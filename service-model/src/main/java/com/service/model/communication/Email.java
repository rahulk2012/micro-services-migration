package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "EMAIL")
@ToString
@DynamicUpdate
@Setter
@Getter
public class Email implements Serializable {

	private static final long serialVersionUID = 1898860149865709140L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;
	
	@Column(name = "TYPE")
	private String type;
	
	@Column(name = "LABEL")
	private String label;

	@Column(name = "REPLIED_MAIL_ID")
	private String repliedMailId;
	
	@Column(name = "TO_ID")
	private String toId;
	
	@Column(name = "FROM_ID")
	private String fromId;
	
	@Column(name = "BCC")
	private String bccId;
	
	@Column(name = "CC")
	private String ccId;
	
	@Column(name = "SUBJECT")
	private String subject;
	
	@Column(name = "MSG")
	private String msg;
	
	@Column(name = "READ_STATUS")
	private Integer readStatus;
	
	@Column(name = "REPLIED_STATUS")
	private Integer repliedStatus;
	
	@Column(name = "SENDER_DEL_STATUS")
	private Integer senderDelStatus;
	
	@Column(name = "GROUP_MAIL_ID")
	private String groupMailId;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SENT_DATE")
	private Date sentDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "READ_DATE")
	private Date readDate;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "SENDER_DEL_DATE")
	private Date senderDelDate;
	
	@Column(name = "RECIVER_DEL_STATUS")
	private Integer reciverDelStatus;
	
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "RECIVER_DEL_DATE")
	private Date reciverDelDate;

	@Column(name = "DRAFT_STATUS")
	private Integer draftStatus;
	
	@Column(name = "IS_DELETED")
	private Integer isDeleted;
}