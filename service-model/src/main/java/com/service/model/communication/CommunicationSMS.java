package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "COMMUNICATION_SMS")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@DynamicUpdate
public class CommunicationSMS implements Serializable {

	private static final long serialVersionUID = -1993962662089338074L;
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;

	@Column(name = "MODULE")
	private String module;
	
	@Column(name = "CONTENT")
	private String content;

	@Column(name = "ISD_CODE")
	private String isdCode;

	@Column(name = "MOBILE_NUMBER")
	private String mobileNumber;
    
	@Column(name = "SENDING_VIA")
	private String sendingVia;
	
	@Column(name = "SEND_AT")
	private Date sendAt;
	
	@Column(name = "REQUEST")
	private String request;
	
	@Column(name = "RESPONSE")
	private String response;
}