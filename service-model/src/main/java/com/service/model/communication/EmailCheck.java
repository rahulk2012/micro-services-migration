package com.service.model.communication;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "EMAIL_CHECK")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class EmailCheck implements Serializable {
	private static final long serialVersionUID = 5919245864728210896L;
	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String id;

	@Column(name = "EMAIL")
	private String email;

	@Column(name = "IS_VERIFIED")
	private Integer isVerified;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "CHECKED_DATE")
	private Date checkeDate;
}