package com.service.model.communication;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "COMMUNICATION_CHANNEL")
@ToString
@AllArgsConstructor
@NoArgsConstructor
@Builder
@Setter
@Getter
public class CommunicationChannel implements Serializable {
	
	private static final long serialVersionUID = 4225533442412182300L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "CHANNEL_ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String channelId;

	@Column(name = "CHANNEL_NAME")
	private String channelName;

	@Column(name = "CHANNEL_SMTP")
	private String channelSmtp;
	
	@Column(name = "CHANNEL_PORT")
	private String channelPort;
	
	@Column(name = "CHANNEL_USER")
	private String channelUser;
	
	@Column(name = "CHANNEL_PASSWORD")
	private String channelPassword;
}