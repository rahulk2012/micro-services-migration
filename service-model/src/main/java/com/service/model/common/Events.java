package com.service.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "EVENTS")
@ToString
@DynamicUpdate
@Setter
@Getter
public class Events implements Serializable {

	private static final long serialVersionUID = 5091144709763958450L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "EVENT_ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String eventId;
	
	@Column(name = "VENDOR_ID")
	private String vendorId;
	
	@Column(name = "EVENT_NAME")
	private String eventName;
	
	@Column(name = "EVENT_DESCRIPTION", columnDefinition = "VARCHAR(500)")
	private String eventDescription;
}