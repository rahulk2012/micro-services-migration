package com.service.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Index;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "STATES", indexes = { @Index(name = "COUNTRY_ID", columnList = "COUNTRY_ID", unique = false)})
@ToString
@DynamicUpdate
@Setter
@Getter
public class State implements Serializable {

	private static final long serialVersionUID = 7317672318622345651L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "INT(10)")
	private Integer stateId;

	@Column(name = "NAME", columnDefinition = "VARCHAR(30) NOT NULL")
	private String stateName;

	@Column(name = "COUNTRY_ID", columnDefinition = "INT(3) NOT NULL DEFAULT 1")
	private Integer countryId;
}