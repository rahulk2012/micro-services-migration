package com.service.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "CITIES")
@ToString
@DynamicUpdate
@Setter
@Getter
public class City implements Serializable {

	private static final long serialVersionUID = -1057056312387683596L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "INT(10)")
	private Integer cityId;

	@Column(name = "NAME", columnDefinition = "VARCHAR(30) NOT NULL")
	private String cityName;

	@Column(name = "STATE_ID", columnDefinition = "INT(10) NOT NULL")
	private Integer stateId;
}