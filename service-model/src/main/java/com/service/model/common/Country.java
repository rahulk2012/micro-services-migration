package com.service.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "COUNTRIES")
@DynamicUpdate
@ToString
@Getter
@Setter
public class Country implements Serializable {

	private static final long serialVersionUID = 7616383436150779090L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "INT(3)")
	private Integer countryId;

	@Column(name = "SORTNAME", columnDefinition = "VARCHAR(3) NOT NULL")
	private String countryCode;

	@Column(name = "NAME", columnDefinition = "VARCHAR(150) NOT NULL")
	private String countryName;

	@Column(name = "NATIONALITY", columnDefinition = "VARCHAR(150) DEFAULT NULL")
	private String countryNationality;

	@Column(name = "DIAL_CODE", columnDefinition = "VARCHAR(20) DEFAULT NULL")
	private String DialCode;
}
