package com.service.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;
import org.hibernate.annotations.GenericGenerator;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "VENDOR")
@ToString
@DynamicUpdate
@Setter
@Getter
public class Vendor implements Serializable {
	
	private static final long serialVersionUID = -6561565376876046781L;

	@Id
	@GeneratedValue(generator = "UUID")
	@GenericGenerator(
			name = "UUID",
			strategy = "org.hibernate.id.UUIDGenerator"
			)
	@Column(name = "VENDOR_ID", updatable = false, nullable = false, columnDefinition = "VARCHAR(36)")
	private String vendorId;

	@Column(name = "VENDOR_NAME")
	private String vendorName;
}