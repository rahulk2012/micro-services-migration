package com.service.model.common;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.DynamicUpdate;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "BOARD")
@ToString
@DynamicUpdate
@Setter
@Getter
public class Board implements Serializable {
	
	private static final long serialVersionUID = -5687824189613688918L;
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ID", columnDefinition = "INT(1)")
	private Integer boardId;

	@Column(name = "BOARD_NAME", columnDefinition = "VARCHAR(50) NOT NULL")
	private String BoardName;

}
