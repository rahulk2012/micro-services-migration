//package com.service.service.user;
//
//import java.util.Optional;
//
//import org.hibernate.service.spi.ServiceException;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.stereotype.Service;
//import org.springframework.transaction.annotation.Transactional;
//
//import com.service.constant.ApiConstant;
//import com.service.model.user.User;
//import com.service.repository.user.UserDao;
//
//import lombok.extern.slf4j.Slf4j;
//
//@Service
//@Slf4j
//public class UserServiceImpl implements UserService {
//	
//	@Autowired
//	private UserDao userDao;
//
//	@Override
//	@Transactional(rollbackFor = ServiceException.class)
//	public User save(User user) {
//		try {
//			return userDao.saveAndFlush(user);
//		} catch (final Exception exception) {
//			throw new ServiceException(ApiConstant.SERVICE_EXCEPTION.getValue(), exception);
//		}
//	}
//
//	@Override
//	public User getUser(Integer id) {
//		Optional<User> user = userDao.findById(id);
//		if(user.isPresent()) {
//			return user.get();
//		}
//		return null;
//	}
//	
//	@Override
//	@Transactional(readOnly = true)
//	public User getUser(String userName) {
//		try {
//			return userDao.getUser(userName);
//		} catch (final ServiceException exception) {
//			log.info("getUserMbileNumber(final String mobileNumber) :: ", exception);
//		}
//		return null;
//	}
//}