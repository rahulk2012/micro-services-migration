package com.service.service.common;

import java.util.List;

import com.service.model.common.Events;

public interface EventsService {
	Events save(Events events);
	Events getEvents(String eventId);
	List<Events> getEventByVendorId(String vendorId);
}
