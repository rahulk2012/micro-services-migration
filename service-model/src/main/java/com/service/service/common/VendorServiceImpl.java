package com.service.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.common.Vendor;
import com.service.repository.common.VendorDao;
import com.service.util.ValidatorUtil;

@Service
//@Slf4j
public class VendorServiceImpl implements VendorService {
	@Autowired
	private VendorDao vendorDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Vendor save(final Vendor vendor) {
		try {
			return vendorDao.saveAndFlush(vendor);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public Vendor getVendor(String vendorId) {
		if (!ValidatorUtil.isValid(vendorId)) {
			return null;
		}
		try {
			Vendor vendor = vendorDao.getVendorById(vendorId);
			return vendor;
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public List<Vendor> getVendorList() {
		try {
			return vendorDao.findAll();
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

}