package com.service.service.common;

import com.service.model.common.Board;

public interface BoardService {
	Board save(Board board);

	Board getBoards(Integer id);

}
