package com.service.service.common;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.common.Board;
import com.service.repository.common.BoardDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class BoardServiceImpl implements BoardService {
	@Autowired
	private BoardDao boardDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Board save(final Board board) {
		try {
			return boardDao.saveAndFlush(board);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION .getValue(), exception);
		}
	}

	@Override
	public Board getBoards(Integer id) {
		if (!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			Optional<Board> board = boardDao.findById(id);
			if (board.isPresent()) {
				return board.get();
			} else {
				return null;
			}
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}


}