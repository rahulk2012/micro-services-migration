package com.service.service.common;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.common.Events;
import com.service.repository.common.EventsDao;
import com.service.util.ValidatorUtil;

@Service
//@Slf4j
public class EventsServiceImpl implements EventsService {
	@Autowired
	private EventsDao eventsDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Events save(final Events events) {
		try {
			return eventsDao.saveAndFlush(events);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION .getValue(), exception);
		}
	}

	@Override
	public Events getEvents(String eventId) {
		if (!ValidatorUtil.isValid(eventId)) {
			return null;
		}
		try {
			Events events = eventsDao.getEvendId(eventId);
			return events;
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public List<Events> getEventByVendorId(String vendorId) {
		if (!ValidatorUtil.isValid(vendorId)) {
			return null;
		}
		try {
			List<Events> events = eventsDao.getEvendsByVendorId(vendorId);
			return events;
		}catch (Exception e) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), e);
		}
	}
}