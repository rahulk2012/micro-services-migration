package com.service.service.common;

import java.util.List;

import com.service.model.common.Vendor;

public interface VendorService {
	Vendor save(Vendor vendor);
	Vendor getVendor(String vendorId);
	List<Vendor> getVendorList();
}