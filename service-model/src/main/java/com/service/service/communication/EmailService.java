package com.service.service.communication;

import java.util.List;

import com.service.model.communication.Email;

public interface EmailService {
	Email save(Email email);
	Email getEmail(Integer id);
	List<Email> getEmailUsingTo(String email, String type);
	List<Email> getEmailUsingFrom(String email, String type);
	List<Email> getNotificationUsingTo(String email, String type, String label);
	List<Email> getEmailUsingFilter(String email, String emailType, String type, String lable, Integer readStatus,
			String readType, Integer draftStatus, Integer deleteStatus, String deleteType);
	public List<Email> getEmailUsingFilterById(String id, String type);
}