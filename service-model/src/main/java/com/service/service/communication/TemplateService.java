package com.service.service.communication;

import com.service.model.communication.Template;

public interface TemplateService {
	Template save(Template template);
	Template getTemplate(String templateId);
	Template getTemplate(Integer templateType, String templateFor);
	Template getTemplate(String vendorId, String eventId, Integer templateType);
}