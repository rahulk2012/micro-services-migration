package com.service.service.communication;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.exception.handler.SeriException;
import com.service.model.communication.CommunicationSMS;
import com.service.repository.communication.CommunicationSMSDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommunicationSMSServiceImpl implements CommunicationSMSService {
	@Autowired
	private CommunicationSMSDao communicationSMSDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public CommunicationSMS save(final CommunicationSMS communicationSMS) {
		try {
			return communicationSMSDao.saveAndFlush(communicationSMS);
		} catch (final Exception exception) {
			log.info("save(final CommunicationSMS communicationSMS)", exception);
		}
		return null;
	}

	@Override
	public CommunicationSMS getCommunicationSMS(final Integer id) {
		if (!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			Optional<CommunicationSMS> attachments = communicationSMSDao.findById(id);
			if (attachments.isPresent()) {
				return attachments.get();
			}
		} catch (final SeriException exception) {
			log.info("getCommunicationSMS(final Integer id)", exception);
		}
		return null;
	}

	@Override
	public List<CommunicationSMS> getCommunicationSMS() {
		try {
			return communicationSMSDao.findAll();
		} catch (final SeriException exception) {
			log.info("getCommunicationSMS()", exception);
		}
		return Collections.emptyList();
	}

}