package com.service.service.communication;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.Email;
import com.service.repository.communication.EmailDao;
import com.service.util.ValidatorUtil;

import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailServiceImpl implements EmailService {
	@Autowired
	private EmailDao emailDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Email save(final Email email) {
		try {
			return emailDao.saveAndFlush(email);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public Email getEmail(final Integer id) {
		if (!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			Optional<Email> email = emailDao.findById(id);
			if (email.isPresent()) {
				return email.get();
			} else {
				return null;
			}
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public List<Email> getEmailUsingTo(String email, String type) {
		try {
			if (ValidatorUtil.isValid(email)) {
				final ExampleMatcher matcher = ExampleMatcher.matchingAll();
				final Email emails = new Email();
				emails.setToId(email);
				emails.setType(type);

				final Example<Email> filterBy = Example.of(emails, matcher);
				return emailDao.findAll(filterBy);
			}
		} catch (final SeriException exception) {
			log.info("getEmailUsingTo(String email, String type) ", exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Email> getEmailUsingFrom(String email, String type) {
		try {

			final ExampleMatcher matcher = ExampleMatcher.matchingAll();
			final Email emails = new Email();
			emails.setFromId(email);
			final Example<Email> filterBy = Example.of(emails, matcher);
			return emailDao.findAll(filterBy);

		} catch (final SeriException exception) {
			log.info("getEmailUsingFrom(String email, String type) ", exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Email> getNotificationUsingTo(String email, String type, String label) {
		try {
			if (ValidatorUtil.isValidEmail(email)) {
				final ExampleMatcher matcher = ExampleMatcher.matchingAll();
				final Email emails = new Email();
				emails.setToId(email);
				emails.setType(type);
				if (label != null) {
					emails.setLabel(label);
				}
				final Example<Email> filterBy = Example.of(emails, matcher);
				return emailDao.findAll(filterBy);
			}
		} catch (final SeriException exception) {
			log.info("getNotificationUsingTo(String email, String type, String label) ", exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Email> getEmailUsingFilter(String email, String emailType, String type, String lable,
			Integer readStatus, String readType, Integer draftStatus, Integer deleteStatus, String deleteType) {
		try {
			if (ValidatorUtil.isValid(email)) {
				final ExampleMatcher matcher = ExampleMatcher.matchingAll();
				final Email emails = new Email();
				emails.setIsDeleted(0);
				if (type.equalsIgnoreCase("email")) {
					emails.setType(type);
				}
				if (emailType.equalsIgnoreCase("to")) {
					emails.setToId(email);
				}
				if (emailType.equalsIgnoreCase("from")) {
					emails.setFromId(email);
				}
				if (!StringUtil.isNullOrEmpty(lable)) {
					emails.setLabel(lable);
				}
				if (readType.equalsIgnoreCase("all")) {

				} else {
					emails.setReadStatus(readStatus);
				}

				if (ValidatorUtil.isValid(draftStatus)) {
					emails.setDraftStatus(draftStatus);
				}
				if (deleteType.equalsIgnoreCase("sender")) {
					emails.setSenderDelStatus(deleteStatus);
				}
				if (deleteType.equalsIgnoreCase("reciver")) {
					emails.setReciverDelStatus(deleteStatus);
				}

				final Example<Email> filterBy = Example.of(emails, matcher);
				return emailDao.findAll(filterBy);
			}
		} catch (final SeriException exception) {
			log.info("getEmailUsingTo(String email, String type) ", exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Email> getEmailUsingFilterById(String id, String type) {
		try {
			if (ValidatorUtil.isValid(id)) {
				final ExampleMatcher matcher = ExampleMatcher.matchingAll();
				final Email emails = new Email();
				if (type.equalsIgnoreCase("email")) {
					emails.setType(type);
				}
				emails.setId(id);

				final Example<Email> filterBy = Example.of(emails, matcher);
				return emailDao.findAll(filterBy);
			}
		} catch (final SeriException exception) {
			log.info("getEmailUsingTo(String email, String type) ", exception);
		}
		return Collections.emptyList();
	}

//	@Override
//	public boolean deleteMail(final Integer id) {
//		try {
//			 emailDao.deleteByEmailId(id);
//			 return true;
//		} catch (final SeriException exception) {
//			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
//		}
//	}

}