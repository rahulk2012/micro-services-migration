package com.service.service.communication;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.CronsExecution;
import com.service.repository.communication.CronsExecutionDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CronsExecutionServiceImpl implements CronsExecutionService {
	@Autowired
	private CronsExecutionDao cronsExecutionDao;
	
	@Override
	@Transactional(rollbackFor = SeriException.class)
	public CronsExecution save(final CronsExecution cronsExecution) {
		try {
			return cronsExecutionDao.saveAndFlush(cronsExecution);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public CronsExecution getCronsExecution(String id) {
		if (!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			return cronsExecutionDao.findByCEId(id);
		}catch (Exception e) {
			log.info("getCronsExecution(String id) ", e);
		}
		return null;
	}
	
	@Override
	public List<CronsExecution> getCronsExecutionList() {
		try {
				final ExampleMatcher matcher = ExampleMatcher.matchingAll();
				final CronsExecution cronsExecution = new CronsExecution();
				//cronsExecution.setStatus("Active"); 
				final Example<CronsExecution> filterBy = Example.of(cronsExecution, matcher);
				return cronsExecutionDao.findAll(filterBy);
		} catch (final SeriException exception) {
			log.info("getCrons() ", exception);
		}
		return Collections.emptyList();
	}

	@Override
	public CronsExecution getCronsExecutionByCrons(String cronId) {
		if (!ValidatorUtil.isValid(cronId)) {
			return null;
		}
		try {
			return cronsExecutionDao.findByCronId(cronId);
		}catch (Exception e) {
			log.info("getCronsExecutionByCrons(String cronId) ", e);
		}
		return null;
	}
}