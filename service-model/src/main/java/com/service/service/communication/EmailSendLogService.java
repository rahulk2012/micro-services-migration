package com.service.service.communication;

import com.service.model.communication.EmailSendLog;

public interface EmailSendLogService {
	EmailSendLog save(EmailSendLog emailSendLog);

	EmailSendLog getEmailSendLog(Integer id);
}
