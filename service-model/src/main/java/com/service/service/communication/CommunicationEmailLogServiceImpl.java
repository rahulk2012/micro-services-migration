package com.service.service.communication;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.CommunicationEmailLog;
import com.service.repository.communication.CommunicationEmailLogDao;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommunicationEmailLogServiceImpl implements CommunicationEmailLogService {
	@Autowired
	private CommunicationEmailLogDao communicationEmailLogDao;

	@Override
	public CommunicationEmailLog save(CommunicationEmailLog communicationEmailLog) {
		try {
			return communicationEmailLogDao.saveAndFlush(communicationEmailLog);
		} catch (final Exception exception) {
			log.info("CommunicationEmailLog save(CommunicationEmailLog communicationEmailLog)", exception);
		}
		return null;
	}

	@Override
	public CommunicationEmailLog getCommunicationEmailLog(String id) {
		if (id==null) {
		return null;
		}
	try {
			return communicationEmailLogDao.findByEId(id);
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public List<CommunicationEmailLog> emailSendList(String vendorId, String eventId) {
		try {
			return communicationEmailLogDao.emailSendList(vendorId, eventId);
		} catch (final SeriException exception) {
			log.error("emailSendList() :: line no: 46",exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Object> distinctVendorList() {
		try {
			return communicationEmailLogDao.distinctVendorList();
		} catch (final SeriException exception) {
			log.error("distinctvendorList() :: line no: 55",exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Object> distinctEventbyVendorList(String vendorId) {
		try {
			return communicationEmailLogDao.distinctEventList(vendorId);
		} catch (final SeriException exception) {
			log.error("distinctEventbyVendorList(Integer vendorId) :: line no: 65",exception);
		}
		return Collections.emptyList();
	}


}