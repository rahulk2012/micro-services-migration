package com.service.service.communication;

import java.util.List;

import com.service.model.communication.EmailCheck;

public interface EmailCheckService {
	EmailCheck save(EmailCheck emailCheck);

	EmailCheck getEmailCheck(Integer id);

	EmailCheck getEmailCheckByEmailId(String emailId);

	List<EmailCheck> getAllEmailCheckDetails();
}
