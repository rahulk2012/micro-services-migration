package com.service.service.communication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.CommunicationChannel;
import com.service.repository.communication.CommunicationChanelDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommunicationChanelServiceImpl implements CommunicationChanelService {
	@Autowired
	private CommunicationChanelDao communicationChanelDao;

	@Override
	public CommunicationChannel save(CommunicationChannel communicationChanel) {
		try {
			return communicationChanelDao.saveAndFlush(communicationChanel);
		} catch (final Exception exception) {
			log.info("save(final CommunicationChanel communicationChanel)", exception);
		}
		return null;
	}

	@Override
	public CommunicationChannel getCommunicationChanel(String channelId) {
		if (!ValidatorUtil.isValid(channelId)) {
			return null;
		}
		try {
			CommunicationChannel communicationChanel = communicationChanelDao.findChannelId(channelId);
			return communicationChanel;
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}
}