package com.service.service.communication;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.data.domain.ExampleMatcher.StringMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.Template;
import com.service.repository.communication.TemplateDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class TemplateServiceImpl implements TemplateService {
	@Autowired
	private TemplateDao templateDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Template save(final Template template) {
		try {
			return templateDao.saveAndFlush(template);
		} catch (final Exception exception) {
			log.info("save(final Template Template)", exception);
		}
		return null;
	}

	@Override
	public Template getTemplate(String templateId) {
		if (!ValidatorUtil.isValid(templateId)) {
			return null;
		}
		try {
			Template template = templateDao.getTemplateById(templateId);
			return template;
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public Template getTemplate(Integer templateType, String templateFor) {
		try {
			final Template template = new Template();
			template.setTemplateType(templateType);
			/* template.setTemplateFor(templateFor); */
			final ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(StringMatcher.EXACT)
					.withIgnoreCase(false);
			final Example<Template> templatefilter = Example.of(template, matcher);
			List<Template> templateList=templateDao.findAll(templatefilter);
			if(!templateList.isEmpty()) {
				return templateList.get(0);
			}
			return null;
		} catch (final SeriException exception) {
			log.info("getTemplate(Integer templateType, String templateFor) :: ", exception);
		}
		return null;
	}

	@Override
	public Template getTemplate(String vendorId, String eventId, Integer templateType) {
		try {
			final Template template = new Template();
			template.setTemplateType(templateType);
			template.setEventId(eventId);
			template.setVendorId(vendorId);
			final ExampleMatcher matcher = ExampleMatcher.matching().withStringMatcher(StringMatcher.EXACT)
					.withIgnoreCase(false);
			final Example<Template> templatefilter = Example.of(template, matcher);
			List<Template> templateList=templateDao.findAll(templatefilter);
			if(!templateList.isEmpty()) {
				return templateList.get(0);
			}
			return null;
		} catch (final SeriException exception) {
			log.info("getTemplate(Integer templateType, Integer vendorId, Integer eventId) :: ", exception);
		}
		return null;
	}
	
}