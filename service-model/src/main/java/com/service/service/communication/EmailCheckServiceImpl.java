package com.service.service.communication;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.EmailCheck;
import com.service.repository.communication.EmailCheckDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailCheckServiceImpl implements EmailCheckService {
	@Autowired
	private EmailCheckDao emailCheckDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public EmailCheck save(final EmailCheck emailCheck) {
		try {
			return emailCheckDao.saveAndFlush(emailCheck);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public EmailCheck getEmailCheck(final Integer id) {
		Optional<EmailCheck> emailCheck = emailCheckDao.findById(id);
		if (emailCheck.isPresent()) {
			return emailCheck.get();
		}
		return null;
	}

	@Override
	public EmailCheck getEmailCheckByEmailId(String emailId) {
		try {
			if (ValidatorUtil.isValid(emailId)) {
				return emailCheckDao.findByEmailId(emailId);
			}
		} catch (final SeriException exception) {
			log.info("getEmailCheckByEmailId(String emailId) ", exception);
		}
		return null;
	}

	@Override
	public List<EmailCheck> getAllEmailCheckDetails() {
		try {
			return emailCheckDao.getAllEmailCheckList();
		} catch (final SeriException exception) {
			log.info("getAllDescription()", exception);
		}
		return Collections.emptyList();
	}

}
