package com.service.service.communication;

import com.service.model.communication.CommunicationSettings;

public interface CommunicationSettingsService {
	CommunicationSettings save(CommunicationSettings communicationSettings);
	CommunicationSettings getCommunicationSettings(Integer id);
	CommunicationSettings findByVendorId(String vendorId);
}