package com.service.service.communication;

import com.service.model.communication.CommunicationChannel;

public interface CommunicationChanelService {
	CommunicationChannel save(CommunicationChannel communicationChanel);
	CommunicationChannel getCommunicationChanel(String channelId);
}