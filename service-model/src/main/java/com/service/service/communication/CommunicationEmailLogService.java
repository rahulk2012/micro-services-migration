package com.service.service.communication;

import java.util.List;

import com.service.model.communication.CommunicationEmailLog;

public interface CommunicationEmailLogService {
	CommunicationEmailLog save(CommunicationEmailLog communicationEmailLog);
	CommunicationEmailLog getCommunicationEmailLog(String id);
	List<CommunicationEmailLog> emailSendList(String vendorId, String eventId);
	List<Object> distinctVendorList();
	List<Object> distinctEventbyVendorList(String vendorId);
}
