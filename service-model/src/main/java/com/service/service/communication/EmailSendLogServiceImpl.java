package com.service.service.communication;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.EmailSendLog;
import com.service.repository.communication.EmailSendLogDao;
import com.service.util.ValidatorUtil;


@Service
//@Slf4j
public class EmailSendLogServiceImpl implements EmailSendLogService {
	@Autowired
	private EmailSendLogDao emailSendLogDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public EmailSendLog save(final EmailSendLog emailSendLog) {
		try {
			return emailSendLogDao.saveAndFlush(emailSendLog);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public EmailSendLog getEmailSendLog(Integer id) {
		if(!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			Optional<EmailSendLog> emailSendLog = emailSendLogDao.findById(id);
			if(emailSendLog.isPresent()) {
				return emailSendLog.get();
			}
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
		return null;
	}
}