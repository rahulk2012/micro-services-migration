package com.service.service.communication;

import java.util.List;

import com.service.model.communication.Crons;

public interface CronsService {
	Crons save(Crons crons);
	Crons getCrons(String id);
	List<Crons> getCrons();
}