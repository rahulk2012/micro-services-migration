package com.service.service.communication;

import java.util.List;

import com.service.model.communication.CommunicationSMS;

public interface CommunicationSMSService {
	CommunicationSMS save(CommunicationSMS communicationSMS);

	CommunicationSMS getCommunicationSMS(Integer id);

	List<CommunicationSMS> getCommunicationSMS();
}
