package com.service.service.communication;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.CommunicationSettings;
import com.service.repository.communication.CommunicationSettingsDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommunicationSettingsServiceImpl implements CommunicationSettingsService {
	@Autowired
	private CommunicationSettingsDao communicationSettingsDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public CommunicationSettings save(final CommunicationSettings communicationSettings) {
		try {
			return communicationSettingsDao.saveAndFlush(communicationSettings);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION .getValue(), exception);
		}
	}

	@Override
	public CommunicationSettings getCommunicationSettings(Integer id) {
		if (!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			Optional<CommunicationSettings> communicationSettings = communicationSettingsDao.findById(id);
			if (communicationSettings.isPresent()) {
				return communicationSettings.get();
			} else {
				return null;
			}
		} catch (final SeriException exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public CommunicationSettings findByVendorId(String vendorId) {
		if (!ValidatorUtil.isValid(vendorId)) {
			return null;
		}
		try {
			return communicationSettingsDao.findByVendorId(vendorId);
		}catch (Exception e) {
			log.info("findByVendorId(Integer vendorId) ", e);
		}
		return null;
	}
}