package com.service.service.communication;

import java.util.List;

import com.service.model.communication.CronsExecution;

public interface CronsExecutionService {
	CronsExecution save(CronsExecution cronsExecution);
	CronsExecution getCronsExecution(String id);
	List<CronsExecution> getCronsExecutionList();
	CronsExecution getCronsExecutionByCrons(String cronId);
}