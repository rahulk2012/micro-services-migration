package com.service.service.communication;

import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.communication.Crons;
import com.service.repository.communication.CronsDao;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CronsServiceImpl implements CronsService {
	@Autowired
	private CronsDao cronsDao;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Crons save(final Crons crons) {
		try {
			return cronsDao.saveAndFlush(crons);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}

	@Override
	public Crons getCrons(String id) {
		if (!ValidatorUtil.isValid(id)) {
			return null;
		}
		try {
			return cronsDao.findByCId(id);
		}catch (Exception e) {
			log.info("getCrons() ", e);
		}
		return null;
	}
	
	@Override
	public List<Crons> getCrons() {
		try {
				final ExampleMatcher matcher = ExampleMatcher.matchingAll();
				final Crons crons = new Crons();
				//crons.setStatus("Active"); if only Active cron list show
				final Example<Crons> filterBy = Example.of(crons, matcher);
				return cronsDao.findAll(filterBy);
		} catch (final SeriException exception) {
			log.info("getCrons() ", exception);
		}
		return Collections.emptyList();
	}
	
}