package com.service.service.fs;

import java.util.List;

import com.service.model.fs.Answers;


public interface AnswersService {
	Answers save(Answers accounts);
	Answers getAnswers(Integer id);
	List<Answers> getAllAnswersList();
}