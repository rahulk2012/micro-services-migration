package com.service.service.fs;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.fs.Questions;
import com.service.repository.fs.QuestionsRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class QuestionsServiceImpl implements QuestionsService {
	@Autowired
	private QuestionsRepository questionsRepository;
	
	@PersistenceContext
	private EntityManager em;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Questions save(final Questions elements) {
		try {
			return questionsRepository.saveAndFlush(elements);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
			//log.info("Elements Save()", exception);
		}
	}

	@Override
	public Questions getQuestions(final Integer id) { 
		if (id==null) {
			return null;
		}
		try {
			Optional<Questions> elements = questionsRepository.findById(id);
			if(elements.isPresent()) {
				return elements.get();
			} else {
				return null;
			}
		} catch (final SeriException exception) {
			log.info("getQuestions(final Integer id)", exception);
		}
		return null;
	}
	
	@Override
	public List<Questions> getAllQuestionsList() {
		try {
				return  questionsRepository.findAll();
			} catch (final SeriException exception) {
				log.info("getAllQuestionsList()", exception);
			}
		return Collections.emptyList();
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<Object[]> getAllQuestionsListByEventType(String vendorId, String eventId, Integer questionType, Integer parentId,  Integer startLimit, Integer endLimit) {
		try {
			StringBuilder query = new StringBuilder("SELECT QS.ID, QS.QUESTION, EL.ELEMENT, QS.MENDETORY, QS.PARENT_ID, QS.EMOJI_CLASS FROM QUESTIONS QS " 
						+ "INNER JOIN ELEMENTS EL ON (EL.ID = QS.ELEMENT_ID AND EL.ACTIVATED=1 AND EL.DELETED=0) ");
			
			query.append(" WHERE QS.QUESTION_TYPE='"+questionType+"' AND QS.PARENT_ID='"+parentId+"' ");
			
			if(!StringUtils.isEmpty(eventId) && !StringUtils.isEmpty(vendorId)) {
				query.append(" AND QS.EVENT_ID='"+eventId+"' AND QS.VENDOR_ID='"+vendorId+"'");
			}
			
			
			query.append(" LIMIT "+startLimit+", "+endLimit);
			
			List list = em.createNativeQuery(query.toString()).getResultList();
			return list;

		} catch (final SeriException exception) {
			log.info("getAllQuestionsAnswersListByEventType()", exception);
		}
		return Collections.emptyList();
	
	}
	
	@SuppressWarnings("rawtypes")
	@Override
	public List<Object[]> getAllQuestionsAnswersListByEventType(String vendorId, String eventId, Integer startLimit, Integer endLimit) {
		try {
			StringBuilder query = new StringBuilder("SELECT QS.ID, AN.EMAILID, QS.QUESTION, (CASE WHEN QC.LABLE IS NULL THEN AN.ANSWER ELSE QC.LABLE END)ANS FROM QUESTIONS QS "  
					+ " LEFT JOIN ANSWERS AN ON (AN.QUESTION_ID = QS.ID AND AN.ACTIVATED=1 AND AN.DELETED=0) "  
					+ " LEFT JOIN QUESTION_CONTENT QC ON (QC.ID = AN.ANSWER AND QC.ACTIVATED=1 AND QC.DELETED=0) ");
			
			
			if(!StringUtils.isEmpty(eventId) && !StringUtils.isEmpty(vendorId)) {
				query.append(" WHERE QS.EVENT_ID='"+eventId+"' AND QS.VENDOR_ID='"+vendorId+"'");
			}
			
			query.append(" ORDER BY AN.UPDATED_DATE LIMIT "+startLimit+", "+endLimit);
			
			List list = em.createNativeQuery(query.toString()).getResultList();
			return list;

		} catch (final SeriException exception) {
			log.info("getAllQuestionsAnswersListByEventType()", exception);
		}
		return Collections.emptyList();
	}

	@Override
	public List<Questions> getALLActiveQuestions(String vendorId, String eventId, Integer startLimit, Integer endLimit) {
		try {
			if(StringUtils.isEmpty(startLimit) && StringUtils.isEmpty(endLimit)) {
				return questionsRepository.getALLActiveQuestions(vendorId, eventId);
			}
			return  questionsRepository.getALLActiveQuestionsLimit(vendorId, eventId, startLimit, endLimit);
		} catch (final SeriException exception) {
			log.info("getALLActiveQuestions()", exception);
		}
	return Collections.emptyList();
	}

	
}