package com.service.service.fs;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.fs.Answers;
import com.service.repository.fs.AnswerRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class AnswersServiceImpl implements AnswersService {
	@Autowired
	private AnswerRepository answerRepository;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Answers save(final Answers elements) {
		try {
			return answerRepository.saveAndFlush(elements);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
			//log.info("Elements Save()", exception);
		}
	}

	@Override
	public Answers getAnswers(final Integer id) { 
		Optional<Answers> elements = answerRepository.findById(id);
		if(elements.isPresent()) {
			return elements.get();
		}
		return null;
	}
	
	@Override
	public List<Answers> getAllAnswersList() {
		try {
				return  answerRepository.findAll();
			} catch (final SeriException exception) {
				log.info("getAllAnswersList()", exception);
			}
		return Collections.emptyList();
	}
}