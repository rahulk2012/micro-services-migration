package com.service.service.fs;

import java.util.List;

import com.service.model.fs.Elements;


public interface ElementsService {
	Elements save(Elements accounts);
	Elements getElements(Integer id);
	List<Elements> getAllElementsList();
}