package com.service.service.fs;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.fs.FeedbackDetails;
import com.service.repository.fs.FeedbackDetailsRepository;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class FeedbackDetailsServiceImpl implements FeedbackDetailsService {
	@Autowired
	private FeedbackDetailsRepository feedbackDetailsRepository;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public FeedbackDetails save(FeedbackDetails feedbackDetails) {
		try {
			return feedbackDetailsRepository.saveAndFlush(feedbackDetails);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}
	
	@Override
	public FeedbackDetails getFeedbackDetails(Integer id) {
		try {
			if (!ValidatorUtil.isValid(id)) {
				return null;
			}
			Optional<FeedbackDetails> elements = feedbackDetailsRepository.findById(id);
			if(elements.isPresent()) {
				return elements.get();
			}else {
				return null;
			}
		}catch (Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
		}
	}
	

	@Override
	public List<FeedbackDetails> getAllFeedbackOnVendorIdList(Integer vendorId){
		try {
				return  feedbackDetailsRepository.getAllFeedbackOnVendorIdList(vendorId);
			} catch (final SeriException exception) {
				log.info("getAllQuestionsContentList()", exception);
			}
		return Collections.emptyList();
	}

	@Override
	public FeedbackDetails getFeedbackDetails(String id) {
		if (id==null) {
			return null;
		}
		try {
			return feedbackDetailsRepository.getFeedbackList(id);
		}catch (Exception e) {
			log.info("getFeedbackDetails(String id)", e);
		}
		return null;
	}
}