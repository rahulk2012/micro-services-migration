package com.service.service.fs;

import java.util.List;

import com.service.model.fs.FeedbackDetails;

public interface FeedbackDetailsService {
	FeedbackDetails save(FeedbackDetails feedbackDetails);
	FeedbackDetails getFeedbackDetails(Integer id);
	FeedbackDetails getFeedbackDetails(String id);
	List<FeedbackDetails> getAllFeedbackOnVendorIdList(Integer vendorId);
}