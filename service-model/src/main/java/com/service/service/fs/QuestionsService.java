package com.service.service.fs;

import java.util.List;

import com.service.model.fs.Questions;

public interface QuestionsService {
	Questions save(Questions accounts);
	Questions getQuestions(Integer id);
	List<Questions> getAllQuestionsList();
	List<Object[]> getAllQuestionsListByEventType(String vendorId, String eventId, Integer questionType, Integer parentId, Integer startLimit, Integer endLimit);
	List<Object[]> getAllQuestionsAnswersListByEventType(String vendorId, String eventId, Integer startLimit, Integer endLimit);
	List<Questions> getALLActiveQuestions(String vendorId, String eventId,  Integer startLimit, Integer endLimit);
}