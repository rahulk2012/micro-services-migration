package com.service.service.fs;

import java.util.List;

import com.service.model.fs.QuestionsContent;

public interface QuestionsContentService {
	QuestionsContent save(QuestionsContent accounts);
	QuestionsContent getQuestionsContent(Integer id);
	List<QuestionsContent> getAllQuestionsContentList(Integer questionId);
	
	boolean deleteQuestionContentData(Integer questionId);
}