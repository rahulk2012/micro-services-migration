package com.service.service.fs;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.fs.QuestionsContent;
import com.service.repository.fs.QuestionsContentRepository;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class QuestionsContentServiceImpl implements QuestionsContentService {
	@Autowired
	private QuestionsContentRepository questionsContentRepository;
	
	@Override
	@Transactional(rollbackFor = SeriException.class)
	public QuestionsContent save(final QuestionsContent elements) {
		try {
			return questionsContentRepository.saveAndFlush(elements);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
			//log.info("Elements Save()", exception);
		}
	}

	@Override
	public QuestionsContent getQuestionsContent(final Integer id) { 
		Optional<QuestionsContent> elements = questionsContentRepository.findById(id);
		if(elements.isPresent()) {
			return elements.get();
		}
		return null;
	}
	
	@Override
	public List<QuestionsContent> getAllQuestionsContentList(Integer questionId) {
		if (!ValidatorUtil.isValid(questionId)) {
			return null;
		}
		try {
				return  questionsContentRepository.getQuestionContentList(questionId);
			} catch (final SeriException exception) {
				log.info("getAllQuestionsContentList()", exception);
			}
		return Collections.emptyList();
	}

	@Override
	public boolean deleteQuestionContentData(Integer questionId) {
		 questionsContentRepository.deleteQuestionContentData(questionId);
		 return false;
	}
}