package com.service.service.fs;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.service.constant.ApiConstant;
import com.service.exception.handler.SeriException;
import com.service.model.fs.Elements;
import com.service.repository.fs.ElementsRepository;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class ElementsServiceImpl implements ElementsService {
	@Autowired
	private ElementsRepository elementsRepository;

	@Override
	@Transactional(rollbackFor = SeriException.class)
	public Elements save(final Elements elements) {
		try {
			return elementsRepository.saveAndFlush(elements);
		} catch (final Exception exception) {
			throw new SeriException(ApiConstant.SERI_ADMIN_EXCEPTION.getValue(), exception);
			//log.info("Elements Save()", exception);
		}
	}

	@Override
	public Elements getElements(final Integer id) { 
		Optional<Elements> elements = elementsRepository.findById(id);
		if(elements.isPresent()) {
			return elements.get();
		}
		return null;
	}
	
	@Override
	public List<Elements> getAllElementsList() {
		try {
				return  elementsRepository.findAll();
			} catch (final SeriException exception) {
				log.info("getAllElementsList()", exception);
			}
		return Collections.emptyList();
	}
}