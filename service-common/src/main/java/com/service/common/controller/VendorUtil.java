package com.service.common.controller;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.common.dto.MasterDTO;
import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.VendorRequest;
import com.service.dto.response.common.VendorResponse;
import com.service.model.common.Vendor;
import com.service.service.common.VendorService;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class VendorUtil {
	
	@Autowired
	VendorService vendorService;
	
	public VendorResponse getEmptyVendorResponse() {
		VendorResponse response = VendorResponse.builder()
				.responseStatus(getEmptyResponseStatus("FS001", "Unable to get Vendor"))
				.build();
		return response;
	}
	public ResponseStatus getEmptyResponseStatus(String code, String message) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(code)
				.message(message)
				.build();
		return responseStatus;
	}
	
	public ResponseStatus saveVendorData(VendorRequest vendorRequest) {
		ResponseStatus response = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue()).build();
		try {
			if(!ValidatorUtil.isValid(vendorRequest.getVendorKey())) {   
				response.setMessage("Vendor Name can't be blank");
				return response;
			}
			
			Vendor vendor = vendorService.getVendor(vendorRequest.getVendorId());
			if(vendor==null) {
				vendor = new Vendor();
			}
			vendor.setVendorName(vendorRequest.getVendorKey());
			vendorService.save(vendor);
			response.setCode(ApiConstant.STATUS_CODE_SUCCESS.getValue());
			response.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.setMessage("Vendor Created Succesfully");
			return response;
		}catch (Exception e) {
			log.error("saveVendorData(VendorRequest vendorRequest) Exception caught :: ", e);
		}
	return response;	
	}
	
	
	public List<MasterDTO> getVendor(String vendorId) {
		if(ValidatorUtil.isValid(vendorId)) {
			Vendor vendor = vendorService.getVendor(vendorId);
			if(vendor == null) {
				List<MasterDTO> s = Collections.emptyList();
				return s;
			}
			List<MasterDTO> vendors = new ArrayList<>(2);
			MasterDTO masterDTO = MasterDTO.builder()
					.key(vendor.getVendorId().toString())
					.value(vendor.getVendorName())
					.orderBy(String.valueOf(1))
					.build();
			vendors.add(masterDTO);
			return vendors;
		}else {
			return null;
		}
	}
	
	public List<MasterDTO> getVendorList() {
		List<Vendor> vendor = vendorService.getVendorList();
		List<MasterDTO> vendors = new ArrayList<>(2);
		for(Vendor vend: vendor) {
			MasterDTO masterDTO = MasterDTO.builder()
					.key(vend.getVendorId().toString())
					.value(vend.getVendorName())
					.orderBy(String.valueOf(1))
					.build();
			vendors.add(masterDTO);	
		}
		return vendors;
	}
}
