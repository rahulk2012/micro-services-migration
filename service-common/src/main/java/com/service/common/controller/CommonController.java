package com.service.common.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.client.WebClient;

import com.service.common.dto.MasterDTO;
import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.EventRequest;
import com.service.dto.request.VendorRequest;
import com.service.dto.response.SaveEventResponse;
import com.service.dto.response.common.EventResponse;
import com.service.dto.response.common.VendorResponse;


@Controller
@CrossOrigin("*")
@RequestMapping("/common-service/api/v1")
public class CommonController {
//	@Autowired
//	private RestTemplate restTemplate;

	@Autowired
	WebClient.Builder webClientBuilder;

	@Autowired
	VendorUtil vendorUtil;
	
	@Autowired
	EventUtil eventUtil;

	
	@RequestMapping(value = "/save-vendor", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<ResponseStatus> getSaveVendorContent(@RequestBody VendorRequest vendorRequest) {
		ResponseStatus response = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue())
				.build();
		
		response = vendorUtil.saveVendorData(vendorRequest);
		if(response.getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}
	
	@RequestMapping(value = "/get-vendor", method = { RequestMethod.GET, RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<VendorResponse> getVendorContent(@RequestBody VendorRequest vendorRequest) {
		VendorResponse response = vendorUtil.getEmptyVendorResponse();
		
		List<MasterDTO> vendors = null;

		if(vendorRequest.getVendorKey()!=null && vendorRequest.getVendorKey().contains("ROLE_SUPER_ADMIN")){
			vendors = vendorUtil.getVendorList();
		} else {
			vendors = vendorUtil.getVendor(vendorRequest.getVendorId());
		}
		if (vendors == null) {
			response.setVendors(null);
			response.getResponseStatus().setMessage("Failed to find vendor List");
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
			return new ResponseEntity<VendorResponse>(response, HttpStatus.NO_CONTENT);
		}
		
		response.setVendors(vendors);
		response.getResponseStatus().setMessage("Vendor list");
		response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		return new ResponseEntity<VendorResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/get-event", method = {RequestMethod.POST }, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<EventResponse> getEventContent(@RequestBody EventRequest eventRequest) {
		EventResponse response = eventUtil.getEmptyEventResponse();

		List<MasterDTO> events = eventUtil.getEvent(eventRequest.getVendorId());
		if (events == null) {
			response.setEvents(null);
			response.getResponseStatus().setMessage("Failed to find dummy feedback");
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
			return new ResponseEntity<EventResponse>(response, HttpStatus.OK);
		}
		response.setEvents(events);
		response.getResponseStatus().setMessage("Events List");
		response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		return new ResponseEntity<EventResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save-event", method = { RequestMethod.POST }, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<SaveEventResponse> getSaveEventContent(@RequestBody EventRequest eventRequest) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue())
				.build();
		SaveEventResponse response= SaveEventResponse.builder()
				.responseStatus(responseStatus)
				.build();
		
		response = eventUtil.saveEventData(eventRequest);
		if(response.getResponseStatus().getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}

}
