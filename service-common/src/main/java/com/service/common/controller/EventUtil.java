package com.service.common.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.common.dto.MasterDTO;
import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.EventRequest;
import com.service.dto.response.SaveEventResponse;
import com.service.dto.response.common.EventResponse;
import com.service.model.common.Events;
import com.service.service.common.EventsService;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class EventUtil {
	
	@Autowired
	EventsService eventsService;
	
	public EventResponse getEmptyEventResponse() {
		EventResponse response = EventResponse.builder()
				.responseStatus(getEmptyResponseStatus("FS001", "Unable to get Event"))
				.build();
		return response;
	}
	public ResponseStatus getEmptyResponseStatus(String code, String message) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(code)
				.message(message)
				.build();
		return responseStatus;
	}

	public List<MasterDTO> getEvent(String vendorId) {
		try {
			List<MasterDTO> masterDTO = new ArrayList<>();
			List<Events> events = eventsService.getEventByVendorId(vendorId);
			if(!events.isEmpty()) {
				for (Events ev : events) {
					MasterDTO master = MasterDTO.builder()
							.key(ev.getEventId().toString())
							.value(ev.getEventName().replace("_"," "))
							.orderBy(String.valueOf(1))
							.build();
					masterDTO.add(master);
				}	
			}
			return masterDTO;	
		}catch (Exception e) {
			log.info("getEvent(Integer vendorId)"+e);
		}
		return null;
	}
	public SaveEventResponse saveEventData(EventRequest eventRequest) {
		SaveEventResponse response =new SaveEventResponse();
		ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue()).build();
		try {
			if  (!ValidatorUtil.isValid(eventRequest.getEmailSubject())) {
				responseStatus.setMessage("Event Subject can't be blank");
				response.setResponseStatus(responseStatus);
				return response;
			}
			if  (!ValidatorUtil.isValid(eventRequest.getVendorId())){
				responseStatus.setMessage("Vendor Id  can't be blank");
				response.setResponseStatus(responseStatus);
				return response;
			}
			Events events = eventsService.getEvents(eventRequest.getEventId());
			if(events==null) {
				events = new Events();
			}
			events.setEventName(eventRequest.getEmailSubject());
			events.setVendorId(eventRequest.getVendorId());
			eventsService.save(events);
			responseStatus.setCode(ApiConstant.STATUS_CODE_SUCCESS.getValue());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage("Event saved Succesfully");
			MasterDTO events1=new MasterDTO();
			events1.setKey(events.getEventId().toString());
			response.setEvents(events1);
			response.setResponseStatus(responseStatus);
			return response;
		}catch (Exception e) {
			log.error("saveEventData(EventRequest eventRequest) Exception caught :: ", e);
		}
	return response;	
	}
	
}
