package com.service.api.v1.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.EventRequest;
import com.service.dto.request.VendorRequest;
import com.service.dto.response.common.EventResponse;
import com.service.dto.response.common.VendorResponse;
import com.service.dto.response.common.VendorsAndEventsResponse;
import com.service.util.ApiUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
@CrossOrigin("*")
@RequestMapping("/gateway/api/v1")
public class CommonController extends BaseController {

	@Autowired
	private ApiUtil apiUtil;

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	WebClient.Builder webClientBuilder;

	@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
	@ApiOperation(value = "${CommonController.me}", response = VendorsAndEventsResponse.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//	@HystrixCommand(fallbackMethod = "getVendorEventFallback")
	@RequestMapping(value = "/common/get-vendor-events/{vendorId}", method = { RequestMethod.GET })
	public ResponseEntity<VendorsAndEventsResponse> getVendor(@PathVariable("vendorId") String vendorId,
			HttpServletRequest request) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		VendorsAndEventsResponse response = VendorsAndEventsResponse.builder().build();
		ResponseStatus responseStatus = apiUtil.getEmptyResponseStatus("FS000", "Vendor and Event api..");
		response.setResponseStatus(responseStatus);

//		boolean hello = authorization().contains("ROLE_SUPER_ADMIN");
		
		List<String> roles=authorization();
		VendorRequest vendorRequest = VendorRequest.builder().vendorId(vendorId).vendorKey(roles.toString()).build();
		HttpEntity<VendorRequest> requestVendor = new HttpEntity<>(vendorRequest, headers);
		ResponseEntity<VendorResponse> vendorResponse = restTemplate.postForEntity(
				ApiConstant.URL_COMMON_SERVICE.getValue() + "/get-vendor", requestVendor, VendorResponse.class);
		log.info("vendorResponse" + vendorResponse);
		boolean vendorFlag=false;
		boolean eventFlag=false;
		String vendorResponseStatus = vendorResponse.getBody().getResponseStatus().getStatus();
		if(vendorResponseStatus.equalsIgnoreCase(ApiConstant.STATUS_MSG_SUCCESS.getValue())) {
			response.setVendors(vendorResponse.getBody().getVendors());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setCode(ApiConstant.CODE_SUCCESS.getValue());
			vendorFlag=true;
		}
		
		if(vendorFlag && roles.size()==1) {
			EventRequest eventRequest = EventRequest.builder().vendorId(vendorId).build();
			HttpEntity<EventRequest> requestEvent = new HttpEntity<>(eventRequest, headers);
			ResponseEntity<EventResponse> eventResponse = restTemplate.postForEntity(
					ApiConstant.URL_COMMON_SERVICE.getValue() + "/get-event", requestEvent, EventResponse.class);
			log.info("eventResponse" + eventResponse);
			String eventResponseStatus = eventResponse.getBody().getResponseStatus().getStatus();
			if(eventResponseStatus.equalsIgnoreCase(ApiConstant.STATUS_MSG_SUCCESS.getValue())) {
				response.setEvents(eventResponse.getBody().getEvents());
				responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
				responseStatus.setCode(ApiConstant.CODE_SUCCESS.getValue());
				eventFlag=true;
			}
		}
		if (vendorFlag && eventFlag) {
			responseStatus.setMessage("Vendor and Events response success");
		} else if (vendorFlag && !eventFlag) {
			responseStatus.setMessage("Vendor response success");
		} else if (!vendorFlag && eventFlag) {
			responseStatus.setMessage("Event response success");
		} else {
			responseStatus.setMessage("Vendor and Events response failed");
		}
		response.setResponseStatus(responseStatus);
		response.setVendors(vendorResponse.getBody().getVendors());
		return new ResponseEntity<VendorsAndEventsResponse>(response, HttpStatus.OK);
	}

	public ResponseEntity<VendorsAndEventsResponse> getVendorEventFallback(String vendorId) {
		ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.CODE_SUCCESS.getValue())
				.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue()).build();
		VendorsAndEventsResponse response = VendorsAndEventsResponse.builder().responseStatus(responseStatus).build();
		return new ResponseEntity<VendorsAndEventsResponse>(response, HttpStatus.OK);
	}

	@RequestMapping(value = "/common/get-events/{vendorId}", method = { RequestMethod.GET })
	public ResponseEntity<EventResponse> getEvents(@PathVariable("vendorId") String vendorId, HttpServletRequest request) {
		
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		EventResponse response = EventResponse.builder().build();
		ResponseStatus responseStatus = apiUtil.getEmptyResponseStatus("FS000", "Vendor and Event api..");
		response.setResponseStatus(responseStatus);

		EventRequest eventRequest = EventRequest.builder().vendorId(vendorId).build();
		HttpEntity<EventRequest> requestEvent = new HttpEntity<>(eventRequest, headers);
		ResponseEntity<EventResponse> eventResponse = restTemplate.postForEntity(
				ApiConstant.URL_COMMON_SERVICE.getValue() + "/get-event", requestEvent, EventResponse.class);
		log.info("eventResponse" + eventResponse);
		String eventResponseStatus = eventResponse.getBody().getResponseStatus().getStatus();
		if(eventResponseStatus.equalsIgnoreCase(ApiConstant.STATUS_MSG_SUCCESS.getValue())) {
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage("Event list");
		}
		response.setEvents(eventResponse.getBody().getEvents());
		response.setResponseStatus(responseStatus);
		return new ResponseEntity<EventResponse>(response, HttpStatus.OK);
	}
}
