package com.service.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.AnswersRequest;
import com.service.dto.request.FeedBackRequest;
import com.service.dto.request.FeedbackDetailRequest;
import com.service.dto.request.QuestionRequest;
import com.service.dto.request.UpdateFeedbackDetailsRequest;
import com.service.dto.response.AnswersResponse;
import com.service.dto.response.FeedBackResponse;
import com.service.dto.response.FeedbackDetailResponse;
import com.service.dto.response.QuestionResponse;
import com.service.dto.response.UpdateFeedbackDetailResponse;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@CrossOrigin("*")
@RequestMapping("/gateway/api/v1")
public class FeedbackController {
	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	WebClient.Builder webClientBuilder;

	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
	@ApiOperation(value = "${FeedbackController.me}", response = FeedBackResponse.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@HystrixCommand(fallbackMethod = "getFeedbackByVendorOrEventFallback")
	@RequestMapping(value="/feedback/get-feedback/{vendorId}/{eventId}", method= {RequestMethod.GET, RequestMethod.POST})
	public ResponseEntity<FeedBackResponse> getFeedbackByVendorOrEvent(@PathVariable("vendorId") String vendorId,
			@PathVariable("eventId") String eventId) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");

		FeedBackRequest feedBackRequest = FeedBackRequest.builder().vendorId(vendorId).eventId(eventId).build();
		HttpEntity<FeedBackRequest> request = new HttpEntity<>(feedBackRequest, headers);

		ResponseEntity<FeedBackResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/get-feedback", request,
				FeedBackResponse.class);
		return response;
	}
	
	public ResponseEntity<FeedBackResponse> getFeedbackByVendorOrEventFallback(String vendorId,  String eventId) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.CODE_SUCCESS.getValue())
				.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue())
				.build();
		FeedBackResponse response = FeedBackResponse.builder()
				.responseStatus(responseStatus)
				.build();
		return new ResponseEntity<FeedBackResponse>(response, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
	@ApiOperation(value = "${FeedbackController.me}", response = QuestionResponse.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	//@HystrixCommand(fallbackMethod = "getQustionFallback")
	@RequestMapping("/feedback/get-question/{vendorId}/{eventId}")
	public ResponseEntity<QuestionResponse> getQuestions(@PathVariable("vendorId") String vendorId,
			@PathVariable("eventId") String eventId, @RequestParam("questionType") Integer questionType, 
			@RequestParam("parentId") Integer parentId, @RequestParam("startLimit") Integer startLimit, 
			@RequestParam("endLimit") Integer endLimit) {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");

		QuestionRequest questionRequest = QuestionRequest.builder()
				.vendorId(vendorId)
				.eventType(eventId)
				.eventId(eventId)
				.questionType(questionType)
				.parentId(parentId)
				.startLimit(startLimit)
				.endLimit(endLimit)
				.build();
		HttpEntity<QuestionRequest> request = new HttpEntity<>(questionRequest, headers);

		ResponseEntity<QuestionResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/get-question", request,
				QuestionResponse.class);
		return response;
	}
	
	public ResponseEntity<QuestionResponse> getQustionFallback(String vendorId,  String eventId, Integer questionType, 
			Integer parentId, Integer startLimit,  Integer endLimit) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.CODE_SUCCESS.getValue())
				.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue())
				.build();
		QuestionResponse response = QuestionResponse.builder()
				.responseStatus(responseStatus)
				.build();
		return new ResponseEntity<QuestionResponse>(response, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
	@ApiOperation(value = "${FeedbackController.me}", response = AnswersResponse.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	//@HystrixCommand(fallbackMethod = "submitAnswerFallback")
		@RequestMapping(value = "/feedback/submit-answers", method = { RequestMethod.GET, RequestMethod.POST})
		public ResponseEntity<AnswersResponse> submitAnswer(@RequestBody AnswersRequest answersRequest) {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");

			HttpEntity<AnswersRequest> request = new HttpEntity<>(answersRequest, headers);

			ResponseEntity<AnswersResponse> response = restTemplate.postForEntity(
					ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/submit-answers", request,
					AnswersResponse.class);
			return response;
		}
		
		public ResponseEntity<AnswersResponse> submitAnswerFallback(@RequestBody AnswersRequest answersRequest) {
			ResponseStatus responseStatus = ResponseStatus.builder()
					.status(ApiConstant.STATUS_MSG_FAILED.getValue())
					.code(ApiConstant.CODE_SUCCESS.getValue())
					.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue())
					.build();
			AnswersResponse response = AnswersResponse.builder()
					.responseStatus(responseStatus)
					.build();
			return new ResponseEntity<AnswersResponse>(response, HttpStatus.OK);
		}
	
	/*
	 * Alternative WebClient way Movie movie =
	 * webClientBuilder.build().get().uri("http://localhost:8082/movies/"+
	 * rating.getMovieId()) .retrieve().bodyToMono(Movie.class).block();
	 */
		//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
		@ApiOperation(value = "${FeedbackController.me}", response = ResponseStatus.class)
		@ApiResponses(value = { //
				@ApiResponse(code = 400, message = "Something went wrong"), //
				@ApiResponse(code = 403, message = "Access denied"), //
				@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
//		@HystrixCommand(fallbackMethod = "getSendFeedbackDetailsFallback")
		@RequestMapping(value = "/feedback/send-feedback-details", method = {
				RequestMethod.GET, RequestMethod.POST })
		public  ResponseEntity<ResponseStatus> getSaveCommunicationTemplate(@RequestBody FeedbackDetailRequest feedbackDetailRequest) {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");

			HttpEntity<FeedbackDetailRequest> request = new HttpEntity<>(feedbackDetailRequest, headers);

			 ResponseEntity<ResponseStatus> response = restTemplate.postForEntity(ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/send-feedback-details", request,ResponseStatus.class);
			return response;
		}
		
		public ResponseEntity<ResponseStatus> getSendFeedbackDetailsFallback(@RequestBody FeedbackDetailRequest feedbackDetailRequest) {
			ResponseStatus response= ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
					.code(ApiConstant.CODE_SUCCESS.getValue())
					.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue()).build();
			return new ResponseEntity<ResponseStatus>(response, HttpStatus.OK);
		}
		
		@ApiOperation(value = "${FeedbackController.me}", response = QuestionResponse.class)
		@ApiResponses(value = { //
				@ApiResponse(code = 400, message = "Something went wrong"), //
				@ApiResponse(code = 403, message = "Access denied"), //
				@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
		//@HystrixCommand(fallbackMethod = "getQustionFallback")
		@RequestMapping("/feedback/get-feedback-detail/{feedbackId}")
		public ResponseEntity<FeedbackDetailResponse> getFeedbackDetail(@PathVariable("feedbackId") String feedbackId) {
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");

			FeedbackDetailRequest feedbackDetailRequest = FeedbackDetailRequest.builder()
					.feedbackId(feedbackId)
					.build();
			HttpEntity<FeedbackDetailRequest> request = new HttpEntity<>(feedbackDetailRequest, headers);

			ResponseEntity<FeedbackDetailResponse> response = restTemplate.postForEntity(
					ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/get-feedback-detail/"+feedbackId, request,
					FeedbackDetailResponse.class);
			
			return response;
		}
		
		
		//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
		@ApiOperation(value = "${FeedbackController.me}", response = AnswersResponse.class)
		@ApiResponses(value = { //
				@ApiResponse(code = 400, message = "Something went wrong"), //
				@ApiResponse(code = 403, message = "Access denied"), //
				@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
		//@HystrixCommand(fallbackMethod = "submitAnswerFallback")
			@RequestMapping(value = "/feedback/update-feedback-details", method = { RequestMethod.GET, RequestMethod.POST})
			public ResponseEntity<UpdateFeedbackDetailResponse> submitClick(@RequestBody UpdateFeedbackDetailsRequest updateFeedbackRequest) {
				HttpHeaders headers = new HttpHeaders();
				headers.set("Content-Type", "application/json");

				HttpEntity<UpdateFeedbackDetailsRequest> request = new HttpEntity<>(updateFeedbackRequest, headers);

				ResponseEntity<UpdateFeedbackDetailResponse> response = restTemplate.postForEntity(
						ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/update-feedback-details", request,
						UpdateFeedbackDetailResponse.class);
				return response;
			}
}
