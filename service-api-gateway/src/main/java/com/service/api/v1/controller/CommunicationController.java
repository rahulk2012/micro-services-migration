package com.service.api.v1.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.CronRequest;
import com.service.dto.request.TemplateRequest;
import com.service.dto.request.TemplateSaveRequest;
import com.service.dto.response.CronsResponse;
import com.service.dto.response.TemplateResponse;
import com.service.util.HeaderUtil;

import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;

@Controller
@CrossOrigin("*")
@RequestMapping("/gateway/api/v1")
public class CommunicationController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	WebClient.Builder webClientBuilder;

	@Autowired
	HeaderUtil uiUtil;
	
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
	@ApiOperation(value = "${CommunicationController.me}", response = TemplateResponse.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	@HystrixCommand(fallbackMethod = "getTemplateByVendorAndEventAndTemplateTypeFallback")
	@RequestMapping(value = "/communication/get-template/{vendorId}/{eventId}/{templateType}", method = {
			RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<TemplateResponse> getTemplateByVendorAndEventAndTemplateType(
			@PathVariable("vendorId") String vendorId, @PathVariable("eventId") String eventId,
			@PathVariable("templateType") Integer templateType) {

		TemplateRequest templateRequest = TemplateRequest.builder().vendorId(vendorId).eventId(eventId).build();
		HttpEntity<TemplateRequest> request = new HttpEntity<>(templateRequest, uiUtil.getHeader());

		ResponseEntity<TemplateResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_COMMUNICATION_SERVICE.getValue() + "/get-template", request, TemplateResponse.class);
		return response;
	}

	public ResponseEntity<TemplateResponse> getTemplateByVendorAndEventAndTemplateTypeFallback(String vendorId,
			String eventId, Integer templateType) {
		ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.CODE_SUCCESS.getValue())
				.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue()).build();
		TemplateResponse response = TemplateResponse.builder().responseStatus(responseStatus).build();
		return new ResponseEntity<TemplateResponse>(response, HttpStatus.OK);
	}

	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
	@ApiOperation(value = "${CommunicationController.me}", response = TemplateResponse.class)
	@ApiResponses(value = { //
			@ApiResponse(code = 400, message = "Something went wrong"), //
			@ApiResponse(code = 403, message = "Access denied"), //
			@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
	//@HystrixCommand(fallbackMethod = "getSaveCommunicationTemplateFallback")
	@RequestMapping(value = "/communication/save-communication-template", method = {RequestMethod.POST })
	public ResponseEntity<TemplateResponse> getSaveCommunicationTemplate(@RequestBody TemplateSaveRequest templateSaveRequest) {
		HttpEntity<TemplateSaveRequest> request = new HttpEntity<>(templateSaveRequest, uiUtil.getHeader());

		ResponseEntity<TemplateResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_COMMUNICATION_SERVICE.getValue()+"/save-communication-template", request, TemplateResponse.class);
		return response;
	}
	
	public ResponseEntity<TemplateResponse> getSaveCommunicationTemplateFallback(@RequestBody TemplateSaveRequest templateSaveRequest) {
		ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.CODE_SUCCESS.getValue())
				.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue()).build();
		TemplateResponse response = TemplateResponse.builder().responseStatus(responseStatus).build();
		return new ResponseEntity<TemplateResponse>(response, HttpStatus.OK);
	}
	
	//@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_CLIENT')")
		@ApiOperation(value = "${CommunicationController.me}", response = TemplateResponse.class)
		@ApiResponses(value = { //
				@ApiResponse(code = 400, message = "Something went wrong"), //
				@ApiResponse(code = 403, message = "Access denied"), //
				@ApiResponse(code = 500, message = "Expired or invalid JWT token") })
		@HystrixCommand(fallbackMethod = "getCronListFallback")
		@RequestMapping(value = "/communication/get-cron-list", method = {
				RequestMethod.GET, RequestMethod.POST })
		public ResponseEntity<CronsResponse> getCronList(@RequestBody CronRequest cronRequest) {
			HttpEntity<CronRequest> request = new HttpEntity<>(cronRequest, uiUtil.getHeader());
			ResponseEntity<CronsResponse> response = restTemplate.postForEntity(
					ApiConstant.URL_COMMUNICATION_SERVICE.getValue()+"/get-cron-list", request, CronsResponse.class);
			return response;
		}
		
		public ResponseEntity<CronsResponse> getCronListFallback(@RequestBody CronRequest cronRequest) {
			ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
					.code(ApiConstant.CODE_SUCCESS.getValue())
					.message(ApiConstant.SERVICE_TEMPORARILY_UNAVAILABLE.getValue()).build();
			CronsResponse response = CronsResponse.builder().responseStatus(responseStatus).build();
			return new ResponseEntity<CronsResponse>(response, HttpStatus.OK);
		}
}
