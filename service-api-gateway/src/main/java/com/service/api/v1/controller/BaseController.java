package com.service.api.v1.controller;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BaseController {
	
	public List<String> authorization() {
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
//		Collection<? extends GrantedAuthority> authorities = auth.getAuthorities(); //get logged in username
//        Collection target = new HashSet();
//        target.addAll(authorities); 
//        SimpleGrantedAuthority singleAuth = new SimpleGrantedAuthority("ROLE_SUPER_ADMIN");
//        log.info("getVendor :: userDetails "+ target.contains(singleAuth));
        
        Collection<? extends GrantedAuthority> grantedAuthorityList = auth
                .getAuthorities();

        List<String> authorities = new ArrayList<String>();

        for (GrantedAuthority grantedAuthority : grantedAuthorityList) {
            authorities.add(grantedAuthority.getAuthority());
        }
	    return authorities;
	}
	public boolean isRoleFound(List<String> roles, String roleForCheck) {
		boolean flag = roles.contains(roleForCheck);
		log.info("getVendor :: flag "+ flag);
		return flag;
	}
}
