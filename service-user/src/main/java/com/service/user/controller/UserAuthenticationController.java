package com.service.user.controller;

import java.util.Date;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import java.util.UUID;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.service.constant.ApiConstant;
import com.service.model.user.Role;
import com.service.model.user.RoleName;
import com.service.model.user.User;
import com.service.repository.user.RoleRepository;
import com.service.repository.user.UserRepository;
import com.service.service.user.UserPrinciple;
import com.service.user.security.jwt.JwtProvider;
//import com.service.service.user.UserService;
import com.service.util.AesUtil;
import com.service.util.ApiUtil;
import com.service.util.ValidatorUtil;
import com.servicer.dto.use.LoginRequest;
import com.servicer.dto.use.LoginResponse;
import com.servicer.dto.use.SignupRequest;
import com.servicer.dto.use.SignupResponse;

import lombok.extern.slf4j.Slf4j;

@RestController
@RequestMapping("/gateway/api/v1")
@CrossOrigin("*")
@Slf4j
public class UserAuthenticationController {

	@Autowired
	AuthenticationManager authenticationManager;

	@Autowired
	UserRepository userRepository;

	@Autowired
	RoleRepository roleRepository;

	@Autowired
	PasswordEncoder passwordEncoder;

	@Autowired
	JwtProvider jwtProvider;

	@Autowired
	AesUtil aesUtil;

//	@Autowired
//	UserService userService;

	@Autowired
	ApiUtil apiUtil;
	
	@RequestMapping(value = "/authenticate", method = RequestMethod.POST)
	public ResponseEntity<?> createAuthenticationToken(@Valid @RequestBody LoginRequest loginRequest){
		log.info("createAuthenticationToken invoked");
		LoginResponse response = LoginResponse.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.statusCode(ApiConstant.CODE_FAILED.getValue())
				.timestamp(apiUtil.getTimesTamp())
				.build();
		try {
			if (!ValidatorUtil.isValid(loginRequest.getUsername())) {
				log.info("createAuthenticationToken Username is invalid");
				response.setMessage("Invalid Request");
				response.setStatusCode("E003");
				return ResponseEntity.ok().body(response);
			}
			if (!ValidatorUtil.isValid(loginRequest.getPassword())) {
				log.info("createAuthenticationToken Password is invalid");
				response.setMessage("Invalid Request");
				response.setStatusCode("E004");
				return ResponseEntity.ok().body(response);
			}
			try {
				Authentication authentication = authenticationManager.authenticate(
						new UsernamePasswordAuthenticationToken(loginRequest.getUsername(), loginRequest.getPassword()));
				if(authentication==null) {
					log.info("createAuthenticationToken authentication is null");
					response.setMessage("Invalid Request");
					response.setStatusCode("E005");
					return ResponseEntity.ok().body(response);
				}
				SecurityContextHolder.getContext().setAuthentication(authentication);
				UserPrinciple userPrincipal = (UserPrinciple) authentication.getPrincipal();
				String token = jwtProvider.generateJwtToken(userPrincipal);
				response.setToken(token);
				response.setEncryptedUserId(UUID.nameUUIDFromBytes(userPrincipal.getId().toString().getBytes()).toString());
				
			}catch(BadCredentialsException e) {
				log.info("createAuthenticationToken BadCredentialsException caught ",e);
				response.setMessage("Either username or password is incorrect");
				response.setStatusCode("E006");
				return ResponseEntity.ok(response);
			}
			response.setMessage("provided credentials are valid.`");
			response.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.setStatusCode(ApiConstant.CODE_SUCCESS.getValue());
			response.setTimestamp(apiUtil.getTimesTamp());
		}catch(Exception e) {
			log.info("createAuthenticationToken exception caught ",e);
			response.setMessage(ApiConstant.TECHNICAL_GLITCH.getValue());
			response.setStatusCode(ApiConstant.CODE_EXCPTION.getValue());
		}
		return ResponseEntity.ok(response);
	}
	
	@PostMapping("/signup")
	public ResponseEntity<SignupResponse> signupUser(@Valid @RequestBody SignupRequest signUpRequest) {
		log.info("signupUser invoked");
		SignupResponse response = SignupResponse.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.statusCode(ApiConstant.CODE_FAILED.getValue())
				.timestamp(apiUtil.getTimesTamp())
				.build();
		try {
			if (userRepository.existsByUserName(signUpRequest.getUsername())) {
				log.info("signupUser Username is already taken!");
				response.setMessage("Username is already taken!");
				response.setStatusCode("E003");
				return ResponseEntity.ok().body(response);
			}
			User user = User.builder()
					.userName(signUpRequest.getUsername())
					.password(passwordEncoder.encode(signUpRequest.getPassword()))
					.active("Y")
					.deleted("N")
					.createdAt(new Date())
					.updatedAt(new Date())
					.build();

			Set<String> strRoles = signUpRequest.getRoles();
			Set<Role> roles = new HashSet<>();
			for(String role : strRoles) {
				Optional<Role> rolesFromUI = roleRepository.findByName(RoleName.valueOf("ROLE_"+role));
				if(!rolesFromUI.isPresent()) {
					log.info("signupUser unknown role: "+role);
					response.setMessage("Unknown role trying to signup");
					response.setStatusCode("E004");
					return ResponseEntity.ok().body(response);
				}
				roles.add(rolesFromUI.get());
			}
			
			user.setRoles(roles);
			userRepository.save(user);
			user.setEncryptedId(UUID.nameUUIDFromBytes(user.getId().toString().getBytes()).toString());
			userRepository.save(user);
			
			response.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.setStatusCode(ApiConstant.CODE_SUCCESS.getValue());
			response.setTimestamp(apiUtil.getTimesTamp());
			response.setEncryptedUserId(user.getEncryptedId());
		}catch(Exception e) {
			log.info("signupUser exception caught ",e);
			response.setMessage(ApiConstant.TECHNICAL_GLITCH.getValue());
			response.setStatusCode(ApiConstant.CODE_EXCPTION.getValue());
		}
		
		return ResponseEntity.ok().body(response);
	}
}