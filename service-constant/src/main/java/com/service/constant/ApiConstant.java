package com.service.constant;

public enum ApiConstant {
	
	URL_FEEDBACK_SURVEY_SERVICE  ("http://ecoservice-feedback-survey/feedback-survey/api/v1"),
	URL_USER_SERVICE  ("http://ecoservice-user/user/api/v1"),
	URL_COMMUNICATION_SERVICE  ("http://ecoservice-communication/communication-service/api/v1"),
	URL_COMMON_SERVICE  ("http://ecoservice-common/common-service/api/v1"),
	URL_API_SERVICE  ("http://ecoservice-api-gateway/gateway/api/v1"),
	URL_QUESTIONS_SERVICE  ("http://ecoservice-feedback-survey/feedback-survey/api/v1"),
	
	TECHNICAL_GLITCH  ("Sorry for inconvenience, system has encountered technical glitch."),
	SERVICE_TEMPORARILY_UNAVAILABLE  ("Sorry for inconvenience, Service temporarily unavailable."),
	MESSAGE_SESSION_OUT  ("Your session has been timed out, please login again"),
	MESSAGE_INVALID_ROLE_ACCESS  ("Unauthrosied access!!! Please loging again"),
	SERVICE_EXCEPTION ("SERVICE EXCEPTION"),
	STATUS_MSG_FAILED ("FAILED"),
	STATUS_MSG_SUCCESS ("SUCCESS"),
	STATUS_MSG_EXCEPTION ("EXCEPTION"),
	STATUS_MSG_SESSION_OUT ("SESSIONOUT"),
	
	CODE_SUCCESS("API000"),
	CODE_FAILED("API002"),
	CODE_EXCPTION("API001"),
	
	ERROR_CODE ("errorCode="),
	ERROR_MESSAGE ("errorMessage="),
	STATUS_CODE_FAILED ("0"),
	STATUS_CODE_SUCCESS  ("1"),
	STATUS_CODE_EXCEPTION  ("2"),
	SERI_ADMIN_EXCEPTION  ("1001"), 
	TOTAL_MONTH("10"),
	
	REQUEST_INVALID_MESSAGE  ("Unable to get response"),
	REQUEST_SUCCESS_MESSAGE  ("Successfully get response"),
	
	EMAIL_AVAILABLE_KEY ("EMAIL-AVAILABLE"),
	EMAIL_RESEND_KEY ("EMAIL-RESEND"),
	COUNTRY_LIST_KEY ("COUNTRIES-LIST"),
	STATE_LIST_KEY ("STATES-LIST"),
	CITY_LIST_KEY ("CITIES-LIST"),
	GRADE_LIST_KEY ("GRADES-LIST"),
	EVENTS_LIST_KEY ("EVENTS-LIST"),
		
	EVENTTYPE_INVALID_CODE ("E101"),
	EVENTTYPE_INVALID_MESSAGE ("Event Type invalid, please send correct event"),
	
	ELEMENTS_INVALID_CODE ("E101"),
	ELEMENTS_INVALID_MESSAGE ("Event Type invalid, please send correct event"),
	
	VENDORS_INVALID_CODE ("E101"),
	VENDORS_INVALID_MESSAGE ("Vendor invalid, please send correct event"),
	
	ANSWER_INVALID_CODE ("A102"),
	ANSWER_INVALID_MESSAGE ("some error, answer not submitted"),
	
	ANSWER_SUCCESS_CODE ("A103"),
	ANSWER_SUCCESS_MESSAGE ("Thanks for your submition !"),
	
	ANSWER_NO_CODE ("E104"),
	ANSWER_NO_MESSAGE ("Answer not available"),
	
	
	EVENTTYPE_NO_CODE ("E105"),
	EVENTTYPE_NO_MESSAGE ("EventId not available"),
	EVENTTYPE_ALREADY_MESSAGE ("Event already exist !"),
	
	EVENTTYPE_SUCCESS_CODE ("E106"),
	EVENTTYPE_SUCCESS_MESSAGE ("Event save successfully"),
	
	QUESTION_TYPE_SUCCESS_CODE ("Q101"),
	QUESTION_TYPE_SUCCESS_MESSAGE ("Question save successfully"),
	
	QUESTION_TYPE_INVALID_CODE ("Q102"),
	QUESTION_TYPE_INVALID_MESSAGE ("Question Type invalid, please send correct question Data"),
	
	SEND_FEEDBACK_DETAILS_TYPE_SUCCESS_CODE ("F101"),
	SEND_FEEDBACK_DETAILS_TYPE_SUCCESS_MESSAGE ("Feedback details saved  or Update successfully"),
	
	SEND_FEEDBACK_DETAILS_TYPE_INVALID_CODE ("F102"),
	SEND_FEEDBACK_DETAILS_TYPE_INVALID_MESSAGE ("Feedback details invalid, please send correct  Data"),
	
	LOG_MISMATCH  ("danger"),
	
	SUCCESS_CODE("200"),
	FAILD_CODE("100"),
	REQUEST_ERROR_CODE ("0000"),
	REQUEST_SUCCESS_CODE ("1111"),
	
	
	JSON  ("JSON"),
	SORT_DIRECTION  ("asc"),
	SECRET_KEY  ("ABCDEFGHIJKLMNOP"),
	PICKR_ACTIVE  ("Y"),
	PICKR_DELETED  ("N"),
	BASE_DTO  ("baseDTO"),
	
	
	DATE_FORMAT("dd-MM-yyyy"),
	DATE_FORMAT_WITH_TIMESTAMP("dd-MM-yyyy HH:mm:ss"),
	DATE_FORMAT_WITH_TIMESTAMP_SEC("yyyy-MM-dd HH:mm:ss")
	;
	
	private Integer activityValue;
	private String value;

	private ApiConstant(final String value){
		this.value = value;
	}
	private ApiConstant(final Integer activityValue){
		this.activityValue = activityValue;
	}

	public Integer getactivityValue() {
		return activityValue;
	}
	public String getValue() {
		return value;
	}
}
