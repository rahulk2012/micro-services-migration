package com.servicer.dto.use;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Builder
@Getter
@Setter
@ToString
public class SignupResponse {
	private String encryptedUserId;
	private String message;
	private String statusCode;
	private String status;
	private String error;
	private String path;
	private String timestamp;	
}