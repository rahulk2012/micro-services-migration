package com.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
public class EventTypeDTO {

	@ApiModelProperty(notes = "eventId", name="eventId", required=true, value="1")
	private Integer eventId;
	@ApiModelProperty(notes = "vendorName", name="vendorName", required=true, value="vendorName")
	private String vendorName;
	@ApiModelProperty(notes = "eventName", name="eventName", required=true, value="eventName")
	private String eventName;
	
}
