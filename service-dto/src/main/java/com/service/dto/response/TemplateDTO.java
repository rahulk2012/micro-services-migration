package com.service.dto.response;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateDTO {
	private String template;
	private String sendMailSubject;
	private String vendorId;
	private String eventId;
	private String templateId;

}
