package com.service.dto.response;

import com.service.common.dto.MasterDTO;
import com.service.common.dto.ResponseStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SaveEventResponse {
	private ResponseStatus responseStatus;
	public MasterDTO events;
}
