package com.service.dto.response;

import java.util.List;

import com.service.common.dto.ResponseStatus;
import com.service.dto.QuestionContentDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionLableListResponse {
	private ResponseStatus responseStatus;
	private List<QuestionContentDTO> questionContentDTO;
}
