package com.service.dto.response;

import java.util.List;

import com.service.common.dto.ResponseStatus;
import com.service.dto.QuestionDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionResponse {
	private ResponseStatus responseStatus;
	private String eventType;
	public List<QuestionDTO> questionList;
}
