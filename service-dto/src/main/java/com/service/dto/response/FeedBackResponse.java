package com.service.dto.response;

import java.util.List;

import com.service.common.dto.ResponseStatus;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class FeedBackResponse {
	private ResponseStatus responseStatus;
	public List<String> feedbacks;
}
