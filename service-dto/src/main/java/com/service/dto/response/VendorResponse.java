package com.service.dto.response;

import java.util.List;

import com.service.common.dto.ResponseStatus;
import com.service.dto.VendorDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VendorResponse {
	private ResponseStatus responseStatus;
	private String eventType;
	public List<VendorDTO> vendorList;
}
