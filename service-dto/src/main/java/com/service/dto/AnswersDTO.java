package com.service.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
public class AnswersDTO {

	@ApiModelProperty(notes = "eventType", name="eventType", required=true, value="eventType")
	private String eventType;
	@ApiModelProperty(notes = "userId", name="userId", required=true, value="userId")
	private String userId;
	private List<AnswerContentDTO> answers;
	
}
