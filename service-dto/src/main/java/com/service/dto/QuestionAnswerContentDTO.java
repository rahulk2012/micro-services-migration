package com.service.dto;

import lombok.AllArgsConstructor;
//import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionAnswerContentDTO {
//	@ApiModelProperty(notes = "questionId", name="questionId", required=true, value="1")
	private Integer questionId;
//	@ApiModelProperty(notes = "userId", name="userId", required=true, value="userId")
	private String userId;
//	@ApiModelProperty(notes = "eventType", name="eventType", required=true, value="eventType")
	private String eventType;
//	@ApiModelProperty(notes = "question", name="question", required=true, value="question")
	private String question;
//	@ApiModelProperty(notes = "answers", name="answers", required=true, value="answers")
	private String answers;
	
}
