package com.service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDataRequest {

	private Integer eventId;
	private Integer questionType;
	private String question;
	private Integer elementId;
	private Integer mendetory;
	private Integer parentId;
	private String emojiClass;
	private String userId;
	
	
	
	
}
