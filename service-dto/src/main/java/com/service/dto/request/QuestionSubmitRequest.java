package com.service.dto.request;

import java.util.List;

import com.service.dto.QuestionContentDTO;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionSubmitRequest {
	private Integer questionId;
	private String vendorId;
	private String eventId;
	private Integer questionType;
	private String question;
	private Integer elementId;
	private Integer parentId;
	private Integer mendetory;
	private String emojiClass;
	private List<QuestionContentDTO> questionContentDTO;
}
