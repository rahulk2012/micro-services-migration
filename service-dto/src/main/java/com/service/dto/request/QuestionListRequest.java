package com.service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionListRequest {
	private String vendorId;
	private Integer questionId;
	private String eventId;
	private Integer parentId;
	private Integer startLimit;
	private Integer endLimit;
}
