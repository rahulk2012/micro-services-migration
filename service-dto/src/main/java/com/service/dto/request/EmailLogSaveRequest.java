package com.service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class EmailLogSaveRequest {
	private String emailLogId;
	private String vendorId;
	private String eventId;
	private String encryptedId;
	private String emailId;
	private String sendToCC;
	private String sendToBCC;
}
