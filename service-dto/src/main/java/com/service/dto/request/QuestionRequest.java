package com.service.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionRequest {
	
	@ApiModelProperty(notes = "eventId", name="eventId", required=true, value="eventId")
	private String eventId;
	@ApiModelProperty(notes = "vendorId", name="vendorId", required=true, value="vendorId")
	private String vendorId;
	@ApiModelProperty(notes = "eventType", name="eventType", required=true, value="eventType")
	private String eventType;
	@ApiModelProperty(notes = "questionType", name="questionType", required=true, value="1")
	private Integer questionType;
	@ApiModelProperty(notes = "parentId", name="parentId", required=true, value="1")
	private Integer parentId;
	@ApiModelProperty(notes = "userId", name="userId", required=true, value="userId")
	private String userId;
	@ApiModelProperty(notes = "startLimit", name="startLimit", required=true, value="1")
	private Integer startLimit;
	@ApiModelProperty(notes = "endLimit", name="endLimit", required=true, value="1")
	private Integer endLimit;
	
	
	
}
