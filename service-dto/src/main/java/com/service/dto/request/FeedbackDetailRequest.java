package com.service.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FeedbackDetailRequest {
	
	@ApiModelProperty(notes = "feedbackId", name="feedbackId", required=true, value="feedbackId")
	private String feedbackId;
	
	@ApiModelProperty(notes = "vendorId", name="vendorId", required=true, value="vendorId")
	private String vendorId;
	
	@ApiModelProperty(notes = "eventId", name="eventId", required=true, value="eventId")
	private String eventId;
	
	@ApiModelProperty(notes = "emailId", name="emailId", required=true, value="emailId")
	private String  emailId;
	
}
