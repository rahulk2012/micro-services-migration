package com.service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class ElementTypeRequest {

	private Integer vendorId;
	private Integer elementId;
	private String elementName;
	private Integer startLimit;
	private Integer endLimit;
	
	
}
