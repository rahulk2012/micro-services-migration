package com.service.dto.request;

import java.util.List;

import com.service.dto.AnswerContentDTO;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class AnswersRequest {

	@ApiModelProperty(notes = "Name of the Event", name="eventType", required=true, value="Event")
	private String eventType;
	@ApiModelProperty(notes = "Emoji", name="emoji", required=true, value="1")
	private Integer emoji;
	@ApiModelProperty(notes = "User Id", name="userId", required=true, value="1")
	private String userId;
	private List<AnswerContentDTO> answers;
	
	
	
}
