package com.service.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class VendorRequest {
	@ApiModelProperty(notes = "Vendor Id", name="vendorId", required=true, value="1")
	private String vendorId;
	@ApiModelProperty(notes = "Vendor Key", name="vendorKey", required=true, value="Vendor Key")
	private String vendorKey;
	
	private Integer startLimit;
	private Integer endLimit;
}
