package com.service.dto.request;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class TemplateSaveRequest {
	private String vendorId;
	private String eventId;
	private String emailSubject;
	private Integer templateType;
	private String emailContent;
	private String templateId;
	private String sendMailSubject;
}
