package com.service.dto.request;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@AllArgsConstructor
public class FeedBackRequest {
	@ApiModelProperty(notes = "eventId", name="eventId", required=true, value="eventId")
	private String eventId;
	@ApiModelProperty(notes = "vendorId", name="vendorId", required=true, value="vendorId")
	private String vendorId;
}
