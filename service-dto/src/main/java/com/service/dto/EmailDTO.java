package com.service.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EmailDTO {
	private Integer emailId;
	private String userName;
	//private String userEmail;
	private String title;
	private String userImage;
	private String date;
	private String readStatus;
	private String fromId;
}