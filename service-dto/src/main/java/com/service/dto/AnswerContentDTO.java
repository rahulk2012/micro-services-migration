package com.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
public class AnswerContentDTO {

	@ApiModelProperty(notes = "questionId", name="questionId", required=true, value="1")
	private Integer questionId;
	@ApiModelProperty(notes = "answer", name="answer", required=true, value="answer")
	private String answer;
	
}
