package com.service.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionContentDTO {

	@ApiModelProperty(notes = "questionContentId", name="questionContentId", required=true, value="1")
	private Integer questionContentId;
	@ApiModelProperty(notes = "questionId", name="questionId", required=true, value="1")
	private Integer questionId;
	@ApiModelProperty(notes = "questionLable", name="questionLable", required=true, value="questionLable")
	private String questionLable;
	@ApiModelProperty(notes = "rightAnswer", name="rightAnswer", required=true, value="1")
	private Integer rightAnswer;
	@ApiModelProperty(notes = "emojiClass", name="emojiClass", required=true, value="emojiClass")
	private String emojiClass;
	
	
}
