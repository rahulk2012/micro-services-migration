package com.service.dto.requesthandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class RequestData {
	private String requestKey;
	private String requestValue;
	private String requestExtra;
	private String requestExtra1;
	private String requestExtra2;
	private String referenceNumber;
	private String role;
	private Integer schoolId;
	private String resultStatus;
	private Integer entityId;
	private Integer standardId;
}