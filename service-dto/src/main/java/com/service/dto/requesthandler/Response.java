package com.service.dto.requesthandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Response {
	private String status;
	private String userType;
	private String statusCode;
	private String message;
	private String extra;
	private String extra1;
	private Integer notificationStatus ;
}