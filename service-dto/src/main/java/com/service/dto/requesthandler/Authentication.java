package com.service.dto.requesthandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class Authentication {
	private String hash;
	private String userType;
	private Integer userId;
	private Integer studentId;
	private Integer teacherId;
	private Integer parentId;
	private Integer stepNum;
}
