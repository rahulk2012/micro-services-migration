package com.service.dto.requesthandler;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class ResponseData {
	private Integer userId;
	private Integer studentId;
	private String path;
	private String status;
	private String statusCode;
	private String message;
}