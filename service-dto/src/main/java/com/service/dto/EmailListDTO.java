package com.service.dto;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class EmailListDTO {
	private Integer emailId;
	private String emailType;
	private String userName;
	private String label;
	private String repliedMailId;
	private String toId;
	private String fromId;
	private String bccId;
	private String ccId;
	private String subject;
	private String msg;
	private Integer readStatus;
	private Integer repliedStatus;
	private Integer senderDelStatus;
	private String groupMailId;
	private Date sentDate;
	private Date readDate;
	private Date senderDelDate;
	private Integer reciverDelStatus;
	private Date reciverDelDate;
	private Integer draftStatus;
}