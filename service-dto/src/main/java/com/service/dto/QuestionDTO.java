package com.service.dto;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
@Builder
@Data
@AllArgsConstructor
@NoArgsConstructor
public class QuestionDTO {

	@ApiModelProperty(notes = "questionId", name="questionId", required=true, value="1")
	private Integer questionId;
	@ApiModelProperty(notes = "question", name="question", required=true, value="question")
	private String question;
	@ApiModelProperty(notes = "elementId", name="elementId", required=true, value="1")
	private Integer elementId;
	@ApiModelProperty(notes = "elementName", name="elementName", required=true, value="elementName")
	private String elementName;
	@ApiModelProperty(notes = "parentId", name="parentId", required=true, value="1")
	private Integer parentId;
	@ApiModelProperty(notes = "mendetory", name="mendetory", required=true, value="1")
	private Integer mendetory;
	private List<QuestionContentDTO> questionContentDTO;
	
}
