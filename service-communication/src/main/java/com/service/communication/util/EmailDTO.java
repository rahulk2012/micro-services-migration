package com.service.communication.util;

import java.io.Serializable;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class EmailDTO implements Serializable {
	private static final long serialVersionUID = 1L;
	private String senderAddress;
	private String[] recepientAddresses;
	private String message;
	private String subject;
	private String alias;
	private String[] ccAddresses;
	private String[] bccAddresses;
	private String contentType;
	private List<EmilFileType> attachements;
	private String senderVendor;
	private String senderEmail;
	private String senderPassword;
	private String senderHost;
	private String senderPort;
	private String bypass;
}