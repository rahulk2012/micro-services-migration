package com.service.communication.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Multipart;
import javax.mail.PasswordAuthentication;
import javax.mail.SendFailedException;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;

import org.apache.commons.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.sendgrid.Attachments;
import com.sendgrid.Content;
import com.sendgrid.Email;
import com.sendgrid.Mail;
import com.sendgrid.Method;
import com.sendgrid.Personalization;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

import io.netty.util.internal.StringUtil;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class SendMail {
	
	@Autowired
	RestTemplateUtil restTemplateUtil;
	
	public boolean postMail(final EmailDTO email) throws Exception {
		log.info("postMail() :: " + email);
		try {
			final String[] recepients = email.getRecepientAddresses();
			final String messageTxt = email.getMessage();
			if ((messageTxt == null) || (messageTxt.trim().length() <= 0)) {
				throw new Exception("Message text is blank.");
			}

			final Properties mailProperties = new Properties();
			mailProperties.setProperty("mail.transport.protocol", "smtps");
			mailProperties.put("mail.host", email.getSenderHost());
			mailProperties.put("mail.smtp.port", email.getSenderPort());
			mailProperties.put("mail.smtp.starttls.enable", "true");
			mailProperties.put("mail.smtp.auth", "true");
			mailProperties.put("mail.smtp.socketFactory.port", email.getSenderPort());

			final Authenticator auth = new SMTPAuthenticator(email.getSenderEmail(), email.getSenderPassword());
			final Session mailSession = Session.getInstance(mailProperties, auth);
			mailSession.setDebug(true);
			final Message message = new MimeMessage(mailSession);
			final InternetAddress senderAddress = new InternetAddress(email.getSenderAddress(), email.getAlias());
			message.setFrom(senderAddress);
			final InternetAddress[] recepientAddresses = new InternetAddress[recepients.length];
			for (int i = 0; i < recepients.length; i++) {
				recepientAddresses[i] = new InternetAddress(recepients[i]);
			}
			message.setRecipients(Message.RecipientType.TO, recepientAddresses);
			if ((email.getCcAddresses() != null) && (email.getCcAddresses().length > 0)) {
				if(email.getCcAddresses()!=null) {
					final String[] ccRecepients = email.getCcAddresses();
					if(ccRecepients!=null && ccRecepients.length>0) {
						final InternetAddress[] ccRecepientAddresses = new InternetAddress[ccRecepients.length];
						for (int i = 0; i < ccRecepientAddresses.length; i++) {
							if(ccRecepients[i]!=null) {
								ccRecepientAddresses[i] = new InternetAddress(ccRecepients[i]);
							}
						}
						message.setRecipients(Message.RecipientType.CC, ccRecepientAddresses);
					}
				}
			}
			if ((email.getBccAddresses() != null) && (email.getBccAddresses().length > 0)) {
				final String[] bccRecepients = email.getBccAddresses();
				final InternetAddress[] bccRecepientAddresses = new InternetAddress[bccRecepients.length];
				for (int i = 0; i < bccRecepients.length; i++) {
					bccRecepientAddresses[i] = new InternetAddress(bccRecepients[i]);
				}
				message.setRecipients(Message.RecipientType.BCC, bccRecepientAddresses);
			}
			message.setSubject(email.getSubject());
//            final List<File> attachmentArray = email.getAttachements();
			if (email.getAttachements() != null && !email.getAttachements().isEmpty()) {
				final MimeBodyPart messageBodyPart = new MimeBodyPart();
				if ((email.getContentType() != null) && !email.getContentType().isEmpty()) {
					messageBodyPart.setContent(messageTxt, email.getContentType());
				} else {
					messageBodyPart.setContent(messageTxt, "text/html");
				}
				final List<MimeBodyPart> listBodyPart = new ArrayList<>();
				int position = 0;
				for (EmilFileType emilFileType : email.getAttachements()) {
					listBodyPart.add(new MimeBodyPart());
					listBodyPart.get(position).attachFile(emilFileType.getFile());
					position++;
				}
				final Multipart multipart = new MimeMultipart();
				multipart.addBodyPart(messageBodyPart);
				for (final MimeBodyPart aListBodyPart : listBodyPart) {
					multipart.addBodyPart(aListBodyPart);
				}
				message.setContent(multipart);
			} else {
				if ((email.getContentType() != null) && !email.getContentType().isEmpty()) {
					message.setContent(messageTxt, email.getContentType());
				} else {
					message.setContent(messageTxt, "text/html");
				}
			}

			log.info("email.getBypass() ::" + email.getBypass());
			if ("Y".equalsIgnoreCase(email.getBypass())) {
				log.info("Email not sent due to flag via postMail ::" + (Arrays.asList(email.getRecepientAddresses())));
			}else {
				log.info("Email send successfully via postMail ::" + (Arrays.asList(email.getRecepientAddresses())));
				Transport.send(message);
			}
			return true;
		} catch (final AddressException exp) {
			log.error("SendMail.java LNo : 173 : Exception Caught", exp);
		} catch (final SendFailedException exp) {
			log.error("SendMail.java LNo : 176 : Exception Caught", exp);
		} catch (final MessagingException exp) {
			log.error("SendMail.java LNo : 179 : Exception Caught", exp);
		} catch (final Exception exp) {
			log.error("SendMail.java LNo : 182 : Exception Caught", exp);
		}
		return false;
	}

	private class SMTPAuthenticator extends javax.mail.Authenticator {
		private String emailid;
		private String pass;

		public SMTPAuthenticator(String email, String password) {
			this.emailid = email;
			this.pass = password;
		}

		@Override
		protected PasswordAuthentication getPasswordAuthentication() {
			return new PasswordAuthentication(emailid, pass);
		}
	}
    
    public boolean postMailViaSarv(final EmailDTO email) throws Exception {
    	log.info("postMailViaSarv() :: "+email);
        try {
        	JsonObject main = new  JsonObject();
        	main.addProperty("owner_id", "70690187");
        	main.addProperty("token", "PcuyCP7keb6eX00mNnlVjESX");
        	main.addProperty("smtp_user_name", "smtp59040598");
        	
        	JsonObject message = new  JsonObject();
        	message.addProperty("html", email.getMessage().replaceAll("\"", "'"));
        	message.addProperty("text", "");
        	message.addProperty("subject", email.getSubject());
        	message.addProperty("from_email", "noreply@internationalschooling.org");
        	message.addProperty("from_name", "Team International Schooling");
        	JsonArray to = new JsonArray();
        	final String[] recepients = email.getRecepientAddresses();
        	if(recepients!=null && recepients.length>0) {
        		for (int i = 0; i < recepients.length; i++) {
        			JsonObject single = new  JsonObject();
        			single.addProperty("email", recepients[i]);
        			single.addProperty("name", "");
        			single.addProperty("type", "to");
        			to.add(single);
        		}
        	}
        	final String[] cc = email.getCcAddresses();
        	if(cc!=null && cc.length>0) {
        		for (int i = 0; i < cc.length; i++) {
        			JsonObject single = new  JsonObject();
        			single.addProperty("email", cc[i]);
        			single.addProperty("name", "");
        			single.addProperty("type", "cc");
        			to.add(single);
        		}
        	}
        	final String[] bcc = email.getBccAddresses();
        	if(bcc!=null && bcc.length>0) {
        		for (int i = 0; i < bcc.length; i++) {
        			JsonObject single = new  JsonObject();
        			single.addProperty("email", bcc[i]);
        			single.addProperty("name", "");
        			single.addProperty("type", "bcc");
        			to.add(single);
        		}
        	}
        	message.add("to", to);
        	
        	JsonObject headers = new  JsonObject();
        	headers.addProperty("Reply-To", "noreply@internationalschooling.org");
        	headers.addProperty("X-Unique-Id","id");
        	message.add("headers", headers);
        	if (email.getAttachements() != null && !email.getAttachements().isEmpty()) {
        		JsonArray attachments = new JsonArray();
        		for (EmilFileType emilFileType : email.getAttachements()) {
        			JsonObject singleFile = new  JsonObject();
        			singleFile.addProperty("type", emilFileType.getContentType());
        			log.info("emilFileType.getContentType()"+emilFileType.getContentType());
        			singleFile.addProperty("name", emilFileType.getName());
        			log.info("emilFileType.getName()"+emilFileType.getName());
        			String content = encodeFileToBase64Binary(emilFileType.getFile().getAbsolutePath());
        			log.info("emilFileType.getFile().getAbsolutePath()"+emilFileType.getFile().getAbsolutePath());
        			log.info("content"+content);
        			singleFile.addProperty("content", content);
        			log.info("content"+content);
        			attachments.add(singleFile);
        			log.info("singleFile"+singleFile);
                }
        		message.add("attachments", attachments);
        		log.info("attachments"+attachments);
        	}
        	
        	main.add("message", message);
        	log.info("message postMailViaSarv"+ message);
        	log.info("email.getBypass() ::" + email.getBypass());
			if (!"N".equalsIgnoreCase(email.getBypass())) {
        		log.info("postMailViaSarv:: email sent successfully via postMailViaSarv");
        		return restTemplateUtil.sendEmailToSarv(main.toString());
        	}
        	log.info("postMailViaSarv:: email not send via postMailViaSarv");
        	return true;
        } catch (final Exception exp) {
            log.error("postMailViaSarv.java LNo : 182 : Exception Caught", exp);
        }
        return false;
    }
    
    private String encodeFileToBase64Binary(String fileName){
		try {
			byte[] fileContent = Files.readAllBytes(new File(fileName).toPath());
			String content = Base64.encodeBase64String(fileContent);
			log.info("encodeFileToBase64Binary :: content "+content);
			return content;
		} catch (IOException e) {
			log.error("encodeFileToBase64BinaryException Caught", e);
		}
		return StringUtil.EMPTY_STRING;
	}
    public boolean sendEmailViaSendgrid(final EmailDTO email) {
		try {
			
			Mail mail = new Mail();

		    Email fromEmail = new Email();
		    fromEmail.setName(email.getAlias());
		    fromEmail.setEmail(email.getSenderAddress());
		    mail.setFrom(fromEmail);

		    Content content = new Content();
		    content.setType("text/html");
		    content.setValue(email.getMessage());
		    mail.addContent(content);
		    
		    mail.setSubject(email.getSubject());

		    
		    Personalization personalization = new Personalization();
		    final String[] recepients = email.getRecepientAddresses();
			if (recepients != null && recepients.length > 0) {
				for (int i = 0; i < recepients.length; i++) {
					Email to = new Email();
				    to.setName("");
				    to.setEmail(recepients[i]);
				    personalization.addTo(to);
				}
			}
			final String[] cc = email.getCcAddresses();
			if (cc != null && cc.length > 0) {
				for (int i = 0; i < cc.length; i++) {
					Email ccs = new Email();
				    ccs.setName("");
				    ccs.setEmail(cc[i]);
				    personalization.addCc(ccs);
				}
			}
			final String[] bcc = email.getBccAddresses();
			if (bcc != null && bcc.length > 0) {
				for (int i = 0; i < bcc.length; i++) {
					Email bccs = new Email();
				    bccs.setName("");
				    bccs.setEmail(bcc[i]);
				    personalization.addBcc(bccs);
				}
			}
		  
		    personalization.setSubject(email.getSubject());
		    personalization.addHeader("X-Test", "test");
		    personalization.addHeader("X-Mock", "true");
		    personalization.addSubstitution("%name%", "Example User");
		    personalization.addSubstitution("%city%", "Riverside");
		    personalization.addCustomArg("user_id", "343");
		    personalization.addCustomArg("type", "marketing");
//		    personalization.setSendAt(new Date().getTime());
		    mail.addPersonalization(personalization);
		    if (email.getAttachements() != null && !email.getAttachements().isEmpty()) {
				for (EmilFileType emilFileType : email.getAttachements()) {
					log.info("emilFileType.getContentType()" + emilFileType.getContentType());
					log.info("emilFileType.getName()" + emilFileType.getName());
					log.info("emilFileType.getFile().getAbsolutePath()" + emilFileType.getFile().getAbsolutePath());
					String fileContent = encodeFileToBase64Binary(emilFileType.getFile().getAbsolutePath());
					log.info("content" + content);
					Attachments attachments = new Attachments();
					attachments.setContent(fileContent);
					attachments.setType(emilFileType.getContentType());//"application/pdf"
					attachments.setFilename(emilFileType.getName());
					attachments.setDisposition("attachment");
					attachments.setContentId("Banner");
			        mail.addAttachments(attachments);
			        
				}
			}
		    
		    SendGrid sg = new SendGrid(email.getSenderPassword());
		    sg.addRequestHeader("X-Mock", "true");

		    Request request = new Request();
		    request.setMethod(Method.POST);
		    request.setEndpoint("mail/send");
		    request.setBody(mail.build());
		    Response response = sg.api(request);
		    log.info("response.getStatusCode() "+response.getStatusCode());
		    log.info("response.getBody(): "+response.getBody());
		    log.info("response.getHeaders(): "+response.getHeaders());
			return true;
		} catch (Exception e) {
			log.error("sendEmailViaSendgrid Caught", e);
		}
		return false;
	}
	
	public boolean sendEmailViaSendgrid1(final EmailDTO email) {
		try {
			SendGrid sg = new SendGrid(email.getSenderPassword());
			Request request = new Request();
			request.setMethod(Method.POST);
			request.setEndpoint("mail/send");
			request.setBody(
					"{\"personalizations\":[{\"to\":[{\"email\":\"admin@seriindia.org\"},{\"email\":\"kedar@seriindia.org\"}],"
					+ "\"subject\":\""+email.getSubject()+"\"}],"
					+ "\"from\":{\"email\":\"noreply@seriindia.org\"},"
					+ "\"content\":[{\"type\":\""+email.getContentType()+"\","
					+ "\"value\": \"This is test message\"}]}");
			Response response = sg.api(request);
			System.out.println(response.getStatusCode());
			System.out.println(response.getBody());
			System.out.println(response.getHeaders());
			return true;
		} catch (IOException e) {
			log.error("sendEmailViaSendgrid Caught", e);
		}
		return false;
	}
}