package com.service.communication.util;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.X509Certificate;

import javax.net.ssl.SSLContext;

import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.conn.ssl.TrustStrategy;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.service.dto.EmailSendViaSarv;
import com.service.dto.EmailVerifyDTO;
import com.service.dto.SMSSendVia91;

import lombok.extern.slf4j.Slf4j;
@Slf4j
@Service
public class RestTemplateUtil {
	@Autowired
	private RestTemplate restTemplate;

	@Value("${BASE_URL_EMAIL_VERIFY}")
    String BASE_URL_EMAIL_VERIFY;

    @Value("${EMAIL_SECRET_KEY}")
    String EMAIL_SECRET_KEY;

    @Value("${BASE_URL_VIA_SARV_EMAIL}")
    String BASE_URL_VIA_SARV_EMAIL;

    @Value("${SMS_SERVICE_BY_PASS}")
    boolean SMS_SERVICE_BY_PASS;
    
	@Bean
	public RestTemplate restTemplate() throws KeyStoreException, NoSuchAlgorithmException, KeyManagementException {
		TrustStrategy acceptingTrustStrategy = (X509Certificate[] chain, String authType) -> true;

		SSLContext sslContext = org.apache.http.ssl.SSLContexts.custom().loadTrustMaterial(null, acceptingTrustStrategy)
				.build();

		SSLConnectionSocketFactory csf = new SSLConnectionSocketFactory(sslContext);

		CloseableHttpClient httpClient = HttpClients.custom().setSSLSocketFactory(csf).build();

		HttpComponentsClientHttpRequestFactory requestFactory = new HttpComponentsClientHttpRequestFactory();

		requestFactory.setHttpClient(httpClient);
		RestTemplate restTemplate = new RestTemplate(requestFactory);
		return restTemplate;
	}

	public EmailVerifyDTO getEmailVerifyResponse(String emailId) {
		ResponseEntity<EmailVerifyDTO> response = restTemplate
				.getForEntity(BASE_URL_EMAIL_VERIFY + EMAIL_SECRET_KEY +emailId, EmailVerifyDTO.class);
		log.info("getEmailVerifyResponse(String emailId) response.getBody() :: " + response.getBody());
		return response.getBody();
	}
	
	public boolean sendEmailToSarv(String message) {
		log.info("rsendEmailToSarv :: message :: " + message);
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);

		HttpEntity<String> entity = new HttpEntity<String>(message.toString(), headers);
//		RestTemplate restTemplate = new RestTemplate();
		ResponseEntity<EmailSendViaSarv> response = restTemplate
				.postForEntity(BASE_URL_VIA_SARV_EMAIL, entity, EmailSendViaSarv.class);
		if(response!=null && response.getBody()!=null) {
			log.info("sendEmailToSarv(String message) :: response.getBody() :: " + response.getBody());
			EmailSendViaSarv responseBody = response.getBody();
			if(responseBody!=null 
					&& ("success".equalsIgnoreCase(responseBody.getStatus()) || "queued".equalsIgnoreCase(responseBody.getStatus()))) {
				return true;
			}
		}
		return false;
	}
	
	public SMSSendVia91 sendSMSVia91(String message, String isdCode) {
		log.info("sendSMSVia91 :: message :: " + message+" isdCode: "+isdCode);
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		headers.set("authkey", "129018AOvxDPw0zxn5dd23e7a");
		
		log.info("sendSMSVia91 :: main :: " + message.toString());
		HttpEntity<String> entity = new HttpEntity<String>(message.toString(), headers);
		ResponseEntity<SMSSendVia91> response = restTemplate
				.postForEntity("https://api.msg91.com/api/v2/sendsms?country="+isdCode, entity, SMSSendVia91.class);
		if(response!=null && response.getBody()!=null) {
			log.info("sendSMSVia91(String message) :: response.getBody() :: " + response.getBody());
			SMSSendVia91 responseBody = response.getBody();
			return responseBody;
//			if(responseBody!=null && ("success".equalsIgnoreCase(responseBody.getType()) )) {
//				return true;
//			}
		}
		return null;
	}
	
	public String sendSMSVia91Get(String requestUrl) {
		log.info("sendSMSVia91Get :: requestUrl :: " + requestUrl+" SMS_SERVICE_BY_PASS: "+SMS_SERVICE_BY_PASS);
		if(!SMS_SERVICE_BY_PASS) {
			ResponseEntity<String> response = restTemplate.getForEntity(requestUrl, String.class);
			if(response!=null && response.getBody()!=null) {
				log.info("sendSMSVia91Get(String requestUrl) :: response.getBody() :: " + response.getBody());
				return response.getBody();
			}
		}
		return null;
	}
}