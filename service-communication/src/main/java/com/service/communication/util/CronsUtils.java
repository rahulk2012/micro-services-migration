package com.service.communication.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.response.CronsListDTO;
import com.service.dto.response.CronsResponse;
import com.service.model.communication.CommunicationChannel;
import com.service.model.communication.CommunicationEmailLog;
import com.service.model.communication.CommunicationSettings;
import com.service.model.communication.Crons;
import com.service.model.communication.CronsExecution;
import com.service.model.communication.Template;
import com.service.service.communication.CommunicationChanelService;
import com.service.service.communication.CommunicationEmailLogService;
import com.service.service.communication.CommunicationSettingsService;
import com.service.service.communication.CronsExecutionService;
import com.service.service.communication.CronsService;
import com.service.service.communication.TemplateService;

import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class CronsUtils{
	
	@Value("${EXECUTE_SCHEDULAR_FLAG}")
    boolean EXECUTE_SCHEDULAR_FLAG;
	
	@Autowired
	CommunicationEmailLogService commnctionElLogService;
	
	@Autowired
	EmailUtils emailUtils;
	
	@Autowired
	TemplateService templateService;
	
	@Autowired
	CommunicationSettingsService communicationSettingsService;
	
	@Autowired
	CommunicationChanelService communicationChanelService;
	
	@Autowired
	CronsExecutionService  cronsExecutionService; 
	
	@Autowired
	CronsService cronsService;
	
//	@Scheduled(cron = "0 30 11 * * *") //for every day 11:30
	public String callCronSchedulerForUnsendEmail(boolean manuallyCalled) {
		log.info("callCronSchedulerForInstalmentPaymentReminder()... cron initiated");
		try {
			if(EXECUTE_SCHEDULAR_FLAG) {
				String cronId="99ae1f7b-9b8e-4125-a5c3-a74ce989f977";
				CronsExecution cronsExecution =new CronsExecution();
				cronsExecution.setCronId(cronId);
				cronsExecution.setStartDateTime(new Date());
				cronsExecution.setCronManually(manuallyCalled?"Y":"N");
				cronsExecutionService.save(cronsExecution);
				
				List<Object> vendors=  commnctionElLogService.distinctVendorList();
				for(Object vendor : vendors) {
					String vendorId = vendor.toString();
					CommunicationSettings communicationSettings = communicationSettingsService.findByVendorId(vendorId);
					CommunicationChannel communicationChannel = communicationChanelService.getCommunicationChanel(communicationSettings.getChannelId());
					List<Object> events =commnctionElLogService.distinctEventbyVendorList(vendorId);
					for(Object event : events) {
						String eventId = event.toString();
						Template template = templateService.getTemplate(vendorId, eventId, 1);
						List<CommunicationEmailLog>  communicationEmailLog =commnctionElLogService.emailSendList(vendorId, eventId);
						if(!communicationEmailLog.isEmpty()) {
							for(CommunicationEmailLog emailLog :communicationEmailLog ) {
								emailUtils.sendCronsUnsendEmailToUser(emailLog, template, communicationSettings, communicationChannel);
							}
						}
					}
				}
				cronsExecution.setEndDateTime(new Date());
				cronsExecutionService.save(cronsExecution);
				log.info("callCronSchedulerForUnsendEmail()... Unsend Email cron run successfully"); 
			}
		}catch(Exception e) {
			log.info("callCronSchedulerForUnsendEmail()  :: ",e);
		}
		return "";
	}
	
	public CronsResponse getCronsList() {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue())
				.build();
		final CronsResponse response = CronsResponse.builder()
				.responseStatus(responseStatus)
				.build();
		try {
			String msg="";
			List<CronsListDTO> cronsDtoList=new ArrayList<>();
			List<Crons> cronsList= cronsService.getCrons();
			if(!cronsList.isEmpty()) {
				for (Crons cro : cronsList) {
					CronsListDTO cron = CronsListDTO.builder()
							.cronsId(cro.getId())
							.cronsName(cro.getCronName())
							.cronsUrl(cro.getCronUrl())
							.cronsStatus(cro.getStatus())
							 .build();
					cronsDtoList.add(cron);
				}	
			msg= "Crons Listing";
			}else {
				msg= "Crons List empty";
			}
			responseStatus.setCode(ApiConstant.STATUS_CODE_SUCCESS.getValue());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage(msg);
			response.setResponseStatus(responseStatus);
			response.setCronsListDTO(cronsDtoList);
			return response;
		}catch (Exception e) {
			log.info("getCronsList()"+e);
		}
		return response;
	}
}