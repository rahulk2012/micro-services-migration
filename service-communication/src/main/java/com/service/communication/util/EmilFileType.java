package com.service.communication.util;

import java.io.File;
import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Setter
@Getter
@ToString
@Builder
public class EmilFileType implements Serializable {
	private static final long serialVersionUID = 1L;
	private File file;
	private String name;
	private String contentType;
}