package com.service.communication.util;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.EmailLogSaveRequest;
import com.service.dto.request.EventRequest;
import com.service.dto.request.TemplateSaveRequest;
import com.service.dto.request.UpdateFeedbackDetailsRequest;
import com.service.dto.response.EmailLogSaveResponse;
import com.service.dto.response.SaveEventResponse;
import com.service.dto.response.TemplateDTO;
import com.service.dto.response.TemplateResponse;
import com.service.model.communication.CommunicationEmailLog;
import com.service.model.communication.Template;
import com.service.service.communication.CommunicationEmailLogService;
import com.service.service.communication.TemplateService;
import com.service.util.DateUtil;
import com.service.util.HeaderUtil;
import com.service.util.ValidatorUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class TemplateUtil {
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	TemplateService templateService;
	
	@Autowired
	CommunicationEmailLogService communicationEmailLogService;
	
	@Autowired
	HeaderUtil uiUtil;

	public TemplateResponse getEmptyTemplateResponse() {
		TemplateResponse response = TemplateResponse.builder()
				.responseStatus(getEmptyResponseStatus("FS001", "Unable to get Template")).build();
		return response;
	}

	public ResponseStatus getEmptyResponseStatus(String code, String message) {
		ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(code).message(message).build();
		return responseStatus;
	}
	
	public TemplateDTO getTemplate(Integer templateType, String vendorId, String eventId) {
		if(ValidatorUtil.isValid(vendorId)) {
			Template template = templateService.getTemplate(vendorId, eventId, templateType);
			if(template == null) {
				return null;
			}
			TemplateDTO templateDTO = TemplateDTO.builder()
					.vendorId(template.getVendorId().toString())
					.sendMailSubject(template.getTemplateSubject()!=null?template.getTemplateSubject():"")
					.eventId(template.getEventId().toString())
					.template(template.getTemplateContent())
					.templateId(template.getTemplateId())
					.build();
			return templateDTO;
		}else {
			return null;
		}
	}
	public TemplateResponse saveTemplateData(TemplateSaveRequest templateSaveRequest) {
		log.info("saveTemplateData(TemplateRequest templateRequest) :: " + templateSaveRequest);
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue())
				.build();
		final TemplateResponse response = TemplateResponse.builder()
				.responseStatus(responseStatus)
				.build();
		try {
			Template template= templateService.getTemplate(templateSaveRequest.getTemplateId());
			if(template==null) {
				if(!ValidatorUtil.isValid(templateSaveRequest.getEventId())) {
					EventRequest eventRequest = EventRequest.builder().vendorId(templateSaveRequest.getVendorId()).emailSubject(templateSaveRequest.getEmailSubject()).build();
					HttpEntity<EventRequest> request = new HttpEntity<>(eventRequest, uiUtil.getHeader());
					ResponseEntity<SaveEventResponse> eventResponse = restTemplate.postForEntity(
							ApiConstant.URL_COMMON_SERVICE.getValue()+"/save-event", request, SaveEventResponse.class);
					SaveEventResponse eventresp =eventResponse.getBody();
					log.info("eventresp ::"+eventresp);
					responseStatus =eventresp.getResponseStatus();
					if(responseStatus.getStatus().equalsIgnoreCase(ApiConstant.STATUS_MSG_FAILED.getValue())) {
						response.setResponseStatus(responseStatus);
						return response;
					}
					String eventId=eventResponse.getBody().getEvents().getKey();
					templateSaveRequest.setEventId(eventId);
				}
				template= new Template();
				template.setCreatedDate(new Date());
			}
			
			template.setActive("Y");
			template.setUpdatedDate(new Date());
			template.setDeleted("N");
			template.setEventId(templateSaveRequest.getEventId());
			template.setTemplateType(templateSaveRequest.getTemplateType());
			template.setTemplateContent(templateSaveRequest.getEmailContent());
			template.setVendorId(templateSaveRequest.getVendorId());
			template.setTemplateSubject(templateSaveRequest.getSendMailSubject());
			templateService.save(template);
			responseStatus.setCode(ApiConstant.STATUS_CODE_SUCCESS.getValue());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage("Template Data saved Succesfully");
			response.setResponseStatus(responseStatus);
			return response;
		} catch (Exception e) {
			log.info("saveTemplateData(TemplateRequest templateRequest)", e);
		}
		return response;
	}
	public EmailLogSaveResponse saveEmailLogData(EmailLogSaveRequest emailLogSaveRequest) {
		log.info("saveEmailLogData(EmailLogSaveRequest emailLogSaveRequest) :: " + emailLogSaveRequest);
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(ApiConstant.STATUS_CODE_FAILED.getValue())
				.build();
		final EmailLogSaveResponse response = EmailLogSaveResponse.builder()
				.responseStatus(responseStatus)
				.build();
		try {
			CommunicationEmailLog communicationEmailLog= communicationEmailLogService.getCommunicationEmailLog(emailLogSaveRequest.getEmailLogId());
			if(communicationEmailLog==null) {
				communicationEmailLog= new CommunicationEmailLog();
				communicationEmailLog.setCreatedDate(new Date());
			}
			communicationEmailLog.setSendTo(emailLogSaveRequest.getEmailId());
			communicationEmailLog.setUpdatedDate(new Date());
			communicationEmailLog.setVendorId(emailLogSaveRequest.getVendorId());
			communicationEmailLog.setEventId(emailLogSaveRequest.getEventId());
			communicationEmailLog.setIsMailSend("N");
			communicationEmailLog.setApiCall("N");
			if(emailLogSaveRequest.getEncryptedId()!=null) {
				communicationEmailLog.setEncryptedId(emailLogSaveRequest.getEncryptedId());
			}
			communicationEmailLogService.save(communicationEmailLog);
			responseStatus.setCode(ApiConstant.STATUS_CODE_SUCCESS.getValue());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage("Email Log Data saved Succesfully");
			response.setResponseStatus(responseStatus);
			return response;
		} catch (Exception e) {
			log.info("saveEmailLogData(EmailLogSaveRequest emailLogSaveRequest) :: " +e);
		}
		return response;
	}
	
	public boolean updateEmailLogData(CommunicationEmailLog emailLog) {
		try {
			CommunicationEmailLog communicationEmailLog= communicationEmailLogService.getCommunicationEmailLog(emailLog.getId());
			if(communicationEmailLog!=null) {
				communicationEmailLog.setIsMailSend("Y");
				communicationEmailLog.setMailSendDate(new Date());
				communicationEmailLog.setApiCall("N");
				communicationEmailLog.setUpdatedDate(new Date());
				communicationEmailLogService.save(communicationEmailLog);
			
				String sendDate= DateUtil.getDateInDesiredFormatAsString(communicationEmailLog.getMailSendDate(), "yyyy-MM-dd HH:mm:ss");
				log.info("sendDate In string: " +sendDate);
				
				UpdateFeedbackDetailsRequest updateFeedbackDetailsRequest = UpdateFeedbackDetailsRequest.builder().key("isMailSend").value(emailLog.getEncryptedId()).date(sendDate).build();
				HttpEntity<UpdateFeedbackDetailsRequest> request = new HttpEntity<>(updateFeedbackDetailsRequest, uiUtil.getHeader()); 
				ResponseEntity<ResponseStatus> responseStatus = restTemplate.postForEntity("http://localhost:8082/feedback-survey/api/v1/update-feedback-details", request, ResponseStatus.class);
//				ResponseEntity<ResponseStatus> responseStatus = restTemplate.postForEntity(ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue()+"/update-feedback-details", request, ResponseStatus.class);
				
				ResponseStatus response =responseStatus.getBody();
				log.info("response ::"+response);
				
				communicationEmailLog.setApiCall("Y");
				communicationEmailLog.setApiCallResponse(response.getStatus());
				communicationEmailLog.setUpdatedDate(new Date());
				communicationEmailLogService.save(communicationEmailLog);
			}
			return false;
		} catch (Exception e) {
			log.info("updateEmailLogData(CommunicationEmailLog emailLog) :: " +e);
			return false;
		}
	}
}
