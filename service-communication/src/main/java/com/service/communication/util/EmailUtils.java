package com.service.communication.util;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.service.communication.service.CommunicationManager;
import com.service.dto.EmailVerifyDTO;
import com.service.model.communication.CommunicationChannel;
import com.service.model.communication.CommunicationEmailLog;
import com.service.model.communication.CommunicationSettings;
import com.service.model.communication.EmailCheck;
import com.service.model.communication.Template;
import com.service.service.communication.EmailCheckService;
import com.service.service.communication.TemplateService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class EmailUtils{
	
	@Autowired
	EmailCheckService emailCheckService;
	
	@Autowired
	TaskExecutor taskExecutor;
	
	@Autowired
	TemplateService templateService;
	
	@Autowired
	CommunicationManager communicationManager;
	
	@Autowired
	RestTemplateUtil restTemplateUtil;
	
	@Autowired
	TemplateUtil templateUtil;
	
	public int isEmailverification(String emailId, CommunicationSettings communicationSettings) {
		log.info("isEmailverification :: communicationSettings.getMailVerifyByThirdParty() :: " + communicationSettings.getMailVerifyByThirdParty());
		try {
			if ("Y".equalsIgnoreCase(communicationSettings.getMailVerifyByThirdParty())) {
				boolean callEmailCheckService = false;
				boolean needToInsert = false;
				EmailCheck emailChecks = emailCheckService.getEmailCheckByEmailId(emailId);
				if (emailChecks!=null) {
					return emailChecks.getIsVerified();
				} else {
					needToInsert = true;
					callEmailCheckService = true;
				}
				if (callEmailCheckService) {
					EmailVerifyDTO emailVerifyDTO = restTemplateUtil.getEmailVerifyResponse(emailId);
					log.info("isEmailverification :: emailVerifyDTO :: " + emailVerifyDTO);
					if (needToInsert) {
						EmailCheck emailCheck = EmailCheck.builder().checkeDate(new Date()).email(emailId)
								.isVerified(emailVerifyDTO.getStatus()).build();
						emailCheckService.save(emailCheck);
					}
					return emailVerifyDTO.getStatus().intValue();
				}
			}
			return 1;
		} catch (Exception e) {
			log.error("isEmailverification exception caught :: ", e);
		}
		log.info("isEmailverification :: outside catch :: ");
		return 1;
	}
	
	public boolean sendCronsUnsendEmailToUser(CommunicationEmailLog emailLog, Template template, CommunicationSettings communicationSettings, CommunicationChannel communicationChannel) {
		log.info("sendCronsUnsendEmailToUser(User user): initiated");
		try {
			taskExecutor.execute(() -> {
				int checkUserEmail = isEmailverification(emailLog.getSendTo(), communicationSettings);
				if(checkUserEmail == 1) {
					//CALL API TO GET USER OBJECT BY EMAIL ID
					String body = template.getTemplateContent();
					String userName="User";
					body = body.replaceAll("#USER_NAME#", userName);
					String feedbackUrl=communicationSettings.getBasePathUI()+"/feedback/"+emailLog.getEncryptedId();
					log.info("sendCronsUnsendEmailToUser "+feedbackUrl);
					body = body.replaceAll("#URL#", feedbackUrl);
					log.info("sendCronsUnsendEmailToUser :: body: " + body);
					communicationManager.sendMail(new String[] {emailLog.getSendTo()}, null, null, body, template.getTemplateSubject(), null, communicationSettings, communicationChannel);
					templateUtil.updateEmailLogData(emailLog);
				}
			});
			return true;
		} catch (Exception e) {
			log.error("sendUnsendEmailToUser(User user): ",e);
		}
		return false;
	}
	
}