package com.service.communication.constant;

public enum EmailType {
	INSTALLMENT_PAYMENT_REMINDER("INSTALLMENT_PAYMENT_REMINDER"),
	;
    private final String title;
	
	private EmailType(final String title){
		this.title=title;
	}

	public String getTitle(){
		return this.title;
	} 
}