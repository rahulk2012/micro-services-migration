package com.service.communication.service;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.task.TaskExecutor;
import org.springframework.stereotype.Service;

import com.service.communication.util.EmailDTO;
import com.service.communication.util.EmilFileType;
import com.service.communication.util.RestTemplateUtil;
import com.service.communication.util.SendMail;
import com.service.model.communication.CommunicationChannel;
import com.service.model.communication.CommunicationSMS;
import com.service.model.communication.CommunicationSettings;
import com.service.model.communication.EmailSendLog;
import com.service.service.communication.CommunicationSMSService;
import com.service.service.communication.EmailSendLogService;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class CommunicationManagerImpl implements CommunicationManager {

//	@Value("${MAIL_SMTP_SERVER_AWS}")
//	String MAIL_SMTP_SERVER_AWS;
//	
//	@Value("${MAIL_SENDER_USER_AWS}")
//	String MAIL_SENDER_USER_AWS ;
//	
//	@Value("${MAIL_SENDER_PASSWORD_AWS}")
//	String MAIL_SENDER_PASSWORD_AWS;
//	
//	@Value("${MAIL_SMTP_SERVER_GMAIL}")
//	String MAIL_SMTP_SERVER_GMAIL;
//	
//	@Value("${MAIL_SENDER_USER_GMAIL}")
//	String MAIL_SENDER_USER_GMAIL ;
//	
//	@Value("${MAIL_SENDER_PASSWORD_GMAIL}")
//	String MAIL_SENDER_PASSWORD_GMAIL;
//	
//	@Value("${MAIL_SMTP_SERVER_IS}")
//	String MAIL_SMTP_SERVER_IS;
//	
//	@Value("${MAIL_SENDER_USER_IS}")
//	String MAIL_SENDER_USER_IS ;
//	
//	@Value("${MAIL_SENDER_PASSWORD_IS}")
//	String MAIL_SENDER_PASSWORD_IS;
//	
//	@Value("${MAIL_SMTP_SERVER_SENDGRID}")
//	String MAIL_SMTP_SERVER_SENDGRID;
//	
//	@Value("${MAIL_SENDER_USER_SENDGRID}")
//	String MAIL_SENDER_USER_SENDGRID ;
//	
//	@Value("${MAIL_SENDER_PASSWORD_SENDGRID}")
//	String MAIL_SENDER_PASSWORD_SENDGRID;
	
	@Value("${MOBILE_FOR_SMS}")
	String MOBILE_FOR_SMS;
	
	@Value("${SMS_SERVICE_BY_PASS}")
	boolean SMS_SERVICE_BY_PASS;

	@Autowired
	TaskExecutor taskExecutor;
	
	@Autowired
	RestTemplateUtil restTemplateUtil;
	
	@Autowired
	SendMail sendMail;
	
	@Autowired
	CommunicationSMSService communicationSMSService;
	
	@Autowired
	EmailSendLogService emailSendLogService;
	
	@Override
	public boolean sendMail(final String[] recepientAddresses, final String[] ccAddresses, final String[] bccAddresses, 
			String msgBody, String subject, List<EmilFileType> attachements, CommunicationSettings communicationSettings, CommunicationChannel communicationChannel) {
		try {
			final EmailDTO email = new EmailDTO();
			email.setContentType("text/html");
			email.setSubject(subject);
			email.setRecepientAddresses(recepientAddresses);
			email.setCcAddresses(ccAddresses);
			email.setBccAddresses(bccAddresses);
			email.setMessage(msgBody);
			
			//new table function
			saveSendEmailLog( recepientAddresses, ccAddresses, bccAddresses, subject, attachements, communicationChannel.getChannelName());
			email.setSenderEmail(communicationChannel.getChannelUser());
			email.setSenderPassword(communicationChannel.getChannelPassword());
			email.setSenderHost(communicationChannel.getChannelSmtp());
			email.setSenderAddress(communicationSettings.getSendBy());
			email.setAlias(communicationSettings.getSendAlias());
			email.setSenderPort(communicationChannel.getChannelPort());
			email.setBypass(communicationSettings.getMailBypass());
			if(attachements!=null) {
				email.setAttachements(attachements);
			}
			if("SENDGRID".equalsIgnoreCase(communicationChannel.getChannelName())) {
				return sendMail.sendEmailViaSendgrid(email);
			}else  if("SARV".equalsIgnoreCase(communicationChannel.getChannelName())) {
				return sendMail.postMailViaSarv(email);
			}else if("AWS".equalsIgnoreCase(communicationChannel.getChannelName())) {
				return sendMail.postMail(email);
			}else if("IS".equalsIgnoreCase(communicationChannel.getChannelName())) {
				return sendMail.postMail(email);
			}else if("GMAIL".equalsIgnoreCase(communicationChannel.getChannelName())) {
				return sendMail.postMail(email);
			}else {
				log.info("CommunicationManagerImpl.java  : sendMail() email not send");
				return false;
			}
		} catch (final Exception e) {
			log.error("CommunicationManagerImpl.java LNo : 98 : Exception Caught", e);
			return false;
		}
	}

	@Override
	public boolean sendSms(String module, String message, String isdCode, String mobileNumber){
		StringBuilder requestUrl = new StringBuilder();
		requestUrl.append("https://api.msg91.com/api/sendhttp.php?");
		requestUrl.append("route=4");
		requestUrl.append("&sender=INTSCH");
		requestUrl.append("&message="+module+message);
		requestUrl.append("&country="+isdCode);
		requestUrl.append("&mobiles="+mobileNumber);
		requestUrl.append("&authkey=129018AOvxDPw0zxn5dd23e7a");
		
//		JsonObject requestUrl = new  JsonObject();
//		requestUrl.addProperty("sender", "INT-SCH");
//		requestUrl.addProperty("route", "4");
//		requestUrl.addProperty("country", isdCode);
//		JsonArray sms = new  JsonArray();
//		JsonObject single = new  JsonObject();
//		single.addProperty("message", message);
//		JsonArray to = new JsonArray();
//		to.add(mobileNumber);
//		single.add("to", to);
//		sms.add(single);
//		requestUrl.add("sms", sms);
//		SMSSendVia91 smsSendVia91 = restTemplateUtil.sendSMSVia91(requestUrl.toString(), isdCode);
		String smsSendVia91 = restTemplateUtil.sendSMSVia91Get(requestUrl.toString());
		CommunicationSMS communicationSMS = CommunicationSMS.builder()
				.module(module)
				.content(message)
				.isdCode(isdCode)
				.mobileNumber(mobileNumber)
				.sendAt(new Date())
				.request(requestUrl.toString())
				.response(smsSendVia91)
				.sendingVia("msg91")
				.build();
		communicationSMSService.save(communicationSMS);
		return false;
	}
	
	public void saveSendEmailLog(final String[] recepientAddresses, final String[] ccAddresses, final String[] bccAddresses, 
		String subject, List<EmilFileType> attachements, String sendMailVia) {
		try {
			taskExecutor.execute(() -> {
				EmailSendLog emailSendLog = new EmailSendLog();
				if(recepientAddresses!=null) {
					emailSendLog.setToMail(emailBuild(recepientAddresses));
				}
				if(ccAddresses!=null) {
					emailSendLog.setCcMail(emailBuild(ccAddresses));
				}
				if(bccAddresses!=null) {
					emailSendLog.setBccMail(emailBuild(bccAddresses));
				}
				emailSendLog.setMailSubject(subject!=null?subject:"");
				emailSendLog.setAttachments(attachements!=null?"Y":"N");
				emailSendLog.setSendMailVia(sendMailVia!=null? sendMailVia:"");
				emailSendLog.setCreatedAt(new Date());
				emailSendLogService.save(emailSendLog);
			});
		}catch  (Exception e) {
			log.error("saveSendEmailLog(): In CommunicationManagerImpl line no 117 ",e);
		}
	}
	 public String emailBuild(final String[] emailList) {
		 if(emailList!=null) {
			 StringBuilder to =new StringBuilder();
			 for(int index=0; index<emailList.length; index++) {
				 to.append(emailList[index]);
				if(!(index==emailList.length-1)) {
					to.append(",");
				}
			 }
			 return to.toString();
		 }return "";
	 }
}
