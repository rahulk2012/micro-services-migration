package com.service.communication.service;

import java.util.List;

import com.service.communication.util.EmilFileType;
import com.service.model.communication.CommunicationChannel;
import com.service.model.communication.CommunicationSettings;

public interface CommunicationManager {
	boolean sendSms(String module, String message, String isdCode, String mobileNumber);

	boolean sendMail(String[] recepientAddresses, String[] ccAddresses, String[] bccAddresses, String msgBody,
			String subject, List<EmilFileType> attachements, CommunicationSettings communicationSettings, CommunicationChannel communicationChannel);

}