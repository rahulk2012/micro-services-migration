package com.service.communication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.reactive.function.client.WebClient;

import com.service.communication.util.CronsUtils;
import com.service.communication.util.TemplateUtil;
import com.service.constant.ApiConstant;
import com.service.dto.request.CronRequest;
import com.service.dto.request.EmailLogSaveRequest;
import com.service.dto.request.TemplateRequest;
import com.service.dto.request.TemplateSaveRequest;
import com.service.dto.response.CronsResponse;
import com.service.dto.response.EmailLogSaveResponse;
import com.service.dto.response.TemplateDTO;
import com.service.dto.response.TemplateResponse;

@Controller
@CrossOrigin("*")
@RequestMapping("/communication-service/api/v1")
public class CommunicationMailController {

	@Autowired
	WebClient.Builder webClientBuilder;
	
	@Autowired
	TemplateUtil templateUtil;
	
	@Autowired
	CronsUtils cronsUtils;
	
	@RequestMapping(value = "/get-template", method = { RequestMethod.GET, RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<TemplateResponse> getVendorContent(@RequestBody TemplateRequest templateRequest) {
		TemplateResponse response = templateUtil.getEmptyTemplateResponse();
		
		TemplateDTO templates = templateUtil.getTemplate(templateRequest.getTemplateType(), templateRequest.getVendorId(), templateRequest.getEventId());
		if (templates == null) {
			response.setTemplateData(null);
			response.getResponseStatus().setMessage("Failed to find dummy feedback");
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
			return new ResponseEntity<TemplateResponse>(response, HttpStatus.OK);
		}
		response.setTemplateData(templates);
		response.getResponseStatus().setMessage("Find dummy feedback");
		response.getResponseStatus().setCode(ApiConstant.CODE_SUCCESS.getValue());
		response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		return new ResponseEntity<TemplateResponse>(response, HttpStatus.OK);
	}
	
	@RequestMapping(value = "/save-communication-template", method = { RequestMethod.POST }, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<TemplateResponse> getSaveUpdateTemplateContent(@RequestBody TemplateSaveRequest templateSaveRequest) {
		TemplateResponse response = templateUtil.getEmptyTemplateResponse();
		response = templateUtil.saveTemplateData(templateSaveRequest);
		if(response.getResponseStatus().getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}
	
	@RequestMapping(value = "/save-communication-email-log", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<EmailLogSaveResponse> getSaveCommunicationEmailLog(@RequestBody EmailLogSaveRequest emailLogSaveRequest) {
		EmailLogSaveResponse response = EmailLogSaveResponse.builder().responseStatus(templateUtil.getEmptyResponseStatus("ES001", "Unable to Save Email Log")).build();
		response = templateUtil.saveEmailLogData(emailLogSaveRequest);
		if(response.getResponseStatus().getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}
	
	@RequestMapping(value = "/get-cron-list", method = { RequestMethod.POST }, consumes = {
			MediaType.APPLICATION_JSON_VALUE})
	public ResponseEntity<CronsResponse> getCronList(@RequestBody CronRequest cronRequest) {
		CronsResponse response = CronsResponse.builder().responseStatus(templateUtil.getEmptyResponseStatus("CL001", "Unable to get Crons List")).build();
			response=cronsUtils.getCronsList();
		if(response.getResponseStatus().getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}
}
