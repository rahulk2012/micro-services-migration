package com.service.communication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.service.communication.util.CronsUtils;

@RestController
@CrossOrigin("*")
@RequestMapping("/communication-service/api/v1")
public class CommunicationSchedularController {
	
	@Value("${EXECUTE_SCHEDULAR_FLAG}")
    boolean EXECUTE_SCHEDULAR_FLAG;
	
	@Autowired
	CronsUtils cronsUtils;
	
	@GetMapping(value = "/send-unsend-emails")
	public String sendApprovalEmails() {
		if(EXECUTE_SCHEDULAR_FLAG) {
			cronsUtils.callCronSchedulerForUnsendEmail(true);
			return "All emails send";
		}
		return "Your profile is not eligible to run crons";
	}
}