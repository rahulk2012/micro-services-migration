package com.service.util;

import static java.time.temporal.TemporalAdjusters.firstInMonth;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Instant;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.StringTokenizer;

import org.apache.commons.lang.StringUtils;

import com.service.constant.ApiConstant;

public interface DateUtil {
	long ONE_HOUR = 60 * 60 * 1000L;
	String[] month = { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };

	String STATUS_MSG_FAILED="FAILED";
	String SERI_ADMIN_EXCEPTION = "1001";
	
	static Timestamp getCurrentDate() {
		return new Timestamp(new GregorianCalendar().getTimeInMillis());
	}

	static Date convertToDateViaInstant(final LocalDate dateToConvert) {
		return Date.from(dateToConvert.atStartOfDay().atZone(ZoneId.systemDefault()).toInstant());
	}

	static Date getTodayDate() {
		return Date.from(Instant.now());
	}

	static String getFormattedDate() {
		final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(new Date());
	}

	public static Date convertStringToDate(final String dateStr) throws Exception {
		final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		final Date date;
		try {
			date = dateFormat.parse(dateStr);
			return date;
		} catch (final Exception exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}
	}

	public static Date convertStringToDate(final String dateStr, String format) throws Exception {
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		final Date date;
		try {
			date = dateFormat.parse(dateStr);
			return date;
		} catch (final Exception exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}
	}

	public static String convertDateToString(final Date date) throws Exception {
		if (date == null) {
			return StringUtils.EMPTY;
		}
		final SimpleDateFormat dateFormat = new SimpleDateFormat("MM-dd-yyyy");
		final String datestr;
		try {
			datestr = dateFormat.format(date);
			return datestr;
		} catch (final Exception exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}
	}

	public static String convertDate(final Date date) throws Exception {
		if (date == null) {
			return StringUtils.EMPTY;
		}
		final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM dd, yyyy hh:mm aa");
		final String datestr;
		try {
			datestr = dateFormat.format(date);
			return datestr;
		} catch (final Exception exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}
	}

	static LocalDate getDateWithoutTime() {
		return LocalDate.now();
	}

	static String getStringDateFromDate(final Date date) {
		if (date == null)
			return null;
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("dd-MM-yyyy");
		return simpleDateformat.format(date);
	}

	static String getStringDateFromDate(final Date date, final String dateFormat) {
		if (date == null || dateFormat == null)
			return null;
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat(dateFormat);
		return simpleDateformat.format(date);
	}

	static String getStringDateFromDateForPDF(final Date date) {
		if (date == null)
			return null;
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy-MM-dd");
		return simpleDateformat.format(date);
	}

	static String getStringDateFromDateForId(final Date date) {
		if (date == null)
			return null;
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyMMddhhmmss");
		return simpleDateformat.format(date);
	}

	static String getStringTimeFromDate(final Date date) {
		if (date == null)
			return null;
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("HH:mm");
		return simpleDateformat.format(date);
	}

	static LocalDate getYesterdayDateWithoutTime() {
		return LocalDate.now().minusDays(1L);
	}

	static LocalDate getTomorrowDateWithoutTime() {
		return LocalDate.now().plusDays(1L);
	}

	static LocalDate getDateAfterThreeMonths() {
		return LocalDate.now().plusMonths(3L);
	}

	static LocalDate getDateBefore7Days() {
		return LocalDate.now().minusWeeks(1L);
	}

	static LocalDate getDateBefore1Month() {
		return LocalDate.now().minusMonths(1L);
	}

	static LocalDate getFirstDayOfQuarter(final Date date) {
		final LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate.with(localDate.getMonth().firstMonthOfQuarter()).with(TemporalAdjusters.firstDayOfMonth());
	}

	static LocalDate getLastDayOfQuarter(final Date date) {
		final LocalDate localDate = date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
		return localDate.with(localDate.getMonth().firstMonthOfQuarter()).with(TemporalAdjusters.lastDayOfMonth());
	}

	static String getFistDateofPreviousYear() {
		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.DAY_OF_YEAR, 1);
		return DateUtil.getStringDateFromDateForPDF(cal.getTime());
	}

	static String getLastDateOfPreviousYear() {
		final Calendar cal = Calendar.getInstance();
		cal.set(Calendar.YEAR, 2016);
		cal.set(Calendar.MONTH, Calendar.DECEMBER);
		cal.set(Calendar.DAY_OF_MONTH, 31);
		return DateUtil.getStringDateFromDateForPDF(cal.getTime());
	}

	static LocalDate getFistDateofCurrentYear() {

		final LocalDate date = LocalDate.now();
		return date.with(TemporalAdjusters.firstDayOfYear());

	}

	static Date getDateByPassingHHMM(final String hhmm) {
		final String splitted[] = hhmm.split("\\:");
		final Calendar calendar1 = new Calendar.Builder().set(Calendar.DATE, 0)
				.set(Calendar.HOUR_OF_DAY, Integer.parseInt(splitted[0]))
				.set(Calendar.MINUTE, Integer.parseInt(splitted[1])).set(Calendar.SECOND, 0)
				.set(Calendar.MILLISECOND, 0).build();
		return calendar1.getTime();
	}

	static Date getLastSundayDateWithoutTime(Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		final int weekDay = cal.get(Calendar.DAY_OF_WEEK);
		if (weekDay == 1) {
			// sunday
			// do nothing
		} else if (weekDay == 2)
			// monday
			cal.add(Calendar.DATE, -1);
		else if (weekDay == 3)
			// tuesday
			cal.add(Calendar.DATE, -2);
		else if (weekDay == 4)
			// wedness day
			cal.add(Calendar.DATE, -3);
		else if (weekDay == 5)
			// thursday
			cal.add(Calendar.DATE, -4);
		else if (weekDay == 6)
			// friday
			cal.add(Calendar.DATE, -5);
		else if (weekDay == 7)
			// satarday
			cal.add(Calendar.DATE, -6);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		date = cal.getTime();
		return date;
	}

	static Date getNextSundayDateWithoutTime(Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		final int weekDay = cal.get(Calendar.DAY_OF_WEEK);
		if (weekDay == 1) {
			// sunday
			// do nothing
		} else if (weekDay == 2) {
			// monday
			cal.add(Calendar.DATE, 6);
		} else if (weekDay == 3) {
			// tuesday
			cal.add(Calendar.DATE, 5);
		} else if (weekDay == 4) {
			// wedness day
			cal.add(Calendar.DATE, 4);
		} else if (weekDay == 5) {
			// thursday
			cal.add(Calendar.DATE, 3);
		} else if (weekDay == 6) {
			// friday
			cal.add(Calendar.DATE, 2);
		} else if (weekDay == 7) {
			// satarday
			cal.add(Calendar.DATE, 1);
		}
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		date = cal.getTime();
		return date;
	}

	static Date getFirstDateOfMonthWithoutTime(Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		date = cal.getTime();
		return date;
	}

	static Date getFirstDateOfNextMonthWithoutTime(Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.MONTH, 1);
		cal.set(Calendar.DAY_OF_MONTH, 1);
		cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
		cal.set(Calendar.SECOND, 0);
		cal.set(Calendar.MILLISECOND, 0);
		date = cal.getTime();
		return date;
	}

	static String getFormattedDateLikeGmail(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		final Date currentDate = new Date();
		final Calendar currentCal = Calendar.getInstance();
		currentCal.setTime(currentDate);
		String formattedDate = "";
		String datePostfix = "";
		if (cal.get(Calendar.DATE) == 1 || cal.get(Calendar.DATE) == 21 || cal.get(Calendar.DATE) == 31)
			datePostfix = "st";
		else if (cal.get(Calendar.DATE) == 2 || cal.get(Calendar.DATE) == 22)
			datePostfix = "nd";
		else if (cal.get(Calendar.DATE) == 3 || cal.get(Calendar.DATE) == 23)
			datePostfix = "rd";
		else
			datePostfix = "th";
		if (cal.get(Calendar.ERA) == currentCal.get(Calendar.ERA)
				&& cal.get(Calendar.YEAR) == currentCal.get(Calendar.YEAR)
				&& cal.get(Calendar.DAY_OF_YEAR) == currentCal.get(Calendar.DAY_OF_YEAR)) {
			if (cal.get(Calendar.HOUR_OF_DAY) > 11)
				formattedDate = (cal.get(Calendar.HOUR_OF_DAY) - 12 == 0 ? "12" : cal.get(Calendar.HOUR_OF_DAY) - 12)
						+ ":"
						+ (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : cal.get(Calendar.MINUTE))
						+ " pm";
			else
				formattedDate = (cal.get(Calendar.HOUR_OF_DAY) == 0 ? "12" : cal.get(Calendar.HOUR_OF_DAY)) + ":"
						+ (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : cal.get(Calendar.MINUTE))
						+ " am";
		} else if (cal.get(Calendar.YEAR) == currentCal.get(Calendar.YEAR))
			formattedDate = cal.get(Calendar.DATE) + datePostfix + " " + DateUtil.month[cal.get(Calendar.MONTH)];
		else
			formattedDate = cal.get(Calendar.DATE) + datePostfix + " " + DateUtil.month[cal.get(Calendar.MONTH)] + " "
					+ cal.get(Calendar.YEAR);
		return formattedDate;
	}

	static boolean compareDate(final Date date) {
		final Calendar cal = Calendar.getInstance();
		final Calendar currentCal = Calendar.getInstance();
		cal.setTime(date);
		currentCal.setTime(DateUtil.getCurrentDate());
		return cal.after(currentCal);
	}

	static int getTrialLeft(final Date expDate) {
		final long timeDiff = expDate.getTime() - new Date().getTime();
		final long dateDiff = timeDiff / (DateUtil.ONE_HOUR * 24);
		return (int) dateDiff;
	}

	static String getFormattedDate(final Date date) {
		if (date == null)
			return null;
		final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(date);
	}

	static String getFormattedDateWithTime(final Date date) {
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		String formattedDate = "";
		String datePostfix = "";
		String dayTime = "";
		if (cal.get(Calendar.DATE) == 1 || cal.get(Calendar.DATE) == 21 || cal.get(Calendar.DATE) == 31)
			datePostfix = "st";
		else if (cal.get(Calendar.DATE) == 2 || cal.get(Calendar.DATE) == 22)
			datePostfix = "nd";
		else if (cal.get(Calendar.DATE) == 3 || cal.get(Calendar.DATE) == 23)
			datePostfix = "rd";
		else
			datePostfix = "th";
		if (cal.get(Calendar.HOUR_OF_DAY) > 11)
			dayTime = (cal.get(Calendar.HOUR_OF_DAY) - 12 == 0 ? "12" : cal.get(Calendar.HOUR_OF_DAY) - 12) + ":"
					+ (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : cal.get(Calendar.MINUTE))
					+ " pm";
		else
			dayTime = (cal.get(Calendar.HOUR_OF_DAY) == 0 ? "12" : cal.get(Calendar.HOUR_OF_DAY)) + ":"
					+ (cal.get(Calendar.MINUTE) < 10 ? "0" + cal.get(Calendar.MINUTE) : cal.get(Calendar.MINUTE))
					+ " am";
		formattedDate = cal.get(Calendar.DATE) + datePostfix + " " + DateUtil.month[cal.get(Calendar.MONTH)] + " "
				+ cal.get(Calendar.YEAR) + " @ " + dayTime;
		return formattedDate;
	}

	static String getDeliveryDateFromCreateDate(Date date) {
		if (date == null)
			return null;
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		cal.add(Calendar.DATE, 5);
		date = cal.getTime();
		final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(date);
	}

	static String getNextDate(final Date currnetDate, final int day) {
		if (currnetDate == null)
			return null;
		final Date nextHourDate = new Date();
		nextHourDate.setTime(currnetDate.getTime() + day * 24 * 60 * 60 * 1000);
		final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(nextHourDate);
	}

	static String getNextDate(final java.sql.Date currnetDate, final int day) {
		final Date nextHourDate = new Date();
		nextHourDate.setTime(currnetDate.getTime() + day * 24 * 60 * 60 * 1000);
		final SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy");
		return formatter.format(nextHourDate);
	}

	static String getCurrentYear() {
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
		return simpleDateformat.format(new Date());
	}

	static String getCurrentYearTwoDigit() {
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("yy");
		return simpleDateformat.format(new Date());
	}

	static String convertDateToFormattedType(final Date date, final String format) {
		if (date == null || format == null)
			return null;
		final SimpleDateFormat dateFormat = new SimpleDateFormat(format);
		return dateFormat.format(date);
	}

	static int daysBetweenTwoDate(final Date startDate, final Date endDate) {
		return (int) ((endDate.getTime() - startDate.getTime()) / (1000 * 60 * 60 * 24 * 365));
	}

	static boolean isValidDate(final String inDate) {
		if (inDate == null)
			return false;
		// set the format to use as a constructor argument
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		if (inDate.trim().length() != dateFormat.toPattern().length())
			return false;
		dateFormat.setLenient(false);
		try {
			// parse the inDate parameter
			dateFormat.parse(inDate.trim());
		} catch (final Exception pe) {
			return false;
		}
		return true;
	}

	static int getDaysLeft(final Date createDate) {
		final long timeDiff = new Date().getTime() - createDate.getTime();
		final long dateDiff = timeDiff / (DateUtil.ONE_HOUR * 24);
		return (int) dateDiff;
	}

	static String getDateInDesiredFormatAsString(final Date date, final String format) {
		if (date == null || format == null)
			return null;
		final SimpleDateFormat formatter = new SimpleDateFormat(format);
		return formatter.format(date);
	}

	static String getFormattedDateLikeGmail(final Date date, final boolean withTime) {
		if (date == null)
			return null;
		if (!withTime) {
			final Calendar cal = Calendar.getInstance();
			cal.setTime(date);
			final Date currentDate = new Date();
			final Calendar currentCal = Calendar.getInstance();
			currentCal.setTime(currentDate);
			String formattedDate = "";
			String datePostfix = "";
			if (cal.get(Calendar.DATE) == 1 || cal.get(Calendar.DATE) == 21 || cal.get(Calendar.DATE) == 31)
				datePostfix = "st";
			else if (cal.get(Calendar.DATE) == 2 || cal.get(Calendar.DATE) == 22)
				datePostfix = "nd";
			else if (cal.get(Calendar.DATE) == 3 || cal.get(Calendar.DATE) == 23)
				datePostfix = "rd";
			else
				datePostfix = "th";
			if (cal.get(Calendar.YEAR) == currentCal.get(Calendar.YEAR))
				formattedDate = cal.get(Calendar.DATE) + datePostfix + " " + DateUtil.month[cal.get(Calendar.MONTH)];
			else
				formattedDate = cal.get(Calendar.DATE) + datePostfix + " " + DateUtil.month[cal.get(Calendar.MONTH)]
						+ " " + cal.get(Calendar.YEAR);
			return formattedDate;
		} else {
			return DateUtil.getFormattedDateLikeGmail(date);
		}
	}

	static Date changeDateFormat(String yyyy, final String m, final String dd) throws Exception {
		String mmm = null;
		final int mm = Integer.parseInt(m);
		switch (mm) {
		case 1:
			mmm = "Jan";
			break;
		case 2:
			mmm = "Feb";
			break;
		case 3:
			mmm = "Mar";
			break;
		case 4:
			mmm = "Apr";
			break;
		case 5:
			mmm = "May";
			break;
		case 6:
			mmm = "Jun";
			break;
		case 7:
			mmm = "Jul";
			break;
		case 8:
			mmm = "Aug";
			break;
		case 9:
			mmm = "Sep";
			break;
		case 10:
			mmm = "Oct";
			break;
		case 11:
			mmm = "Nov";
			break;
		case 12:
			mmm = "Dec";
			break;
		default:
			break;
		}
		if (yyyy.contains("Before "))
			yyyy = yyyy.replace("Before ", "");
		else if (yyyy.contains(" or earlier "))
			yyyy = yyyy.replace(" or earlier ", "");
		final DateFormat formatter;
		Date date = new Date();

		formatter = new SimpleDateFormat("dd-MMM-yy");
		try {
			date = formatter.parse(dd + "-" + mmm + "-" + yyyy);
		} catch (final java.text.ParseException exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}
		return date;
	}

	static Date changeDateFormat(final String dateInString) throws Exception {
		String mm = null;
		final String[] loopDate = dateInString.split("-");
		if (loopDate[1].equalsIgnoreCase("01"))
			mm = "Jan";
		if (loopDate[1].equalsIgnoreCase("02"))
			mm = "Feb";
		if (loopDate[1].equalsIgnoreCase("03"))
			mm = "Mar";
		if (loopDate[1].equalsIgnoreCase("04"))
			mm = "Apr";
		if (loopDate[1].equalsIgnoreCase("05"))
			mm = "May";
		if (loopDate[1].equalsIgnoreCase("06"))
			mm = "Jun";
		if (loopDate[1].equalsIgnoreCase("07"))
			mm = "Jul";
		if (loopDate[1].equalsIgnoreCase("08"))
			mm = "Aug";
		if (loopDate[1].equalsIgnoreCase("09"))
			mm = "Sep";
		if (loopDate[1].equalsIgnoreCase("10"))
			mm = "Oct";
		if (loopDate[1].equalsIgnoreCase("11"))
			mm = "Nov";
		if (loopDate[1].equalsIgnoreCase("12"))
			mm = "Dec";
		final DateFormat formatter;
		Date date = new Date();

		formatter = new SimpleDateFormat("dd-MMM-yy");
		try {
			date = formatter.parse(loopDate[0] + "-" + mm + "-" + loopDate[2]);
		} catch (final java.text.ParseException exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}

		return date;
	}

	static Date changeDateFormatToDateTime(final String dateInString) throws Exception {
		final DateFormat formatter;
		Date date = new Date();
		formatter = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
		try {
			date = formatter.parse(dateInString);
		} catch (final java.text.ParseException exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}

		return date;
	}

	static String changeDateFormatDDMMYYYY(final String dateTimeStamp) {
		String mm = null;
		final String[] loopDate = dateTimeStamp.split(" ");
		if (loopDate[1].equalsIgnoreCase("jan"))
			mm = "01";
		if (loopDate[1].equalsIgnoreCase("feb"))
			mm = "02";
		if (loopDate[1].equalsIgnoreCase("mar"))
			mm = "03";
		if (loopDate[1].equalsIgnoreCase("apr"))
			mm = "04";
		if (loopDate[1].equalsIgnoreCase("may"))
			mm = "05";
		if (loopDate[1].equalsIgnoreCase("jun"))
			mm = "06";
		if (loopDate[1].equalsIgnoreCase("jul"))
			mm = "07";
		if (loopDate[1].equalsIgnoreCase("aug"))
			mm = "08";
		if (loopDate[1].equalsIgnoreCase("sep"))
			mm = "09";
		if (loopDate[1].equalsIgnoreCase("oct"))
			mm = "10";
		if (loopDate[1].equalsIgnoreCase("nov"))
			mm = "11";
		if (loopDate[1].equalsIgnoreCase("dec"))
			mm = "12";
		return loopDate[2] + "-" + mm + "-" + loopDate[5];
	}

	static Date changeDateFormatToDate(final String dateInString, final String dateFormat) throws Exception {
		try {
			return new SimpleDateFormat(dateFormat).parse(dateInString);
		} catch (final Exception exception) {
			throw new Exception(SERI_ADMIN_EXCEPTION, exception);
		}
	}

	static String getDateFormatBack(final String strDate) {
		final String[] loopDate = strDate.split("/");
		return loopDate[2] + "-" + loopDate[1] + "-" + loopDate[0];
	}

	static String[] splitDateAndTime(final String date) {
		final StringTokenizer st = new StringTokenizer(date, " ");
		return new String[] { st.nextToken(), st.nextToken() };
	}

	static String[] splitDate(final String date) {
		final StringTokenizer st = new StringTokenizer(date, "-");
		return new String[] { st.nextToken(), st.nextToken(), st.nextToken() };
	}

	static String[] splitTime(final String time) {
		final StringTokenizer st = new StringTokenizer(time, ":");
		return new String[] { st.nextToken(), st.nextToken(), st.nextToken() };
	}

	static int getCurrentDay() {
		final Calendar currentDate = Calendar.getInstance();
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String dt[] = DateUtil.splitDateAndTime(formatter.format(currentDate.getTime()));
		final String ddmmyyyy[] = DateUtil.splitDate(dt[0]);
		return Integer.parseInt(ddmmyyyy[2]);
	}

	static int getCurrentMonth() {
		final Calendar currentDate = Calendar.getInstance();
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String dt[] = DateUtil.splitDateAndTime(formatter.format(currentDate.getTime()));
		final String ddmmyyyy[] = DateUtil.splitDate(dt[0]);
		return Integer.parseInt(ddmmyyyy[1]);
	}

	static String getCurrentMonthInString() {
		final String[] months = { "January", "February", "March", "April", "May", "June", "July", "August", "September",
				"October", "November", "December" };
		final Calendar cal = Calendar.getInstance();
		return months[cal.get(Calendar.MONTH)];
	}

	static String getMonthInString(final int mm) {
		String month = null;
		switch (mm) {
		case 1:
			month = "Jan";
			break;
		case 2:
			month = "Feb";
			break;
		case 3:
			month = "Mar";
			break;
		case 4:
			month = "Apr";
			break;
		case 5:
			month = "May";
			break;
		case 6:
			month = "Jun";
			break;
		case 7:
			month = "Jul";
			break;
		case 8:
			month = "Aug";
			break;
		case 9:
			month = "Sep";
			break;
		case 10:
			month = "Oct";
			break;
		case 11:
			month = "Nov";
			break;
		case 12:
			month = "Dec";
			break;
		default:
			break;
		}
		return month;
	}

	static int getYearFromDate(final java.util.Date currentDate) {
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("yyyy");
		return Integer.parseInt(simpleDateformat.format(currentDate));
	}

	static int getMonthFromDate(final java.util.Date currentDate) {
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("MM");
		return Integer.parseInt(simpleDateformat.format(currentDate));
	}

	static int getDayFromDate(final java.util.Date currentDate) {
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("dd");
		return Integer.parseInt(simpleDateformat.format(currentDate));
	}

	static String getDateInDDMMYYYFormat(final java.util.Date currentDate) {
		final SimpleDateFormat simpleDateformat = new SimpleDateFormat("dd-MM-yyyy");
		return simpleDateformat.format(currentDate);
	}

	static int getAge(final java.util.Date date) {
		final SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd");
		final String[] temp = DateUtil.splitDate(fmt.format(date));
		final Calendar calDOB = Calendar.getInstance();
		calDOB.set(Integer.parseInt(temp[0]), Integer.parseInt(temp[1]), Integer.parseInt(temp[2]));
		final Calendar calNow = Calendar.getInstance();
		calNow.setTime(new java.util.Date());
		int ageYr = calNow.get(Calendar.YEAR) - calDOB.get(Calendar.YEAR);
		final int ageMo = calNow.get(Calendar.MONTH) - calDOB.get(Calendar.MONTH);
		if (ageMo < 0)
			// adjust years by subtracting one
			ageYr--;
		return ageYr;
	}

	static int getCurrentMonthInNumeric() {
		final Calendar currentDate = Calendar.getInstance();
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String dt[] = DateUtil.splitDateAndTime(formatter.format(currentDate.getTime()));
		final String ddmmyyyy[] = DateUtil.splitDate(dt[0]);
		return Integer.parseInt(ddmmyyyy[1]);
	}

	static String getCurrentDateOnly() {
		final Calendar currentDate = Calendar.getInstance();
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		final String dt[] = DateUtil.splitDateAndTime(formatter.format(currentDate.getTime()));
		return dt[0];
	}

	static Date getNextDate(final int day) {
		final Date currnetDate = new Date();
		final Date nextHourDate = new Date();
		nextHourDate.setTime(currnetDate.getTime() + day * 24 * 60 * 60 * 1000);
		return nextHourDate;
	}

	static Date getNextTwoHourDate(final int nextDay) {
		final Date currnetDate = new Date();
		final Date nextHourDate = new Date();
		nextHourDate.setTime(currnetDate.getTime() + nextDay * 60 * 60 * 1000);
		return nextHourDate;
	}

	static String getDateReverse(final String cDate) {
		return cDate.substring(6, 10) + "-" + cDate.substring(3, 5) + "-" + cDate.substring(0, 2);
	}

	static boolean isSunday(final Date date) {
		boolean falg = false;
		final SimpleDateFormat f = new SimpleDateFormat("EEEE");
		falg = f.format(date).equals("Sunday");
		return falg;
	}

	static String getNextDateInMMMYYYYFormat(final int nextDay) {
		final Calendar nowCal = Calendar.getInstance();
		final int month = nowCal.get(Calendar.MONTH) + nextDay;
		final int year = nowCal.get(Calendar.YEAR);
		final Calendar cal = Calendar.getInstance();
		cal.clear();
		cal.set(Calendar.YEAR, year);
		cal.set(Calendar.MONTH, month);
		cal.set(Calendar.DAY_OF_MONTH, nextDay);
		final Date dueDate = new Date(cal.getTimeInMillis());
		final SimpleDateFormat dateFormat = new SimpleDateFormat("MMM/yyyy");
		return dateFormat.format(dueDate);
	}

	static String getDateInString() {
		final Date dateNow = new Date();
		final SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
		return dateformat.format(dateNow);
	}

	static String getDateTimeInString() {
		final Date dateNow = new Date();
		final SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHMMSS");
		return dateformat.format(dateNow);
	}

	static long getDaysBetweenTwoDate(final Calendar startDate, final Calendar endDate) {
		final Calendar date = (Calendar) startDate.clone();
		long daysBetween = 0;
		while (date.before(endDate)) {
			date.add(Calendar.DAY_OF_MONTH, 1);
			daysBetween++;
		}
		return daysBetween;
	}

	static long getDaysBetweenTwoDate(final Date startDate, final Date endDate) {
		return Math.round((endDate.getTime() - startDate.getTime()) / (double) (24 * 3600 * 1000));
	}

	static String getFormattedDateForUpload() {
		final Date dateNow = new Date();
		final SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMddHHmmss");
		return dateformat.format(dateNow);
	}

	static DateDTO totalDifferenceBetweenTwoDate(final Date prevDate, final Date todayDate) {
		final DateDTO dateDTO = DateDTO.builder().build();
		final long diff = todayDate.getTime() - prevDate.getTime();
		dateDTO.setDiff(diff);
		final long diffSeconds = diff / 1000 % 60;
		dateDTO.setDiffSeconds(diffSeconds);
		final long diffMinutes = diff / (60 * 1000) % 60;
		dateDTO.setDiffMinutes(diffMinutes);
		final long diffHours = diff / (60 * 60 * 1000) % 24;
		dateDTO.setDiffHours(diffHours);
		final long diffDays = diff / (24 * 60 * 60 * 1000);
		dateDTO.setDiffDays(diffDays);
		return dateDTO;
	}

	static long getTotalDifferenceBetweenTwoDate(final LocalDate prevDate, final LocalDate todayDate) {
		long noOfDaysBetween = ChronoUnit.DAYS.between(prevDate, todayDate);
		return noOfDaysBetween;
	}

	static Date changeDateFormatToDate(final String inputDate) throws Exception {
		final SimpleDateFormat inSDF = new SimpleDateFormat("MM/dd/yyyy");
		final SimpleDateFormat outSDF = new SimpleDateFormat("yyyy-MM-dd");
		if (StringUtils.isNotBlank(inputDate))
			try {
				final Date date = inSDF.parse(inputDate);
				final String Date = outSDF.format(date);
				return outSDF.parse(Date);
			} catch (final Exception exception) {
				throw new Exception(SERI_ADMIN_EXCEPTION, exception);
			}
		return null;
	}

	public static String getDateTimeFormatInStringMMddyyyy(Date date) {
		SimpleDateFormat dateformat = new SimpleDateFormat("MMMM dd, yyyy HH:mm:ss");
		return dateformat.format(date);
	}

	public static String getDateFormatInStringMMddyyyy(Date date) {
		SimpleDateFormat dateformat = new SimpleDateFormat("MMMM dd-yyyy");
		return dateformat.format(date).replace("-", ",");
	}

	// get Mondays,Tuesdays,Wednesdays in 6 months
	public static List<LocalDate> getDaysInMonTuesWed(YearMonth month) {
		LocalDate currentDate = LocalDate.now();
		LocalDate firstMonday = currentDate.with(firstInMonth(DayOfWeek.MONDAY));
//		LocalDate firstTuesday = currentDate.with(firstInMonth(DayOfWeek.TUESDAY)); 
//		LocalDate firstWednesday = currentDate.with(firstInMonth(DayOfWeek.WEDNESDAY)); 
		List<LocalDate> firstDaysOfWeeks = new ArrayList<>();
		for (LocalDate day = currentDate.with(firstInMonth(DayOfWeek.MONDAY)); stillInCalendar(month,
				day); day = day.plusWeeks(1)) {
			firstDaysOfWeeks.add(day);
		}
		for (LocalDate day = firstMonday.with(DayOfWeek.TUESDAY); stillInCalendar(month, day); day = day.plusWeeks(1)) {
			firstDaysOfWeeks.add(day);
		}
		for (LocalDate day = firstMonday.with(DayOfWeek.WEDNESDAY); stillInCalendar(month,
				day); day = day.plusWeeks(1)) {
			firstDaysOfWeeks.add(day);
		}
//		    for (LocalDate day = firstMonday.with(DayOfWeek.THURSDAY); stillInCalendar(month, day); day = day
//		        .plusWeeks(1)) {
//		      firstDaysOfWeeks.add(day);
//		}
//		    for (LocalDate day = firstMonday.with(DayOfWeek.FRIDAY); stillInCalendar(month, day); day = day
//		        .plusWeeks(1)) {
//		      firstDaysOfWeeks.add(day);
//		}

		return firstDaysOfWeeks;
	}

	public static boolean stillInCalendar(YearMonth yearMonth, LocalDate day) {
		return !day.isAfter(yearMonth.plusMonths(ApiConstant.TOTAL_MONTH.getactivityValue()).atEndOfMonth());
	}

	public static String getDateFormatInStringMonthDateYear(Date date) {
		SimpleDateFormat dateformat = new SimpleDateFormat("MMM dd, yyyy");
		return dateformat.format(date).replace("-", ",");
	}

	public static String getDateFormatInTime(Time time) {
		SimpleDateFormat dateformat = new SimpleDateFormat("HH:mm");
		return dateformat.format(time);
	}

	static String getDateInDayNameYearMonthDateTime(Date date) {
		if (date == null)
			return null;
		final SimpleDateFormat dateformat = new SimpleDateFormat("E MMM dd yyyy HH:mm:ss z");
		return dateformat.format(date);
	}

	static String getInstallmentDigit(Integer position) {
		final String[] number = { "1<sup>st</sup>", "2<sup>nd</sup>", "3<sup>rd</sup>", "4<sup>th</sup>",
				"5<sup>th</sup>", "6<sup>th</sup>", "7<sup>th</sup>", "8<sup>th</sup>", "9<sup>th</sup>",
				"10<sup>th</sup>" };
		String result = number[position - 1];
		return result;
	}
}