package com.service.util;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class DateDTO {
	private Long diffDays;
	private Long diffHours;
	private Long diffMinutes;
	private Long diffSeconds;
	private Long diff;
}