package com.service.util;

import java.time.LocalDateTime;

import org.springframework.stereotype.Service;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;

@Service
public class ApiUtil {

	public String getTimesTamp() {
		return LocalDateTime.now().toString();
	}

	public ResponseStatus getEmptyResponseStatus(String code, String message) {
		ResponseStatus responseStatus = ResponseStatus.builder().status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(code).message(message).build();
		return responseStatus;
	}
}