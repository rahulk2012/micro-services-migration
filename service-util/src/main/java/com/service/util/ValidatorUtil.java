package com.service.util;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;

@Service
public interface ValidatorUtil {
	StringBuilder expression = new StringBuilder();
	String PIN_NUMBER_REGEX = "[1-9]{1}+[0-9]{5}";
	String USER_NAME_REGEX = "[a-zA-Z0-9]{3,15}";
	String CITY_NAME_REGEX = "[a-zA-Z]{3,15}";
	String CANADIAN_ZIP_CODE_REGEX = "^(?!.*[DFIOQU])[A-VXY][0-9][A-Z] ?[0-9][A-Z][0-9]$";
	String UK_ZIP_CODE_REGEX = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
	String US_ZIP_CODE_REGEX = "^[0-9]{5}$";
	String US_SSN_CODE_REGEX = "^(?!000|666)[0-8][0-9]{2}(?!00)[0-9]{2}(?!0000)[0-9]{4}$";
	String EIN_CODE_REGEX = "^[1-9]\\d?-\\d{7}$";
	String ONLY_DIGIT_REGEX = "^[0-9]{9}$";

	static boolean isValidVerfication(final String verficationCode) {
		boolean flagValid = false;
		flagValid = StringUtils.isNotBlank(verficationCode) && verficationCode.length() == 6;
		return flagValid;
	}

	static boolean isValidEmail(final String email) {
		boolean flagValid = false;
		if (StringUtils.isNotBlank(email)) {
			expression.replace(0, 200, "^[\\w\\-]([\\.\\w])+[\\w]+@([\\w\\-]+\\.)+[A-Z]{2,4}$");
			final CharSequence inputStr = email;
			final Pattern pattern = Pattern.compile(expression.toString(), Pattern.CASE_INSENSITIVE);
			final Matcher matcher = pattern.matcher(inputStr);
			flagValid = matcher.matches();
		} else
			flagValid = false;
		return flagValid;
	}

	static boolean checkValidEmail(final String email) {
		final boolean flagValid = false;
		if (StringUtils.isNotBlank(email)) {
			final String EMAIL_REGEX = "^[\\w-_\\.+]*[\\w-_\\.]\\@([\\w]+\\.)+[\\w]+[\\w]$";
			final CharSequence inputStr = email;
			final Pattern pattern = Pattern.compile(EMAIL_REGEX, Pattern.CASE_INSENSITIVE);
			final Matcher matcher = pattern.matcher(inputStr);
			return matcher.matches();
		}
		return flagValid;
	}

	static boolean isValid(final double value) {
		return value > 0;
	}

	static boolean isValid(final Double value) {
		return value != null && value > 0;
	}

	static boolean isValid(final float value) {
		return value > 0;
	}

	static boolean isValid(final Float value) {
		return value != null && value > 0;
	}

	static boolean isValid(final BigDecimal value) {
		return value != null;
	}

	static boolean isValid(final Integer value) {
		return value != null && value > 0;
	}

	static boolean isValid(final Long value) {
		return value != null && value > 0;
	}

	static boolean isValid(final String value) {
		return StringUtils.isNotBlank(value) && !"0".equalsIgnoreCase(value) && !"".equalsIgnoreCase(value) && !" ".equalsIgnoreCase(value);
	}
	
	static boolean isValidOTP(final String value) {
		return StringUtils.isNotBlank(value) && !"0".equalsIgnoreCase(value) && !"".equalsIgnoreCase(value) && !" ".equalsIgnoreCase(value)
				&&  value.length() !=6;
	}

	static double getPercentage(final double value, final double percentage) {
		return value * percentage / 100;
	}

	static double round(final double valueToRound, final int numberOfDecimalPlaces) {
		final double multipicationFactor = Math.pow(10, numberOfDecimalPlaces);
		final double interestedInZeroDPs = valueToRound * multipicationFactor;
		return Math.round(interestedInZeroDPs) / multipicationFactor;
	}

	static String getProperDoubleValue(final String value) {
		final java.text.DecimalFormat dfSingleCarLoan = new java.text.DecimalFormat("#0.0");
		return dfSingleCarLoan.format(Double.valueOf(value) / 100000);
	}

	static boolean isValidMobile(final String mobno) {
		if (StringUtils.isNotBlank(mobno) && mobno.length() == 10
				&& ( mobno.startsWith("6") || mobno.startsWith("7") || mobno.startsWith("8") || mobno.startsWith("9")))
			try {
				final Long mob = Long.parseLong(mobno);
				return mob > 0;
			} catch (final Exception e) {
				return false;
			}
		return false;
	}
	
	static boolean isValidMobileOtherCountry(final String mobno) {
		if (StringUtils.isNotBlank(mobno) && mobno.length() <= 15 && mobno.length() >=4)
			try {
				final Long mob = Long.parseLong(mobno);
				return mob > 0;
			} catch (final Exception e) {
				return false;
			}
		return false;
	}
	
	static boolean isValidRating(String rating) {
		if(!isValid(rating)) {
			return false;
		}
		try {
			int ratingValue = new Double(rating).intValue();
			if(ratingValue>=1 && ratingValue<=5) {
				return true;
			}
		}catch(Exception e) {
			e.printStackTrace();
		}
		return false;
	}
		
	static boolean isValidNRIMobile(final String mobno) {
		if (StringUtils.isNotBlank(mobno) && mobno.length() > 1 && mobno.length() <= 12)
			try {
				final Long mob = Long.parseLong(mobno);
				return mob > 0;
			} catch (final Exception e) {
				return false;
			}
		return false;
	}

	static boolean isValidGender(final String gender) {
		if (StringUtils.isNotBlank(gender))
			return StringUtils.equalsAnyIgnoreCase(gender, "Male", "Female", "Other");
		return false;
	}

	static boolean isValidPanNo(String panNo) {
		if (panNo != null)
			panNo = panNo.trim();
		else
			return false;
		final Pattern pattern = Pattern.compile("([A-Za-z]{3})+(P|C|F|p|c|f)+([A-Za-z]{1})+([0-9]{4})+([A-Za-z]{1})");
		final Matcher matcher = pattern.matcher(panNo);
		return matcher.matches();
	}

	static boolean isValidAadhaarNo(String aadhaarNumber) {
		if (isValid(aadhaarNumber))
			aadhaarNumber = aadhaarNumber.trim();
		else
			return false;
		final Pattern pattern = Pattern.compile("([1-9]{1})+([0-9]{11})");
		final Matcher matcher = pattern.matcher(aadhaarNumber);
		return matcher.matches();
	}

	static boolean isValidPin(final String pinNo) {
		if (!isValid(pinNo) || pinNo.length() < 6)
			return false;
		final Pattern pattern = Pattern.compile(PIN_NUMBER_REGEX);
		final Matcher matcher = pattern.matcher(pinNo);
		return matcher.matches() || pinNo.length() == 6;
	}

	static boolean dateValidation(final String inDate) {
		boolean status = false;
		try {
			if (inDate == null)
				return false;
			// set the format to use as a constructor argument
			// SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
			final SimpleDateFormat dateFormat = new SimpleDateFormat("MM/dd/yyyy");
			if (inDate.trim().length() != dateFormat.toPattern().length())
				return false;
			dateFormat.setLenient(false);
			try {
				// parse the inDate parameter
				dateFormat.parse(inDate.trim());
			} catch (final Exception pe) {
				return false;
			}
			return true;
		} catch (final Exception e) {
			status = false;
		}
		return status;
	}

	static boolean isValidDate(final String inDate) {
		if (inDate == null)
			return false;
		// set the format to use as a constructor argument
		final SimpleDateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy");
		if (inDate.trim().length() != dateFormat.toPattern().length())
			return false;
		dateFormat.setLenient(false);
		try {
			// parse the inDate parameter
			dateFormat.parse(inDate.trim());
		} catch (final Exception pe) {
			return false;
		}
		return true;
	}

	static boolean isValidUserName(String userName) {
		if (isValid(userName)) {
			userName = userName.trim();
			final Pattern pattern = Pattern.compile(USER_NAME_REGEX);
			final Matcher matcher = pattern.matcher(userName);
			return matcher.matches();
		}
		return false;
	}

	static boolean isValidCity(final String cityName) {
		if (isValid(cityName)) {
			final Pattern pattern = Pattern.compile(CITY_NAME_REGEX);
			final Matcher matcher = pattern.matcher(cityName);
			return matcher.find();
		}
		return false;
	}

	static boolean isValidCanadaZipCode(final String canadaZipCode) {
		if (isValid(canadaZipCode)) {
			final Pattern pattern = Pattern.compile(CANADIAN_ZIP_CODE_REGEX);
			final Matcher matcher = pattern.matcher(canadaZipCode);
			return matcher.find();
		}
		return false;
	}

	static boolean isValidUkZipCode(final String ukZipCode) {
		if (isValid(ukZipCode)) {
			final Pattern pattern = Pattern.compile(UK_ZIP_CODE_REGEX);
			final Matcher matcher = pattern.matcher(ukZipCode);
			return matcher.find();
		}
		return false;
	}

	static boolean isValidUsaZipCode(final String usaZipCode) {
		if (isValid(usaZipCode)) {
			final Pattern pattern = Pattern.compile(US_ZIP_CODE_REGEX);
			final Matcher matcher = pattern.matcher(usaZipCode);
			return matcher.find();
		}
		return false;
	}

	/**
	 *
	 * Valid SSN Number Format United States Social Security numbers are nine-digit
	 * numbers in the format AAA-GG-SSSS with following rules. The first three
	 * digits called the area number. The area number cannot be 000, 666, or between
	 * 900 and 999. Digits four and five are called the group number and range from
	 * 01 to 99. The last four digits are serial numbers from 0001 to 9999.
	 *
	 */
	static boolean isValidUsaSsnCode(final String ssnNumber) {
		if (isValid(ssnNumber)) {
			final Pattern pattern = Pattern.compile(US_SSN_CODE_REGEX);
			final Matcher matcher = pattern.matcher(ssnNumber);
			return matcher.find();
		}
		return false;
	}

	static boolean isValidUsaEINCode(final String einNumber) {
		if (isValid(einNumber)) {
			final Pattern pattern = Pattern.compile(EIN_CODE_REGEX);
			final Matcher matcher = pattern.matcher(einNumber);
			return matcher.find();
		}
		return false;
	}

	static boolean isValidBusinessNumber(final String businessNumber) {
		if (isValid(businessNumber)) {
			final Pattern pattern = Pattern.compile(ONLY_DIGIT_REGEX);
			final Matcher matcher = pattern.matcher(businessNumber);
			return matcher.find();
		}
		return false;
	}
	static boolean isValid(final Date date) {
		return date!=null;
	}
}
