package com.service.fs.util;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.EmailLogSaveRequest;
import com.service.dto.request.FeedbackDetailRequest;
import com.service.dto.request.UpdateFeedbackDetailsRequest;
import com.service.dto.response.EmailLogSaveResponse;
import com.service.dto.response.FeedBackResponse;
import com.service.dto.response.SurvayResponse;
import com.service.model.fs.FeedbackDetails;
import com.service.service.fs.FeedbackDetailsService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FeedbackUtil {
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	FeedbackDetailsService feedbackDetailsService ; 
	
	public FeedBackResponse getEmptyFeedBackResponse() {
		FeedBackResponse response = FeedBackResponse.builder()
				.responseStatus(getEmptyResponseStatus("FS001", "Unable to get feedback"))
				.build();
		return response;
	}
	public SurvayResponse getEmptySurveyResponse() {
		SurvayResponse response = SurvayResponse.builder()
				.responseStatus(getEmptyResponseStatus("FS001", "Unable to get survey"))
				.build();
		return response;
	}
	public ResponseStatus getEmptyResponseStatus(String code, String message) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(code)
				.message(message)
				.build();
		return responseStatus;
	}
	
	public ResponseStatus sendFeedbackDetails(FeedbackDetailRequest feedbackDetailRequest) {
		ResponseStatus response = getEmptyResponseStatus("FS001", "Invalid Request");
		response.setCode(ApiConstant.FAILD_CODE.getValue());
		response.setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
		response.setMessage(ApiConstant.SEND_FEEDBACK_DETAILS_TYPE_INVALID_MESSAGE.getValue());
		try {
			FeedbackDetails feedbackDetails = feedbackDetailsService.getFeedbackDetails(feedbackDetailRequest.getFeedbackId().toString());
			if(feedbackDetails==null) {
				feedbackDetails= new FeedbackDetails();
				feedbackDetails.setCreatedDate(new Date());
				feedbackDetails.setIsMailSend("N");
				feedbackDetails.setIsClicked("N");
				feedbackDetails.setIsReplied("N");
				feedbackDetails.setStatus("active");
			}
			feedbackDetails.setVendorId(feedbackDetailRequest.getVendorId());
			feedbackDetails.setEventId(feedbackDetailRequest.getEventId());
			feedbackDetails.setEmailId(feedbackDetailRequest.getEmailId());
			feedbackDetails.setUpdatedDate(new Date());
			feedbackDetailsService.save(feedbackDetails);
			
			log.info("feedbackDetails In string format check", feedbackDetails.getUpdatedDate().toString());
			
			HttpHeaders headers = new HttpHeaders();
			headers.set("Content-Type", "application/json");
			EmailLogSaveRequest emailLogSaveRequest = EmailLogSaveRequest.builder().vendorId(feedbackDetails.getVendorId()).eventId(feedbackDetails.getEventId()).emailId(feedbackDetails.getEmailId()).encryptedId(feedbackDetails.getId().toString()).build();
			HttpEntity<EmailLogSaveRequest> request = new HttpEntity<>(emailLogSaveRequest, headers);
			ResponseEntity<EmailLogSaveResponse> emailLogSaveResponse = restTemplate.postForEntity(ApiConstant.URL_COMMUNICATION_SERVICE.getValue()+"/save-communication-email-log", request, EmailLogSaveResponse.class);
			
			EmailLogSaveResponse emailLogSResponse= emailLogSaveResponse.getBody();
			log.info("EmailLogSaveResponse:  " +emailLogSResponse);
			
			response.setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.setMessage(ApiConstant.SEND_FEEDBACK_DETAILS_TYPE_SUCCESS_MESSAGE.getValue());	
		}catch (Exception e) {
			log.info("sendFeedbackDetails(FeedbackDetailRequest feedbackDetailRequest)"+e);
		}
		return response;
	}
	
	public ResponseStatus updateFeedbackDetails(UpdateFeedbackDetailsRequest updateFeedbackRequest) {
		ResponseStatus response = getEmptyResponseStatus("FS001", "Invalid Request");
		response.setCode(ApiConstant.FAILD_CODE.getValue());
		response.setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
		response.setMessage(ApiConstant.SEND_FEEDBACK_DETAILS_TYPE_INVALID_MESSAGE.getValue());
		try {
			FeedbackDetails feedbackDetails = feedbackDetailsService.getFeedbackDetails(updateFeedbackRequest.getValue());
			if(feedbackDetails==null) {
				return response;
			}
			Date date1=null;
			if("isMailSend".equalsIgnoreCase(updateFeedbackRequest.getKey()) && updateFeedbackRequest.getDate()==null) {
				return response;
			}else if ("isMailSend".equalsIgnoreCase(updateFeedbackRequest.getKey())){
				//date1=new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").parse(sDate1);  
				date1 = new Date();
			}
			
			if("isMailSend".equalsIgnoreCase(updateFeedbackRequest.getKey())){
				feedbackDetails.setIsMailSend("Y");
				feedbackDetails.setMailSendDate(date1);
			}else if("isClicked".equalsIgnoreCase(updateFeedbackRequest.getKey())){
				feedbackDetails.setIsClicked("Y");
				feedbackDetails.setIsClickedDate(new Date());
			}else if("IsReplied".equalsIgnoreCase(updateFeedbackRequest.getKey())){
				feedbackDetails.setIsReplied("Y");
				feedbackDetails.setReplyDate(new Date());
			}
			feedbackDetails.setUpdatedDate(new Date());
			feedbackDetailsService.save(feedbackDetails);
			
			response.setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.setMessage(ApiConstant.SEND_FEEDBACK_DETAILS_TYPE_SUCCESS_MESSAGE.getValue());	
		}catch (Exception e) {
			log.info("updateFeedbackDetails(UpdateFeedbackDetailsRequest updateFeedbackRequest)"+e);
		}
		return response;
	}
}
