package com.service.fs.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.AnswerContentDTO;
import com.service.dto.ElementTypeDTO;
import com.service.dto.FeedbackDetailDTO;
import com.service.dto.QuestionAnswerContentDTO;
import com.service.dto.QuestionContentDTO;
import com.service.dto.QuestionDTO;
import com.service.dto.request.AnswersRequest;
import com.service.dto.request.QuestionSubmitRequest;
import com.service.dto.response.AnswersResponse;
import com.service.dto.response.QuestionResponse;
import com.service.model.fs.Answers;
import com.service.model.fs.Elements;
import com.service.model.fs.FeedbackDetails;
import com.service.model.fs.Questions;
import com.service.model.fs.QuestionsContent;
import com.service.service.fs.AnswersService;
import com.service.service.fs.ElementsService;
import com.service.service.fs.FeedbackDetailsService;
import com.service.service.fs.QuestionsContentService;
import com.service.service.fs.QuestionsService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class QuestionUtil {
	
	@Autowired
	QuestionsService questionsService;
	
	@Autowired
	QuestionsContentService questionsContentService;
	
	@Autowired
	ElementsService elementsService;
	
	@Autowired
	AnswersService answersService;
	
	
	@Autowired
	FeedbackDetailsService feedbackDetailsService;
	
	
	public QuestionResponse getQuestionResponse(String vendorId, String eventId, Integer questionType, Integer parentId,   Integer startLimit, Integer endLimit) {
		QuestionResponse response = QuestionResponse.builder()
				.responseStatus(getQuestionResponseStatus("FS001", "Unable to get question"))
				.questionList(getQuestionList(vendorId, eventId, questionType, parentId, startLimit, endLimit))
				.build();
		return response;
	}
	
	public ResponseStatus getQuestionResponseStatus(String code, String message) {
		ResponseStatus responseStatus = ResponseStatus.builder()
				.status(ApiConstant.STATUS_MSG_FAILED.getValue())
				.code(code)
				.message(message)
				.build();
		return responseStatus;
	}
	
	public List<QuestionDTO> getQuestionList(String vendorId, String eventId, Integer questionType, Integer parentId,   Integer startLimit, Integer endLimit) {
		
		List<QuestionDTO> questionListDTO = new LinkedList<>();
		
		List<Object[]> questions =  questionsService.getAllQuestionsListByEventType(vendorId, eventId, questionType, parentId, startLimit, endLimit);
		if(!questions.isEmpty()) {
			for (Object[] obj : questions) {
				List<QuestionContentDTO> questionContentListDTO = new LinkedList<>();
				Integer questionId = ((obj[0]!=null)?Integer.valueOf(obj[0].toString()):0);
				Integer parentIdNew = ((obj[4]!=null)?Integer.valueOf(obj[4].toString()):0);
				if(questionType==1) {
					List<Object[]> questionsType =  questionsService.getAllQuestionsListByEventType(vendorId, eventId, 0, questionId, startLimit, endLimit);
					if(!questionsType.isEmpty()) {
						for (Object[] objType : questionsType) {
							 QuestionContentDTO questionContentsDTO = QuestionContentDTO.builder()
									 .questionContentId(((objType[0]!=null)?Integer.valueOf(objType[0].toString()):0))
									 .questionId(((objType[4]!=null)?Integer.valueOf(objType[4].toString()):0))
									 .questionLable((objType[1]!=null)?objType[1].toString():"")
									 .emojiClass((objType[5]!=null)?objType[5].toString():"")
									 .rightAnswer(0)
									 .build();
							 questionContentListDTO.add(questionContentsDTO);
									 
						}
					}
				}else {
					List<QuestionsContent> questionsContent = questionsContentService.getAllQuestionsContentList(questionId);
					if(!questionsContent.isEmpty()) {
						 for (QuestionsContent questionsContentss : questionsContent) {
							 QuestionContentDTO questionContentsDTO = QuestionContentDTO.builder()
									 .questionContentId(questionsContentss.getID())
									 .questionId(questionsContentss.getQuestionId())
									 .questionLable(questionsContentss.getLable())
									 .rightAnswer(questionsContentss.getAnswer())
									 .build();
							 questionContentListDTO.add(questionContentsDTO);
									 
						}
					}
				}
				
				
				
				QuestionDTO questionDTO = QuestionDTO.builder()
						.questionId((obj[0]!=null)?Integer.valueOf(obj[0].toString()):0)
						.question((obj[1]!=null)?obj[1].toString():"")
						.elementName((obj[2]!=null)?obj[2].toString():"")
						.mendetory((obj[3]!=null)?Integer.valueOf(obj[3].toString()):0)
						.parentId(parentIdNew)
						.questionContentDTO(questionContentListDTO).build();
				
				
				questionListDTO.add(questionDTO);
			}	
		}
		
		return questionListDTO;
	}
	
public AnswersResponse submitQuestion(AnswersRequest answersRequest) {
		AnswersResponse ansResponse = new AnswersResponse();
		ResponseStatus responseStatus = getQuestionResponseStatus("FS001", "Unable to get question");
		ansResponse.setResponseStatus(responseStatus);
		ansResponse.getResponseStatus().setCode(ApiConstant.FAILD_CODE.getValue());
		ansResponse.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
		ansResponse.getResponseStatus().setMessage(ApiConstant.ANSWER_INVALID_MESSAGE.getValue());
		Integer i=0;
		for (AnswerContentDTO answersQuestionDTO : answersRequest.getAnswers()) {
			Answers answers = answersService.getAnswers(answersQuestionDTO.getQuestionId());
			if(answers==null) {
				answers = new Answers();
			}
			answers.setEmoji(answersRequest.getEmoji());
			answers.setEmailId(answersRequest.getUserId());
			answers.setQuestionId(answersQuestionDTO.getQuestionId());
			answers.setAnswer(answersQuestionDTO.getAnswer());
			answers.setActivated(1);
			answers.setDeleted(0);
			answers.setCreatedDate(new Date());
			answers.setUpdatedDate(new Date());
			answersService.save(answers);
			i = i+1;
		}
		
		if(i>0) {
			ansResponse.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
			ansResponse.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			ansResponse.getResponseStatus().setMessage(ApiConstant.ANSWER_SUCCESS_MESSAGE.getValue());
		}
		return ansResponse;
	}

public List<QuestionAnswerContentDTO> getQuestionAnswersList(String vendorId, String eventId, Integer startLimit, Integer endLimit) {
	List<QuestionAnswerContentDTO> questionAnswerContentListDTO = new LinkedList<>();
	List<Object[]> questions =  questionsService.getAllQuestionsAnswersListByEventType(vendorId, eventId, startLimit, endLimit);
	if(!questions.isEmpty()) {
		for (Object[] obj : questions) {
			
			QuestionAnswerContentDTO questionDTO = QuestionAnswerContentDTO.builder()
					.questionId((obj[0]!=null)?Integer.valueOf(obj[0].toString()):0)
					.userId((obj[1]!=null)?obj[1].toString():"")
					.eventType((obj[2]!=null)?obj[2].toString():"")
					.question((obj[3]!=null)?obj[3].toString():"")
					.answers((obj[4]!=null)?obj[4].toString():"").build();
			
			questionAnswerContentListDTO.add(questionDTO);
		}	
	}
	
	return questionAnswerContentListDTO;
}

//public List<EventTypeDTO> getEvents(Integer vendorId, Integer startLimit, Integer endLimit) {
//	List<EventTypeDTO> eventsDTO = new LinkedList<>();
//	List<Object[]> eventTypes =  eventTypeService.getAllEventTypeByLimitList(vendorId, startLimit, endLimit);
//	if(!eventTypes.isEmpty()) {
//		
//		for (Object[] obj : eventTypes) {
//			
//			EventTypeDTO questionDTO = EventTypeDTO.builder()
//					.eventId((obj[0]!=null)?Integer.valueOf(obj[0].toString()):0)
//					.vendorName((obj[1]!=null)? obj[1].toString():"")
//					.eventName((obj[2]!=null)? obj[2].toString():"").build();
//			
//			eventsDTO.add(questionDTO);
//		}	
//	}
//	
//	return eventsDTO;
//}

public List<ElementTypeDTO> getElements() {
	List<ElementTypeDTO> eventsDTO = new LinkedList<>();
	List<Elements> elements =  elementsService.getAllElementsList();
	if(!elements.isEmpty()) {
		
		for (Elements elementObj : elements) {
			
			ElementTypeDTO questionDTO = ElementTypeDTO.builder()
					.elementId(elementObj.getID())
					.elementName(elementObj.getElement()).build();
			
			eventsDTO.add(questionDTO);
		}	
	}
	
	return eventsDTO;
}

//public List<VendorDTO> getVendors(Integer startLimit, Integer endLimit) {
//	List<VendorDTO> eventsDTO = new LinkedList<>();
//	List<Object[]> eventTypes =  vendorsService.getAllVendorsByLimitList(startLimit, endLimit);
//	if(!eventTypes.isEmpty()) {
//		
//		for (Object[] obj : eventTypes) {
//			
//			VendorDTO questionDTO = VendorDTO.builder()
//					.vendorId((obj[0]!=null)?Integer.valueOf(obj[0].toString()):0)
//					.vendorName((obj[1]!=null)? obj[1].toString():"").build();
//			
//			eventsDTO.add(questionDTO);
//		}	
//	}
//	
//	return eventsDTO;
//}

//public EventTypeResponse submitEvents(EventTypeRequest eventTypeRequest) {
//	EventTypeResponse ansResponse = new EventTypeResponse();
//	ResponseStatus responseStatus = getQuestionResponseStatus("FS001", "Invalid Request");
//	ansResponse.setResponseStatus(responseStatus);
//	ansResponse.getResponseStatus().setCode(ApiConstant.FAILD_CODE.getValue());
//	ansResponse.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
//	ansResponse.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
//	
//	EventType eventTypeName = eventTypeService.getEventTypeByEventName(eventTypeRequest.getVendorId(), eventTypeRequest.getEventName());
//	
//	EventType eventType = new EventType();
//	if(eventTypeRequest.getEventId()!=null) {
//		eventType = eventTypeService.getEventType(eventTypeRequest.getEventId());
//		if(eventType==null) {
//			eventType = new EventType();
//		}
//	}else {
//		if(eventTypeName !=null) {
//			ansResponse.getResponseStatus().setCode(ApiConstant.FAILD_CODE.getValue());
//			ansResponse.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
//			ansResponse.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_ALREADY_MESSAGE.getValue());
//			return ansResponse;
//		}
//	}
//		eventType.setVendorId(eventTypeRequest.getVendorId());
//		eventType.setEventName(eventTypeRequest.getEventName());
//		eventType.setActivated(1);
//		eventType.setDeleted(0);
//		eventType.setCreatedDate(new Date());
//		eventType.setUpdatedDate(new Date());
//		eventTypeService.save(eventType);
//	
//		ansResponse.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
//		ansResponse.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
//		ansResponse.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_SUCCESS_MESSAGE.getValue());
//	return ansResponse;
//}
	public ResponseStatus submitQuestion(QuestionSubmitRequest questionsSubmit) {
		ResponseStatus response = getQuestionResponseStatus("FS001", "Invalid Request");
		response.setCode(ApiConstant.FAILD_CODE.getValue());
		response.setStatus(ApiConstant.STATUS_MSG_FAILED.getValue());
		response.setMessage(ApiConstant.QUESTION_TYPE_INVALID_MESSAGE.getValue());
		try {
			Questions questions = questionsService.getQuestions(questionsSubmit.getQuestionId());
			if(questions==null) {
				questions= new Questions();
				questions.setCreatedDate(new Date());
			}
			questions.setVendorId(questionsSubmit.getVendorId());
			questions.setEventId(questionsSubmit.getEventId());
			questions.setQuestionType(questionsSubmit.getQuestionType());
			questions.setQuestion(questionsSubmit.getQuestion());
			questions.setElementId(questionsSubmit.getElementId());
			questions.setMendetory(questionsSubmit.getMendetory());
			questions.setParentId(questionsSubmit.getParentId());
			questions.setEmojiClass(questionsSubmit.getEmojiClass());
			questions.setActivated(1);
			questions.setDeleted(0);
			questions.setUpdatedDate(new Date());
			questionsService.save(questions);
		
			Integer questionId=questions.getID();
			questionsContentService.deleteQuestionContentData(questionId);
			
			List<QuestionContentDTO> questContsubmit=questionsSubmit.getQuestionContentDTO();
			if(!questContsubmit.isEmpty() && questContsubmit!=null) {
				for(QuestionContentDTO ques :questContsubmit) {
					QuestionsContent questContent= new QuestionsContent();
					questContent.setQuestionId(questionId);
					questContent.setLable(ques.getQuestionLable());
					questContent.setAnswer(ques.getRightAnswer());
					questContent.setActivated(1);
					questContent.setDeleted(0);
					questContent.setCreatedDate(new Date());
					questContent.setUpdatedDate(new Date());
					questionsContentService.save(questContent);
				}
			}
			response.setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.setMessage(ApiConstant.QUESTION_TYPE_SUCCESS_MESSAGE.getValue());	
		}catch (Exception e) {
			log.info("submitQuestion(QuestionSubmitRequest questionsSubmit)"+e);
		}
		return response;
	}
	public List<QuestionSubmitRequest> getAllQuestionList(String vendorId, String eventId, Integer startLimit, Integer endLimit) {
		try {
		List<QuestionSubmitRequest> questionListing = new ArrayList<>();
		
		List<Questions> questions =  questionsService.getALLActiveQuestions(vendorId, eventId, startLimit, endLimit);
			if(!questions.isEmpty()) {
				for (Questions ques : questions) {
					QuestionSubmitRequest questionList= QuestionSubmitRequest.builder()
							.questionId(ques.getID())
							.eventId(ques.getEventId())
							.questionType(ques.getQuestionType())
							.question(ques.getQuestion())
							.elementId(ques.getElementId())
							.parentId(ques.getParentId())
							.mendetory(ques.getMendetory())
							.emojiClass(ques.getEmojiClass())
							.build();
					questionListing.add(questionList);
				}	
			}
		return questionListing;	
		}catch (Exception e) {
			log.info("getAllQuestionList()"+e);
		}
		return null;
	}
	
	public List<QuestionContentDTO> getQuestionLableList(Integer questionId) {
		try {
		List<QuestionContentDTO> questContList = new ArrayList<>();
		List<QuestionsContent> questCont =  questionsContentService.getAllQuestionsContentList(questionId);
			if(!questCont.isEmpty()) {
				for (QuestionsContent cont : questCont) {
					 QuestionContentDTO questionContentsDTO = QuestionContentDTO.builder()
							 .questionContentId(cont.getID())
							 .questionId(cont.getQuestionId())
							 .questionLable(cont.getLable())
							 .rightAnswer(cont.getAnswer())
							 .build();
					 questContList.add(questionContentsDTO);
				}	
			}
			return questContList;	
		}catch (Exception e) {
			log.info("getQuestionLableList(Integer questionId)"+e);
		}
		return null;
	}
	
	public FeedbackDetailDTO getFeedbackDetail(String feedbackId) {
		FeedbackDetailDTO feedbackDetailDTO = new FeedbackDetailDTO();
		FeedbackDetails feedbackDetails =  feedbackDetailsService.getFeedbackDetails(feedbackId);
		if(feedbackDetails!=null) {
			feedbackDetailDTO = FeedbackDetailDTO.builder().
			feedbackId(feedbackDetails.getId())
			.vendorId(feedbackDetails.getVendorId())
			.eventId(feedbackDetails.getEventId())
			.emailId(feedbackDetails.getEmailId())
			.build();
		}
		return feedbackDetailDTO;
	}
}