package com.service.fs.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.service.constant.ApiConstant;
import com.service.dto.request.FeedBackRequest;
import com.service.dto.response.SurvayResponse;
import com.service.fs.util.FeedbackUtil;

@RestController
@RequestMapping("/feedback-survey/api/v1")
public class SurveyController {
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	FeedbackUtil feedbackUtil;

	@RequestMapping(value = "/get-survey", method = { RequestMethod.GET, RequestMethod.POST },
	consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public SurvayResponse getFeedbackContent(@RequestBody FeedBackRequest feedBackRequest) {
		SurvayResponse response = feedbackUtil.getEmptySurveyResponse();
		if(!"IS".equalsIgnoreCase(feedBackRequest.getVendorId())) {
			response.getResponseStatus().setMessage("Invalid vendor!");
			return response;
		}
		if(!"SIGNUP".equalsIgnoreCase(feedBackRequest.getEventId())) {
			response.getResponseStatus().setMessage("Invalid event!");
			return response;
		}
		response.getResponseStatus().setMessage("Find dummy survey");
		response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		return response;
	}
}
