package com.service.fs.controller;

import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.ElementTypeDTO;
import com.service.dto.FeedbackDetailDTO;
import com.service.dto.QuestionAnswerContentDTO;
import com.service.dto.QuestionContentDTO;
import com.service.dto.QuestionDTO;
import com.service.dto.request.AnswersRequest;
import com.service.dto.request.FeedBackRequest;
import com.service.dto.request.FeedbackDetailRequest;
import com.service.dto.request.QuestionListRequest;
import com.service.dto.request.QuestionRequest;
import com.service.dto.request.QuestionSubmitRequest;
import com.service.dto.request.UpdateFeedbackDetailsRequest;
import com.service.dto.response.AnswersResponse;
import com.service.dto.response.ElementTypeResponse;
import com.service.dto.response.FeedBackResponse;
import com.service.dto.response.FeedbackDetailResponse;
import com.service.dto.response.QuestionAnswersResponse;
import com.service.dto.response.QuestionLableListResponse;
import com.service.dto.response.QuestionListResponse;
import com.service.dto.response.QuestionResponse;
import com.service.fs.util.FeedbackUtil;
import com.service.fs.util.QuestionUtil;

@RestController
@RequestMapping("/feedback-survey/api/v1")
public class FeedbackController {
	@Autowired
	RestTemplate restTemplate;

	@Autowired
	FeedbackUtil feedbackUtil;
	
	@Autowired
	QuestionUtil questionUtil;

	
	@RequestMapping(value = "/get-feedback", method = { RequestMethod.POST }, consumes = {MediaType.APPLICATION_JSON_VALUE})
	public FeedBackResponse getFeedbackContent(@RequestBody FeedBackRequest feedBackRequest) {
		FeedBackResponse response = feedbackUtil.getEmptyFeedBackResponse();
		if (!"IS".equalsIgnoreCase(feedBackRequest.getVendorId())) {
			response.getResponseStatus().setMessage("Invalid vendor!");
			return response;
		}
		
		if (!"SIGNUP".equalsIgnoreCase(feedBackRequest.getEventId())) {
			response.getResponseStatus().setMessage("Invalid event!");
			return response;
		}
		response.getResponseStatus().setMessage("Find dummy feedback");
		response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		return response;
	}
	
	@RequestMapping(value = "/get-question", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public QuestionResponse getQuestion(@RequestBody QuestionRequest questionRequest) {
		final QuestionResponse response = new QuestionResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);
		response.setEventType(questionRequest.getEventId()); 
		if(StringUtils.isEmpty(questionRequest.getEventId())) {
			response.getResponseStatus().setCode(ApiConstant.EVENTTYPE_INVALID_CODE.getValue());
			response.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
			return response;
		}
		List<QuestionDTO> questionLists = questionUtil.getQuestionList(questionRequest.getVendorId(), questionRequest.getEventId(), questionRequest.getQuestionType(), questionRequest.getParentId(),  questionRequest.getStartLimit(), questionRequest.getEndLimit());
		if(!questionLists.isEmpty())
		{
			response.setQuestionList(questionLists);
			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		}else {
			response.getResponseStatus().setCode(ApiConstant.EVENTTYPE_INVALID_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
			return response;
		}
		
		return response;
	}
	
	@RequestMapping(value = "/submit-answers", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public AnswersResponse submitAnswers(@RequestBody AnswersRequest answersRequest) {
		final AnswersResponse response = new AnswersResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);
		response.setEventType(answersRequest.getEventType());
		if(StringUtils.isEmpty(answersRequest.getUserId())) {
			response.getResponseStatus().setMessage(ApiConstant.ANSWER_INVALID_MESSAGE.getValue());
			return response;
		}
		AnswersResponse responseq = questionUtil.submitQuestion(answersRequest);
		if(responseq!=null)
		{
			response.getResponseStatus().setCode(responseq.getResponseStatus().getCode());
			response.getResponseStatus().setStatus(responseq.getResponseStatus().getStatus());
			response.getResponseStatus().setMessage(responseq.getResponseStatus().getMessage());
		}else {
			return response;
		}
		return response;
	}
	
	@RequestMapping(value = "/get-question-answers", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public QuestionAnswersResponse getQuestionAnswers(@RequestBody QuestionRequest questionRequest) {
		final QuestionAnswersResponse response = new QuestionAnswersResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);
		response.setEventType(questionRequest.getEventId());
//		if(StringUtils.isEmpty(questionRequest.getEventType())) {
//			response.getResponseStatus().setCode(ApiConstant.EVENTTYPE_INVALID_CODE.getValue());
//			response.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
//			return response;
//		}
		List<QuestionAnswerContentDTO> questionLists = questionUtil.getQuestionAnswersList(questionRequest.getVendorId(),  questionRequest.getEventId(), Integer.valueOf(questionRequest.getStartLimit()), Integer.valueOf(questionRequest.getEndLimit()));
		if(!questionLists.isEmpty())
		{
			response.setQuestionAnswersList(questionLists);
			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		}else {
			response.getResponseStatus().setCode(ApiConstant.FAILD_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.getResponseStatus().setMessage(ApiConstant.ANSWER_NO_MESSAGE.getValue());
			return response;
		}
		return response;
	}
	
//	@RequestMapping(value = "/submit-events", method = { RequestMethod.GET, RequestMethod.POST },
//			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
//	public EventTypeResponse submitEvents(@RequestBody EventTypeRequest eventTypeRequest) {
//		final EventTypeResponse response = new EventTypeResponse();
//		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
//		response.setResponseStatus(responseStatus);
//		if(StringUtils.isEmpty(eventTypeRequest.getEventName())) {
//			response.getResponseStatus().setMessage(ApiConstant.ANSWER_INVALID_MESSAGE.getValue());
//			return response;
//		}
//		EventTypeResponse responseq = questionUtil.submitEvents(eventTypeRequest);
//		if(responseq!=null)
//		{
//			response.getResponseStatus().setCode(responseq.getResponseStatus().getCode());
//			response.getResponseStatus().setStatus(responseq.getResponseStatus().getStatus());
//			response.getResponseStatus().setMessage(responseq.getResponseStatus().getMessage());
//		}else {
//			return response;
//		}
//		return response;
//	}
	
//	@RequestMapping(value = "/get-events", method = { RequestMethod.GET, RequestMethod.POST },
//			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
//	public EventTypeResponse getEvents(@RequestBody EventTypeRequest eventTypeRequest) {
//		final EventTypeResponse response = new EventTypeResponse();
//		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
//		response.setResponseStatus(responseStatus);
//
//		List<EventTypeDTO> eventTypeList = questionUtil.getEvents(eventTypeRequest.getVendorId(), eventTypeRequest.getStartLimit(), eventTypeRequest.getEndLimit());
//		if(!eventTypeList.isEmpty())
//		{
//			response.setEventTypeList(eventTypeList);
//			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
//			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
//		}else {
//			response.getResponseStatus().setCode(ApiConstant.EVENTTYPE_INVALID_CODE.getValue());
//			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
//			response.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
//			return response;
//		}
//		return response;
//	}
	
	@RequestMapping(value = "/get-elements", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ElementTypeResponse getElements() {
		final ElementTypeResponse response = new ElementTypeResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);

		List<ElementTypeDTO> elementTypeList = questionUtil.getElements();
		if(!elementTypeList.isEmpty())
		{
			response.setElementTypeList(elementTypeList);
			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		}else {
			response.getResponseStatus().setCode(ApiConstant.ELEMENTS_INVALID_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.getResponseStatus().setMessage(ApiConstant.ELEMENTS_INVALID_MESSAGE.getValue());
			return response;
		}
		return response;
	}
	
	@RequestMapping(value = "/submit-question", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseStatus submitQuestion(@RequestBody QuestionSubmitRequest questionSubmitRequest) {
		ResponseStatus response = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response = questionUtil.submitQuestion(questionSubmitRequest);
		if(response.getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return response;
		}
		return response;
	}
	
	@RequestMapping(value = "/get-question-list", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public QuestionListResponse getQuestionList(@RequestBody QuestionListRequest questionListRequest) {
		final QuestionListResponse response = new QuestionListResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);
		List<QuestionSubmitRequest> questionList = questionUtil.getAllQuestionList(questionListRequest.getVendorId(), questionListRequest.getEventId(),  questionListRequest.getStartLimit(), questionListRequest.getEndLimit());
		if(questionList==null) {
			return response;
		}else if(questionList.isEmpty()) {
			responseStatus.setCode(ApiConstant.SUCCESS_CODE.getValue());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage("Question not available for coming request");
			return response;
		}
		responseStatus.setCode(ApiConstant.SUCCESS_CODE.getValue());
		responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		responseStatus.setMessage("Question Message List");
		response.setResponseStatus(responseStatus);
		response.setQuestionList(questionList);
		return response;
	}
	
	@RequestMapping(value = "/get-question-content-list", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public QuestionLableListResponse getQuestionContentList(@RequestBody QuestionListRequest questionListRequest) {
		final QuestionLableListResponse response = new QuestionLableListResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);
		if(questionListRequest==null) {
			response.setResponseStatus(responseStatus);
		}
		List<QuestionContentDTO> questContList = questionUtil.getQuestionLableList(questionListRequest.getQuestionId());
		if(questContList==null) {
			return response;
		}else if(questContList.isEmpty()) {
			responseStatus.setCode(ApiConstant.SUCCESS_CODE.getValue());
			responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			responseStatus.setMessage("Question Content not available for that question");
			response.setResponseStatus(responseStatus);
			return response;
		}
		responseStatus.setCode(ApiConstant.SUCCESS_CODE.getValue());
		responseStatus.setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		responseStatus.setMessage("Question content List");
		response.setResponseStatus(responseStatus);
		response.setQuestionContentDTO(questContList);
		return response;
	}
	
	@RequestMapping(value = "/get-answers-user", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public QuestionResponse getAnswerByUser(@RequestBody QuestionRequest questionRequest) {
		final QuestionResponse response = new QuestionResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);
		response.setEventType(questionRequest.getEventId()); 
		if(StringUtils.isEmpty(questionRequest.getEventId())) {
			response.getResponseStatus().setCode(ApiConstant.EVENTTYPE_INVALID_CODE.getValue());
			response.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
			return response;
		}
		List<QuestionDTO> questionLists = questionUtil.getQuestionList(questionRequest.getVendorId(), questionRequest.getEventId(), questionRequest.getQuestionType(), questionRequest.getParentId(),  questionRequest.getStartLimit(), questionRequest.getEndLimit());
		if(!questionLists.isEmpty())
		{
			response.setQuestionList(questionLists);
			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
		}else {
			response.getResponseStatus().setCode(ApiConstant.EVENTTYPE_INVALID_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.getResponseStatus().setMessage(ApiConstant.EVENTTYPE_INVALID_MESSAGE.getValue());
			return response;
		}
		
		return response;
	}
	
//	@RequestMapping(value = "/get-vendor", method = { RequestMethod.GET, RequestMethod.POST },
//			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
//	public VendorResponse getVendors(@RequestBody VendorRequest vendorRequest) {
//		final VendorResponse response = new VendorResponse();
//		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
//		response.setResponseStatus(responseStatus);
//
//		List<VendorDTO> vendorList = questionUtil.getVendors(vendorRequest.getStartLimit(), vendorRequest.getEndLimit());
//		if(!vendorList.isEmpty())
//		{
//			response.setVendorList(vendorList);
//			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
//			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
//			response.getResponseStatus().setMessage(ApiConstant.REQUEST_SUCCESS_MESSAGE.getValue());
//		}else {
//			response.getResponseStatus().setCode(ApiConstant.VENDORS_INVALID_CODE.getValue());
//			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
//			response.getResponseStatus().setMessage(ApiConstant.VENDORS_INVALID_MESSAGE.getValue());
//			return response;
//		}
//		return response;
//	}
//	
	@RequestMapping(value = "/send-feedback-details", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ResponseStatus> sendFeedbackDetails(@RequestBody FeedbackDetailRequest feedbackDetailRequest) {
		ResponseStatus response = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response = feedbackUtil.sendFeedbackDetails(feedbackDetailRequest);
		if(response.getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}

	
	@RequestMapping(value = "/update-feedback-details", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public ResponseEntity<ResponseStatus> updateFeedbackDetails(@RequestBody UpdateFeedbackDetailsRequest updateFeedbackRequest) {
		ResponseStatus response = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response = feedbackUtil.updateFeedbackDetails(updateFeedbackRequest);
		if(response.getStatus()==ApiConstant.STATUS_MSG_FAILED.getValue()) {
			return ResponseEntity.ok().body(response);
		}
		return ResponseEntity.ok().body(response);
	}
	
	@RequestMapping(value = "/get-feedback-detail/{feedbackId}", method = { RequestMethod.GET, RequestMethod.POST },
			consumes = {MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE})
	public FeedbackDetailResponse getFeedBackDetail(@PathVariable("feedbackId") String feedbackId) {
		final FeedbackDetailResponse response = new FeedbackDetailResponse();
		ResponseStatus responseStatus = feedbackUtil.getEmptyResponseStatus(ApiConstant.FAILD_CODE.getValue(), ApiConstant.REQUEST_INVALID_MESSAGE.getValue());
		response.setResponseStatus(responseStatus);

		FeedbackDetailDTO feedBackDetail = questionUtil.getFeedbackDetail(feedbackId);
		if(feedBackDetail!=null)
		{
			response.setFeedbackDetail(feedBackDetail);
			response.getResponseStatus().setCode(ApiConstant.SUCCESS_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.getResponseStatus().setMessage(ApiConstant.REQUEST_SUCCESS_MESSAGE.getValue());
		}else {
			response.getResponseStatus().setCode(ApiConstant.SEND_FEEDBACK_DETAILS_TYPE_INVALID_CODE.getValue());
			response.getResponseStatus().setStatus(ApiConstant.STATUS_MSG_SUCCESS.getValue());
			response.getResponseStatus().setMessage(ApiConstant.SEND_FEEDBACK_DETAILS_TYPE_INVALID_MESSAGE.getValue());
			return response;
		}
		return response;
	}
	
}