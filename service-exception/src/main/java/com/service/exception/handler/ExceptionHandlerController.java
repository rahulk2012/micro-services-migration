package com.service.exception.handler;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;

@RestControllerAdvice
public class ExceptionHandlerController {

	@ExceptionHandler({ SeriException.class })
	public ResponseEntity<ResponseStatus> handleAdminException(final SeriException exception) {
		return getPickrAdminResponseResponseEntity(exception.getMessage(), exception.getErrorCode(), exception);
	}

	private ResponseEntity<ResponseStatus> getPickrAdminResponseResponseEntity(final String statusCode,
			final String message, final SeriException exception) {

		final ResponseStatus response = ResponseStatus.builder().status(ApiConstant.STATUS_CODE_FAILED.getValue())
				.code(statusCode).message(message).build();
		final MultiValueMap<String, String> headers = new LinkedMultiValueMap<>(2);
		return new ResponseEntity<>(response, headers, HttpStatus.OK);
	}
}