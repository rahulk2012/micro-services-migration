package com.service.exception.handler;

import com.service.constant.ApiConstant;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Setter
@Getter
public class SeriException extends RuntimeException {

	private static final long serialVersionUID = 8764418922855205493L;
	private String errorCode;
	private Throwable rootCause;
	public SeriException(final String errorCode, final Exception e) {
		super(e);
		log.info(ApiConstant.ERROR_CODE + errorCode + ApiConstant.ERROR_MESSAGE + e.getMessage());
		this.errorCode = errorCode;
	}

	public SeriException(final String errorCode, final Throwable cause) {
		rootCause = cause;
		log.info(ApiConstant.ERROR_CODE + errorCode + ApiConstant.ERROR_MESSAGE + cause.getMessage());
		this.errorCode = errorCode;
	}

	public SeriException(final String errorCode, final String errorMessage) {
		super(errorMessage);
		log.info(ApiConstant.ERROR_CODE + errorCode + ApiConstant.ERROR_MESSAGE + errorMessage);
		this.errorCode = errorCode;
	}
}