package com.service.common.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
@ToString
public class MasterDTO {
	
	@ApiModelProperty(notes = "key", name="key", required=true, value="key")
	private String key;
	@ApiModelProperty(notes = "value", name="value", required=true, value="value")
	private String value;
	@ApiModelProperty(notes = "orderBy", name="orderBy", required=true, value="orderBy")
	private String orderBy;
}