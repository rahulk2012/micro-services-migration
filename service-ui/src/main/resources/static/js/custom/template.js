function requestForEventsOnly() {
	vendorId = $('#templateForm #vendorId').val();
	if(vendorId==''){
		showMessage(true, 'Please select verdor.');
		return false;
	}
	hideMessage('');   
	$.ajax({
        type : "GET",
        url : API_BASE_URL+"/common/get-events/"+vendorId,
        dataType : 'json',
        success : function(json) {
        	if(json!=""){
                if(json.responseStatus.status == "FAILED" || json.responseStatus.status == "EXCEPTION" ){
                	showMessage(true, json.responseStatus.message);
                }else{
                	$('#eventId').empty();
                	$('#eventId').append('<option value="">Select event</option>');
                	$.each(json.events, function (key, data) {
                		$('#eventId').append(getSingleOption(key, data));
                	});
                	$('#eventId').append(getSingleOptionForOther("0","ADD NEW..."));
                }
                return false;
        	}
        }
    });
}

function requestForVendorEvents() {
	hideMessage('');   
	$.ajax({
        type : "GET",
        url : UI_BASE_URL+"/dashboard/get-vendor-events/"+vendorId,
        data : getVendorEvents(vendorId),
        dataType : 'json',
        success : function(json) {
        	if(json!=""){
                if(json.responseStatus.status == "FAILED" || json.responseStatus.status == "EXCEPTION" ){
                	showMessage(true, json.responseStatus.message);
                }else{
                	$('#vendorId').html('');
                	$('#vendorId').append('<option value="">Select Vendor</option>');
                    $.each(json.vendors, function (key, data) {
                    	$('#vendorId').append(getSingleOption(key, data));
                    });
                    $('#eventId').html('');
	        	   $.each(json.events, function (key, data) {
	        		   $('#eventId').append(getSingleOption(key, data));
	        	   });
	        	   $('#eventId').append(getSingleOptionForOther("0","ADD NEW..."));
                }
                return false;
        	}
        }
    });
}

function getVendorEvents(vendorId){
	var requestBody ={};
	console.log('getVendorEvents '+requestBody);
	return requestBody;
}

function requestForTemplateView(vendorId, eventId, templateType) {
	hideMessage('');
	$.ajax({
        type : "GET",
        url : UI_BASE_URL+"/dashboard/get-template/"+vendorId+"/"+eventId+"/"+templateType,
        dataType : 'json',
        success : function(json) {
	        if(json!=""){
	            if(json.responseStatus.status == "FAILED" || json.responseStatus.status == "EXCEPTION" ){
	            	showMessage(true, json.responseStatus.message);
	            }else{
	            	console.log(json);
	            	if(json.templateData!=null){
	            		editorContent('editor1', json.templateData.template)
	            		$('#templateId').val(json.templateData.templateId);
	            		$('#sendMailSubject').val(json.templateData.sendMailSubject);
	            	}else if(json.templateData==null){
	            		initEditor(1, 'editor1','Add Template Content', false);
	            		$('#templateId').val("");
	            		$('#sendMailSubject').val("");
	            	}
	            }
	            return false;
	        }
        }
    });
}

//function getTemplateByVendorAndEventAndTemplateType(vendorId, eventId, templateType){
//	var requestBody ={};
//	requestBody['vendorId']=vendorId;
//	requestBody['eventId']=eventId;
//	requestBody['templateType']=templateType;
//	requestBody = JSON.stringify(requestBody)
//	console.log('getTemplateByVendorAndEventAndTemplateType '+requestBody);
//	return requestBody;
//}

//$("#saveTemplate").click(function () {
//	requestForSaveTemplate();
//});  

function validateRequestForSaveTemplate(){
	if($('#vendorId').val()==0 || $('#vendorId').val()==''){
		showMessage(true, 'vendor is required.');
		return false;
	}
	
	if($('#eventId').val()==''){
		showMessage(true, 'event is required.');
		return false;
	}
	
	if($('#sendMailSubject').val()==undefined || $('#sendMailSubject').val()==''){
		showMessage(true, ' Mail subject is required.');
		return false;
	}
	
	if($('#eventId').val()=="0"){
		if($('#newEvent').val()==undefined || $('#newEvent').val()==''){
			showMessage(true, 'Add New Event is required.');
			return false;
		}
	}

	if(editorContent('editor1')==""){
		showMessage(true, 'please  add template content');
		return false;
	}
	
	return true;
}	

function requestForSaveTemplate() {
	templateType=1;
	hideMessage('');
	if(!validateRequestForSaveTemplate()){
		return false;
	}
    $.ajax({
	    type : "POST",
	    url : UI_BASE_URL+"/dashboard/save-communication-template",
	    data : getTemplateBySaveTemplate(templateType),
	    dataType : 'json',
	    success : function(json) {
		    if(json!=""){
	            if(json.responseStatus.status == "FAILED" || json.responseStatus.status == "EXCEPTION" ){
	            	showMessage(true, json.responseStatus.message);
	            }else{
	            	console.log(json);
	            	requestForVendorEvents(vendorId);
	            	showMessage(true, json.responseStatus.message);
	            }
	            return false;
		    }
	    }
    });
}

function getTemplateBySaveTemplate(templateType){
		var templateSaveRequest={};
		if(editor1!=undefined){
			templateSaveRequest['emailContent']=editorContent('editor1').replace(/'/g, '&#39;');
		}
		if($('#eventId').val()!="0"){
			templateSaveRequest['emailSubject']=$('#eventId option:selected').text().trim();
			templateSaveRequest['eventId']=$('#eventId').val();
		}else{
			templateSaveRequest['emailSubject']=$('#newEvent').val().trim();
			templateSaveRequest['eventId']="";
		}
		templateSaveRequest['sendMailSubject']=$('#sendMailSubject').val().trim();
		templateSaveRequest['vendorId']=$('#vendorId').val();
		templateSaveRequest['templateType']=templateType;
		templateSaveRequest['templateId']=$('#templateId').val();
		
		templateSaveRequest = JSON.stringify(templateSaveRequest)
		console.log('getTemplateBySaveTemplate()'+templateSaveRequest);
		return templateSaveRequest;
	}

function callEventsByVendor() {
	eventId = $("#eventId").val();
	vendorId = $("#vendorId").val();
	if (eventId == "") {
		$("#eventAdd").hide();
		$('#templateId').val("");
		$('#newEvent').val("");
		$('#sendMailSubject').val("");
		editorContent('editor1','');
	} else if (eventId == "0") {
		editorContent('editor1','');
		initEditor(1, 'editor1', 'Add Template Content', false);
		$('#templateId').val("");
		$('#sendMailSubject').val("");
		$("#eventAdd").show();
	} else {
		$("#eventAdd").hide();
		$('#newEvent').val("");
		$('#sendMailSubject').val("");
		var templateType=1;
		requestForTemplateView(vendorId, eventId, templateType);
	}
}

function validateRequestForMailTemplate(formId){
	return true;
}
function callForMailTemplate(formId) {
	if(!validateRequestForMailTemplate(formId)){
		return false;
	}
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/mailtemplate"),
		data : JSON.stringify(getRequestForMailTemplate(formId)),
		dataType : 'html',
		success : function(htmlContent) {
			if(htmlContent!=""){
				$(".app-main__inner").html(htmlContent);
				requestForVendorEvents(vendorId);
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForMailTemplate(formId){
	var vendorRequest = {};
	return vendorRequest;
}
function callForCronslist(formId) {
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/crons-listing"),
		data : JSON.stringify(getRequestForCronslist(formId)),
		dataType : 'html',
		success : function(htmlContent) {
			if(htmlContent!=""){
				$(".app-main__inner").html(htmlContent);
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}
function getRequestForCronslist(formId){
	var cronRequest = {};
	return cronRequest;
}
