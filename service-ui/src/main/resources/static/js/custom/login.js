$(document).ready(function() {
	// callForGetElementMaster();
});

function validateRequestForLogin(formId) {

	if ($("#email").val() == null || $("#email").val() == '') {
		showMessage(true, 'Please fill email');
		return false
	}
	if ($("#password").val() == null || $("#password").val() == '') {
		showMessage(true, 'Please fill password');
		return false
	}

	return true;
}

function callForLogin(formId) {
	if (!validateRequestForLogin(formId)) {
		return false;
	}

	$.ajax({
		type : "POST",
		url : getURLFor("/login"),
		contentType : "application/json",
		data : JSON.stringify(getRequestForLogin(formId)),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(jsonContent) {
			if (jsonContent != "") {
				if (jsonContent.status == 'SUCCESS') {
					console.log(jsonContent.message);
					goAhead('/dashboard/admin', jsonContent, '');
				}
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForLogin(formId) {

	var loginRequest = {};
	loginRequest['username'] = $("#" + formId + " #email").val();
	loginRequest['password'] = $("#" + formId + " #password").val();

	return loginRequest;
}
