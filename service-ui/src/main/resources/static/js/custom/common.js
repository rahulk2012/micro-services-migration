var TECHNICAL_GLITCH = 'Sorry for inconvenience, system has encountered technical glitch.';
var SERVICE_UNAVAILABLE = 'Temporarily Ticket Service is not available!';
var UI_BASE_URL = "http://localhost:9000";
var API_BASE_URL = "http://localhost:8080/gateway/api/v1";
function getURLFor(suffixUrl) {
	return UI_BASE_URL + suffixUrl;
}

function goAhead(url, hash, role) {
	if (role == '') {
		var form = $(
				'<form action="' + url + '" method="POST">'
				+ '<input type="hidden" name="token" id="token" value="'+ hash.token + '" />'
				+ '<input type="hidden" name="uniqueId" id="uniqueId" value="'+ hash.encryptedUserId + '" />' 
				+ '</form>'
				);
		$('body').append(form);
		$(form).submit();
	} else {
		window.close();
		window.open(self.location.href,'_blank','toolbar=no, location=no, directories=no, status=no, menubar=no, scrollbars=yes, resizable=no, copyhistory=no');
	}
}

function showMessage(isWarnig, message, id) {
	window.scrollTo({ top: 100, left: 100, behavior: 'smooth' });
	if (isWarnig) {
		$('#errorHeading').html('Error! Be focus on work');
		$('#statusMessage').addClass('danger-color');
		$('#statusMessage').removeClass('success-color');
	} else {
		$('#errorHeading').html('Information!');
		$('#statusMessage').removeClass('danger-color');
		$('#statusMessage').addClass('success-color');
	}
	$('#statusMessage').html(message);
	$('#modalMessage').modal("show");
		setTimeout(function(){
			$('#modalMessage').modal("hide");
		}, 3000);
}

function hideMessage(id) {
	$('#errorHeading').html('');
	$('#statusMessage').removeClass('success-color');
	$('#statusMessage').removeClass('danger-color');
	$('#statusMessage').html('');
	$('#modalMessage').modal("hide");
}


function getSingleOption(key, data){
	return '<option value="'+data.key+'">'+data.value+'</option>';
}

function getSingleOptionForOther(key, data){
	return '<option value="'+key+'">'+data+'</option>';
}

$.ajaxSetup({
	timeout: 600000,
	crossDomain: true,
	cache : false,
	contentType: 'application/json',
    beforeSend: function (xhr){
       xhr.setRequestHeader("Accept","application/json");
       xhr.setRequestHeader("Authorization","Bearer "+token);        
    }
});
$(document).ajaxError(function(event, jqxhr, settings, exception) {
	console.log("event is" + event + "jqxhr is" + jqxhr + "settings" + settings + "exception is" + exception);
	//customLoader(false);
	if (isJson(jqxhr.responseText)) {
		var parseResponse = JSON.parse(jqxhr.responseText);
		console.log("parse Response is:" + jqxhr.status);
		var hasProperty = parseResponse.hasOwnProperty("message");
		if (hasProperty) {
			showMessage(true, parseResponse.message)
		} else {
			showMessage(true, TECHNICAL_GLITCH);
		}
	} else {
		showMessage(true, TECHNICAL_GLITCH);
	}
});
function isJson(str) {
    try {
        JSON.parse(str);
    } catch (e) {
        return false;
    }
    return true;
}