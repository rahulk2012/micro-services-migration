var API_BASE_URL = "http://localhost:8080/gateway/api/v1"
  var eventId = "";
  var vendorId = "";
  
  var questionType = 1;
  var parentId = 0;
  var startLimit = 0;
  var endLimit = 10;




function getQuestions(parentId ){
  $(".choice .emoji").removeClass("emoji-selected");
  $("#choice-"+parentId+" .emoji").addClass("emoji-selected");
  $("#selectParentId").val(parentId);
  console.log(parentId);
  questionType = 0;
  startLimit = 0;
  endLimit = 100;
  callMainQuestion(vendorId, eventId, questionType, parentId, startLimit, endLimit);
}

function callMainQuestion(vendorId='', eventId='', questionType, parentId, startLimit, endLimit) {
	

  var data = "?questionType="+questionType+"&parentId="+parentId+"&startLimit="+startLimit+"&endLimit="+endLimit+"";

	$.ajax({
		url : API_BASE_URL+"/feedback/get-question/"+vendorId+"/"+eventId+""+data,
		crossDomain: true,
	    contentType: "application/json",
	    dataType: 'json',
		cache : false,
		timeout : 600000,
		success : function(jsonContent) {
			if(jsonContent!=""){
            	//console.log(JSON.stringify(jsonContent));
              
              if(questionType==1){
                $("#emojiQuestion").html(jsonContent.questionList[0].question);
                var htmlLi = "";
                var ra = 5;
                for(i=0; i<jsonContent.questionList[0].questionContentDTO.length; i++){
                  if(i==0){
                    htmlLi = htmlLi + "<li class=\"choice\" data-value=\""+ra+"\" id=\"choice-"+jsonContent.questionList[0].questionContentDTO[i].questionContentId+"\" onClick=\"getQuestions("+jsonContent.questionList[0].questionContentDTO[i].questionContentId+");\"><p class=\"emoji emoji-selected\"><i class=\""+jsonContent.questionList[0].questionContentDTO[i].emojiClass+"\"></i></p><p class=\"reaction\">"+jsonContent.questionList[0].questionContentDTO[i].questionLable+"</p></li>";
                  }else{
                    htmlLi = htmlLi + "<li class=\"choice\" data-value=\""+ra+"\" id=\"choice-"+jsonContent.questionList[0].questionContentDTO[i].questionContentId+"\" onClick=\"getQuestions("+jsonContent.questionList[0].questionContentDTO[i].questionContentId+");\"><p class=\"emoji\"><i class=\""+jsonContent.questionList[0].questionContentDTO[i].emojiClass+"\"></i></p><p class=\"reaction\">"+jsonContent.questionList[0].questionContentDTO[i].questionLable+"</p></li>";
                  }
                  ra = ra - 1;
                }
                $(".emoji-choices").html(htmlLi);
                getQuestions(jsonContent.questionList[0].questionContentDTO[0].questionContentId);
              }else{
                $(".textarea").html('');
                $(".radio-buttons").html('');
                $(".checkbox").html('');
                if(jsonContent.questionList==null){
                  $(".textarea").html('No Question available');
                }else{
                    for(i=0; i<jsonContent.questionList.length; i++){
                    var htmlQuest = "";
                    var htmlQuestAns = "";
                    if(jsonContent.questionList[i].elementName.toUpperCase()=='TEXTAREA'){
                      htmlQuest = "";
                      htmlQuest = htmlQuest + " <div class=\"question\" id=\""+jsonContent.questionList[i].questionId+"-"+jsonContent.questionList[i].elementName.toUpperCase()+"\"><div class=\"row\"><div class=\"col-12\"><p><strong>"+jsonContent.questionList[i].question+"</strong></p></div> </div></div>";
                      htmlQuest = htmlQuest + " <div class=\"row\"><div class=\"col-12\"><textarea rows=\"3\" cols=\"\" class=\"form-control\" name=\"answer-"+jsonContent.questionList[i].questionId+"\"></textarea></div></div><hr />";
                      $(".textarea").append(htmlQuest);
                    }
                    //console.log(jsonContent.questionList[i].elementName.toUpperCase());
                    if(jsonContent.questionList[i].elementName.toUpperCase()=='RADIO'){
                      
                      htmlQuest = "";
                      htmlQuestAns = "";
                      htmlQuest = htmlQuest + " <div class=\"question\" id=\""+jsonContent.questionList[i].questionId+"-"+jsonContent.questionList[i].elementName.toUpperCase()+"\"><div class=\"row\"><div class=\"col-12\"><p><strong>"+jsonContent.questionList[i].question+"</strong></p></div></div></div>";
                      htmlQuest = htmlQuest + "<div class=\"row\"><div class=\"col-12\">";
                      for(r=0; r<jsonContent.questionList[i].questionContentDTO.length; r++){
                        htmlQuestAns = htmlQuestAns + " <div class=\"radio\"><input id=\"radio-"+jsonContent.questionList[i].questionContentDTO[r].questionContentId+"\" name=\"radio-answer-"+jsonContent.questionList[i].questionId+"\" value=\""+jsonContent.questionList[i].questionContentDTO[r].questionContentId+"\" type=\"radio\" ><label for=\"radio-"+jsonContent.questionList[i].questionContentDTO[r].questionContentId+"\" class=\"radio-label\">"+jsonContent.questionList[i].questionContentDTO[r].questionLable+"</label></div>";
                      }
                      htmlQuest = htmlQuest + htmlQuestAns + "</div></div><hr />";
                      $(".radio-buttons").append(htmlQuest);
                    }
                    if(jsonContent.questionList[i].elementName.toUpperCase()=='CHECKBOX'){
                      htmlQuest = "";
                      htmlQuestAns = "";
                      htmlQuest = htmlQuest + " <div class=\"question\" id=\""+jsonContent.questionList[i].questionId+"-"+jsonContent.questionList[i].elementName.toUpperCase()+"\"><div class=\"row\"><div class=\"col-12\"><p><strong>"+jsonContent.questionList[i].question+"</strong></p></div></div></div>";
                      htmlQuest = htmlQuest + " <div class=\"row\"><div class=\"col-12\"><div class=\"checkbox-block\"> ";
                      for(r=0; r<jsonContent.questionList[i].questionContentDTO.length; r++){
                        htmlQuestAns = htmlQuestAns + " <input class=\"checkbox-effect checkbox-effect-6\" id=\"get-up-"+jsonContent.questionList[i].questionContentDTO[r].questionContentId+"\" type=\"checkbox\" value=\""+jsonContent.questionList[i].questionContentDTO[r].questionContentId+"\" name=\"checkbox-answer-"+jsonContent.questionList[i].questionId+"\"/><label for=\"get-up-"+jsonContent.questionList[i].questionContentDTO[r].questionContentId+"\">"+jsonContent.questionList[i].questionContentDTO[r].questionLable+"</label>";
                      }
                      htmlQuest = htmlQuest + htmlQuestAns +  " </div></div></div><hr /> ";
                      $(".checkbox").append(htmlQuest);
                    }
                  }
                }
                
              }
			}
		},
		error : function(e) {
			//showMessage(true, e.responseText);
            console.log(e.responseText);
			return false;
		}
	});
	
}	


$("#loginform").on("click",function(){
  var answersRequest = {};
  var userId='';
  var answers = [];
  var ansW = {};
  var questionId = '';
  var answer='';
  var questionNameId = '';
  var elementId = '';
  
  answersRequest['eventType']= eventId;
  answersRequest['emoji']= $("#selectParentId").val();
  answersRequest['userId']= $("#userName").val();
  $( ".question" ).each(function( index ) {
    ansW = {};
    questionId = $(this).attr('id').split("-");
    if(questionId.length>1){
      questionNameId = questionId[0];
      elementId = questionId[1];
    }else{
      questionNameId = $(this).attr('id');
    }
    if(elementId =='RADIO'){
      ansW ['questionId'] = questionNameId;
      ansW ['answer'] = $("input[name='radio-answer-"+questionNameId+"']:checked"). val();
    }
    if(elementId =='CHECKBOX'){
      ansW ['questionId'] = questionNameId;
        var favorite = '';
        $.each($("input[name='checkbox-answer-"+questionNameId+"']:checked"), function(){
          favorite = favorite + $(this).val() +',';
        });
      ansW ['answer'] = favorite;
    }
    if(elementId =='TEXTAREA'){
      ansW ['questionId'] = questionNameId;
      ansW ['answer'] = $("textarea[name='answer-"+questionNameId+"']").val();
    }
    answers.push(ansW);

  });

  answersRequest['answers']=answers;
  console.log("answersRequest=> ",JSON.stringify(answersRequest));

  $.ajax({
		type : "POST",
		url : API_BASE_URL+"/feedback/submit-answers",
		contentType: "application/json",
		data : JSON.stringify(answersRequest),
		crossDomain: true,
		dataType: 'json',
		cache : false,
		timeout : 600000,
		success : function(response) {
			console.log(JSON.stringify(response));
		      if(response.responseStatus.status=='SUCCESS'){
		    	  
		    	  var htmlPage = "<section class=\"head\"><div class=\"row\"><div class=\"col-6\"><div class=\"logo\"> <img src=\"../images/logo.png\" class=\"img-fluid\" /> </div>";
		          htmlPage = htmlPage + " </div> </div></section><hr style=\"margin-bottom:0;\"/>";
		          htmlPage = htmlPage + " <section class=\"center-content\"> <div class=\"row\"><div class=\"col-12 text-center\"><p class=\"thanku-emoji\"><i class=\"em em-thumbsup\"></i></p>";
		          htmlPage = htmlPage + "   <p class=\"thanks-reaction\">"+response.responseStatus.message+"!</p></div> </div></section> ";
				    	  
		          $(".main-div").html(htmlPage);
		      }
		},
		error : function(e) {
      console.log(e.responseText);
		}
	});


});
