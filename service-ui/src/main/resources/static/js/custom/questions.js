$( document ).ready(function() {
	//callForGetElementMaster();
});

function validateRequestForVendor(formId){
	return true;
}

function callForVendors(formId) {
	if(!validateRequestForVendor(formId)){
		return false;
	}
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/questions"),
		data : JSON.stringify(getRequestForVendor(formId)),
		dataType : 'html',
		success : function(htmlContent) {
			if(htmlContent!=""){
				$(".app-main__inner").html(htmlContent);
			}
		}
	});
}

function getRequestForVendor(formId){
	var vendorRequest = {};
	vendorRequest['vendorId'] = $("#"+formId+" #vendor").val();
	vendorRequest['startLimit'] = '';
	vendorRequest['endLimit'] = '';
	return vendorRequest;
}

function validateRequestForGetQuestion(formId){
	return true;
}

function callForGetQuestion(formId, eventId) {
	if(!validateRequestForGetQuestion(formId)){
		return false;
	}
	var vendorId = $("#"+formId+" #vendor").val();
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/get-question-list"),
	    contentType: "application/json",
		data : JSON.stringify(getRequestForGetQuestion(formId, eventId, vendorId)),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(jsonContent) {
			if(jsonContent!=""){
				var htlContent ="<option value=\"0\">--Select--</option>";
				var quest = jsonContent.questionList;
				if(quest!=null){
					var parentId = 0;
					for (var i = 0; i < quest.length; i++) {
						if(quest[i].questionType==1){
							parentId = quest[i].questionId;
							htlContent = htlContent + " <option value=\""+quest[i].questionId+"\">"+quest[i].question+"</option>";
						}
						if(parentId == quest[i].parentId){
							htlContent = htlContent + " <option value=\""+quest[i].questionId+"\">"+quest[i].question+"</option>";
						}
						
					}
					$("#parentQuestion").html(htlContent);
				}
				
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForGetQuestion(formId, eventId, vendorId){

	var questionRequest = {};
	questionRequest['vendorId'] = vendorId;
	questionRequest['eventId'] = eventId;
	questionRequest['startLimit'] = '';
	questionRequest['endLimit'] = '';
	
	return questionRequest;
}

function validateRequestForGetEventMaster(formId){
	return true;
}

function callForGetEventMaster(formId, vendorId) {
	if(!validateRequestForGetEventMaster(formId)){
		return false;
	}
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/get-event-list"),
	    contentType: "application/json",
		data : JSON.stringify(getRequestForEventMaster(formId, vendorId)),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(jsonContent) {
			if(jsonContent!=""){
				console.log(JSON.stringify(jsonContent));
				var htlContent ="<option value=\"0\">--Select--</option>";
				var quest = jsonContent.eventTypeList;
				if(quest!=null){
					for (var i = 0; i < quest.length; i++) {
						htlContent = htlContent + " <option value=\""+quest[i].eventId+"\">"+quest[i].eventName+"</option>";
					}
					$("#questionEventType").html(htlContent);
				}
				callForGetElementMaster();
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForEventMaster(formId, vendorId){
	var questionRequest = {};
	questionRequest['vendorId'] = vendorId;
	questionRequest['startLimit'] = '';
	questionRequest['endLimit'] = '';
	return questionRequest;
}

function validateRequestForGetElementMaster(formId){
	return true;
}

function callForGetElementMaster() {
	if(!validateRequestForGetElementMaster()){
		return false;
	}
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/get-element-list"),
	    contentType: "application/json",
		data : JSON.stringify(getRequestForElementMaster()),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(jsonContent) {
			if(jsonContent!=""){
				console.log(JSON.stringify(jsonContent));
				var htlContent ="<option value=\"0\">--Select--</option>";
				var quest = jsonContent.elementTypeList;
				if(quest!=null){
					for (var i = 0; i < quest.length; i++) {
						htlContent = htlContent + " <option value=\""+quest[i].elementId+"\">"+quest[i].elementName+"</option>";
					}
					$("#questionElementType").html(htlContent);
				}
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForElementMaster(){
	var elementTypeRequest = {};
	return elementTypeRequest;
}

//	$("input[type=submit]").click(function(e) {
//	  e.preventDefault();
//	  $(this).next("[name=textbox]")
//	  .val(
//	    $.map($(".inc :text"), function(el) {
//	      return el.value
//	    }).join(",\n")
//	  )
//	})

function validateRequestForSubmitQuestion(formId){
	return true;
}

function callForGetSubmitQuestion(formId) {
	if(!validateRequestForSubmitQuestion(formId)){
		return false;
	}
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/submit-question"),
	    contentType: "application/json",
		data : JSON.stringify(getRequestForSubmitQuestion(formId)),
		dataType : 'json',
		cache : false,
		timeout : 600000,
		success : function(jsonContent) {
			if(jsonContent!=""){
				if(jsonContent.status=='SUCCESS'){
					alert(jsonContent.message); 
					callForVendors('questionForm');
				}
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForSubmitQuestion(formId){
	var questionSubmitRequest = {};
	var questionContentDTO = [];
	var questionContent = {};
	questionSubmitRequest['questionId'] = $("#"+formId+" #questionId").val();
	questionSubmitRequest['vendorId'] = $("#"+formId+" #vendor").val();
	questionSubmitRequest['eventId'] = $("#"+formId+" #questionEventType").val();
	questionSubmitRequest['questionType'] = $("#"+formId+" #questionType").val();
	questionSubmitRequest['question'] = $("#"+formId+" #question").val();
	questionSubmitRequest['elementId'] = $("#"+formId+" #questionElementType").val();
	questionSubmitRequest['mendetory'] = $("#"+formId+" #mandatory").val();
	questionSubmitRequest['parentId'] = $("#"+formId+" #parentQuestion").val();
	
	var test_arr = $("input[name*='answerLabel']");
	$.each(test_arr, function(i, item) {  //i=index, item=element in array
	   // alert($(item).val());
	    questionContent['questionLable'] = $(item).val();
		questionContent['rightAnswer'] = '0';
		questionContentDTO.push(questionContent);
	});
		
	questionSubmitRequest['questionContentDTO'] = questionContentDTO;		
	console.log("questionSubmitRequest=> ",questionSubmitRequest);
	return questionSubmitRequest;
}


function validateRequestForSendFeedback(formId){
	return true;
}

function callForSendFeedback(formId) {
	if(!validateRequestForSendFeedback(formId)){
		return false;
	}
	
	$.ajax({
		type : "POST",
		url : getURLFor("/dashboard/feedback-send-report"),
	    contentType: "application/json",
		data : JSON.stringify(getRequestForSendFeedback(formId)),
		dataType : 'html',
		cache : false,
		timeout : 600000,
		success : function(htmlContent) {
			if(htmlContent!=""){
				$(".app-main__inner").html(htmlContent);
			}
		},
		error : function(e) {
			console.log(true, e.responseText);
		}
	});
}

function getRequestForSendFeedback(formId){
	var vendorRequest = {};
	
	return vendorRequest;
}
