/**
 * Copyright (c) 2003-2019, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or https://ckeditor.com/legal/ckeditor-oss-license
 */

/* exported initSample */
if ( CKEDITOR.env.ie && CKEDITOR.env.version < 9 ){
	CKEDITOR.tools.enableHtml5Elements( document );
}

// The trick to keep the editor in the sample quite small
// unless user specified own height.
CKEDITOR.config.height = 'auto';
CKEDITOR.config.width = 'auto';

var initEditor = ( function() {
	var wysiwygareaAvailable = isWysiwygareaAvailable(), isBBCodeBuiltIn = !!CKEDITOR.plugins.get( 'bbcode' );

	return function(index, elementId, placeholder, needToFlush) {
		console.log(index+' = '+ elementId+' = '+placeholder+' = '+needToFlush+' = '+CKEDITOR.instances[elementId]);
		if(CKEDITOR.instances[elementId]){
			if($('#'+elementId).html('')!=''){
				//CKEDITOR.instances[elementId].setData('');
			}
			CKEDITOR.instances[elementId].destroy();	
		}
		var editor = CKEDITOR.document.getById( elementId );
		console.log('isBBCodeBuiltIn '+isBBCodeBuiltIn);
		if ( isBBCodeBuiltIn) {
			CKEDITOR.instances[elementId].setData('');
		}
		// Depending on the wysiwygarea plugin availability initialize classic or inline editor.
		if ( wysiwygareaAvailable ) {
			CKEDITOR.replace( elementId );
		} else {
			editor.setAttribute( 'contenteditable', 'true' );
			CKEDITOR.inline( elementId );
			// TODO we can consider displaying some info box that
			// without wysiwygarea the classic editor may not work.
		}
	};

	function isWysiwygareaAvailable() {
		// If in development mode, then the wysiwygarea must be available.
		// Split REV into two strings so builder does not replace it :D.
		if ( CKEDITOR.revision == ( '%RE' + 'V%' ) ) {
			return true;
		}

		return !!CKEDITOR.plugins.get( 'wysiwygarea' );
	}
} )();

function editorContent(elementId, elementContent){
	if(CKEDITOR.instances[elementId]){
		if(elementContent==undefined){
			return CKEDITOR.instances[elementId].getData();
		}else{
			CKEDITOR.instances[elementId].setData(elementContent);
		}
	}
	return '';
}

//function initEditor(index, elementId, placeholder, needToFlush){
//	console.log(elementId+' = '+placeholder)
//	if(index==1 && editor1){
//		editor1.destroy();
//	}
//	if(index==2 && editor2){
//		editor2.destroy();
//	}
//	if(index==3 && editor3){
//		editor3.destroy();
//	}
//	if(index==4 && editor4){
//		editor4.destroy();
//	}
//	if(needToFlush){
//		$('#'+elementId).html('');
//	}
//	ClassicEditor
//		.create( document.querySelector( '#'+elementId ), {
//			 placeholder: placeholder,
//			toolbar: {
//					items: [
//						'heading',
//						'|',
//						'bold',
//						'italic',
//						'link',
//						'bulletedList',
//						'numberedList',
//						'|',
//						'indent',
//						'outdent',
//						'|',
//						'imageUpload',
//						'blockQuote',
//						'insertTable',
//						'mediaEmbed',
//						'undo',
//						'redo',
//						'alignment',
//						'codeBlock',
//						'comment',
//						'fontColor',
//						'fontBackgroundColor',
//						'fontSize',
//						'fontFamily',
//						'highlight',
//						'horizontalLine',
//						'pageBreak',
//						'removeFormat',
//						'strikethrough',
//						'superscript',
//						'subscript',
//						'trackChanges',
//						'underline'
//					]
//				},
//                language: 'en-gb',
//				image: {
//					toolbar: [
//						'imageTextAlternative',
//						'imageStyle:full',
//						'imageStyle:side'
//					]
//				},
//				table: {
//					contentToolbar: [
//						'tableColumn',
//						'tableRow',
//						'mergeTableCells'
//					]
//				},
//				licenseKey: '',
//				sidebar: {
//				container: document.querySelector( '.sidebar' )
//			},
//                
//		} )
//		.then( editor => {
//			window.editor = editor;
//		    if(index==1){
//		    	editor1=editor;
//			}else if(index==2){
//		    	editor2=editor;
//			}else if(index==3){
//				editor3=editor;
//			}else if(index==4){
//		    	editor4=editor;
//			}
//		} )
//		.catch( err => {
//			console.error( err.stack );
//		} );
//}