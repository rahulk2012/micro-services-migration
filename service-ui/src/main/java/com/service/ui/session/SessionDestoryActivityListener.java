package com.service.ui.session;

import javax.servlet.http.HttpSession;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import lombok.extern.slf4j.Slf4j;

@Configuration
@Slf4j
public class SessionDestoryActivityListener {
	
	
	@Bean // bean for http session listener
	public HttpSessionListener httpSessionListener() {
		return new HttpSessionListener() {
			@Override
			public void sessionCreated(final HttpSessionEvent event) {
				log.info("Session Created with session id+" + event.getSession().getId());
			}

			@Override
			public void sessionDestroyed(final HttpSessionEvent event) {
				log.info("SessionMailListener.java LNo : sessionDestroyed()");
				try {
					HttpSession session = event.getSession();
					log.info("SessionMailListener.java LNo :: session :: " + session);
					if (session != null) {
						if (session.getAttribute("uniqueId") != null) {
							String uniqueId = (String) session.getAttribute("uniqueId");
							String token = (String) session.getAttribute("token");
							if (uniqueId != null) {
								log.info("SessonDestory " + uniqueId);
							}
						}
					}
				} catch (final Exception e) {
					log.info("SessionMailListener.java LNo : 139 : Exception Caught", e);
				}
			}
		};
	}

}