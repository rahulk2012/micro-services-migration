package com.service.ui.session;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
//@Slf4j
public class UiUtil {

	@Autowired
	private SessionUtil sessionUtil;

	public HttpHeaders getHeader() {
		HttpHeaders headers = new HttpHeaders();
		headers.set("Content-Type", "application/json");
		String token = sessionUtil.getToken();
		if (token!=null) {
			headers.set("Authorization", "Bearer " + sessionUtil.getToken());
			headers.set("encryptedUserId", sessionUtil.getEncryptedUserId());
		}
		return headers;
	}
}