package com.service.ui.session;

import java.util.UUID;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

@Component
//@Slf4j
public class SessionUtil {


	public static final String MESSAGE = "message";
	public static final String CAPTCHA = "captcha";
	public static final String USER = "userMaster";
	public static final String USER_ID = "userMasterId";
	public static final String UNIQUE_USER_SESSION = "uniqueUserSession";
	public static final String USER_ROLE_ID = "userRoleId";
	public static final String STORE_USER_ID = "storeUserId";
	public static final String UNIQUE_ID = "randomUniqueId";
	public static final String SEARCH_CARD_RESPONSE = "searchCardResponse";
	public static final String TOKEN = "token";
	public static final String ENCRYPTED_USER_ID = "encryptedUserId";

	public HttpSession getSession() {
		final ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
		return attr.getRequest().getSession(true);
	}

	public void setStoreUserId(final Integer storeUserId) {
		getSession().setAttribute(STORE_USER_ID, storeUserId);
	}

	public Integer getStoreUserId() {
		if (getSession().getAttribute(STORE_USER_ID) != null) {
			return (Integer) getSession().getAttribute(STORE_USER_ID);
		}
		return null;
	}

	public void setCaptcha(final String captcha) {
		getSession().setAttribute(CAPTCHA, captcha);
	}

	public String getCaptcha() {
		if (getSession().getAttribute(CAPTCHA) != null) {
			return (String) getSession().getAttribute(CAPTCHA);
		}
		return null;
	}

	public String getMessage() {
		if (getSession().getAttribute(MESSAGE) != null) {
			return (String) getSession().getAttribute(MESSAGE);
		}
		return null;
	}

	public void setMessage(final String message) {
		getSession().setAttribute(MESSAGE, message);
	}
	

	public void setRandomUniqueId(final UUID uniqueId) {
		getSession().setAttribute(UNIQUE_ID, uniqueId);
	}

	public UUID getRandomUniqueId() {
		if (getSession().getAttribute(UNIQUE_ID) != null) {
			return (UUID) getSession().getAttribute(UNIQUE_ID);
		}
		return null;
	}

	public void setToken(final String token) {
		getSession().setAttribute(TOKEN, token);
	}

	public String getToken() {
		if (getSession().getAttribute(TOKEN) != null) {
			return (String) getSession().getAttribute(TOKEN);
		}
		return null;
	}
	
	public void setEncryptedUserId(final String uniqueId) {
		getSession().setAttribute(ENCRYPTED_USER_ID, uniqueId);
	}

	public String getEncryptedUserId() {
		if (getSession().getAttribute(ENCRYPTED_USER_ID) != null) {
			return (String) getSession().getAttribute(ENCRYPTED_USER_ID);
		}
		return null;
	}

	
}
