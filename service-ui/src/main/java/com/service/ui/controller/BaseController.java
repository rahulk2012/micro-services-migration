package com.service.ui.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;

import com.service.ui.session.SessionUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class BaseController {
	@Autowired
	SessionUtil sessionUtil;
	
	public Model updateModel(Model model) {
		String encryptedUserId = sessionUtil.getEncryptedUserId();
		log.info("questions getEncryptedUserId " + encryptedUserId);
		model.addAttribute("encryptedUserId", encryptedUserId);
		String token = sessionUtil.getToken();
		log.info("questions token " + token);
		model.addAttribute("token", token);
		return model;
	}
	
}
