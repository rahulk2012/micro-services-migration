package com.service.ui.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.service.common.dto.ResponseStatus;
import com.service.constant.ApiConstant;
import com.service.dto.request.CronRequest;
import com.service.dto.request.ElementTypeRequest;
import com.service.dto.request.EventTypeRequest;
import com.service.dto.request.QuestionListRequest;
import com.service.dto.request.QuestionSubmitRequest;
import com.service.dto.request.TemplateSaveRequest;
import com.service.dto.request.VendorRequest;
import com.service.dto.response.CronsListDTO;
import com.service.dto.response.CronsResponse;
import com.service.dto.response.ElementTypeResponse;
import com.service.dto.response.EventTypeResponse;
import com.service.dto.response.QuestionListResponse;
import com.service.dto.response.TemplateResponse;
import com.service.dto.response.VendorResponse;
import com.service.dto.response.common.VendorsAndEventsResponse;
import com.service.ui.session.SessionUtil;
import com.service.ui.session.UiUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class DashboardController extends BaseController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	protected SessionUtil sessionUtil;

	@Autowired
	protected UiUtil uiUtil;

	@RequestMapping(value = "/", method = { RequestMethod.GET })
	public String login() {
		sessionUtil.setToken(null);
		sessionUtil.setEncryptedUserId(null);
		return "login";
	}

	@RequestMapping(value = "/dashboard/admin", method = { RequestMethod.GET, RequestMethod.POST })
	public String dashboard(Model model, HttpSession session, HttpServletRequest request) {
		String encryptedUserId = sessionUtil.getEncryptedUserId();
		if (encryptedUserId == null) {
			return "redirect:/";
		}
		updateModel(model);
		return "dashboard";
	}

	@RequestMapping(value = "/dashboard/questions", method = { RequestMethod.GET, RequestMethod.POST })
	public String questions(Model model) {
		String encryptedUserId = sessionUtil.getEncryptedUserId();
		if (encryptedUserId == null) {
			return "redirect:/";
		}
		updateModel(model);
		
		VendorRequest vendorRequest = VendorRequest.builder().vendorId(encryptedUserId).startLimit(0).endLimit(10).build();
		HttpEntity<VendorRequest> request = new HttpEntity<>(vendorRequest, uiUtil.getHeader());

		ResponseEntity<VendorResponse> responseVendor = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue() + "/get-vendor", request, VendorResponse.class);
		model.addAttribute("vendors", responseVendor.getBody());
		return "questionForm";
	}

	@RequestMapping("/dashboard/get-question-list")
	public ResponseEntity<QuestionListResponse> getQuestions(@RequestBody QuestionListRequest questionRequest) {
		HttpEntity<QuestionListRequest> request = new HttpEntity<>(questionRequest, uiUtil.getHeader());
		ResponseEntity<QuestionListResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue() + "/get-question-list", request,
				QuestionListResponse.class);
		return response;
	}

	@RequestMapping("/dashboard/get-event-list")
	public ResponseEntity<EventTypeResponse> getEventList(@RequestBody EventTypeRequest eventTypeRequest) {
		HttpEntity<EventTypeRequest> request = new HttpEntity<>(eventTypeRequest, uiUtil.getHeader());
		ResponseEntity<EventTypeResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue() + "/get-events", request, EventTypeResponse.class);
		return response;
	}

	@RequestMapping("/dashboard/get-element-list")
	public ResponseEntity<ElementTypeResponse> getElementList(@RequestBody ElementTypeRequest elementTypeRequest) {
		HttpEntity<ElementTypeRequest> request = new HttpEntity<>(elementTypeRequest, uiUtil.getHeader());
		ResponseEntity<ElementTypeResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue() + "/get-elements", request,
				ElementTypeResponse.class);
		return response;
	}

	@RequestMapping(value = "/dashboard/submit-question", method = { RequestMethod.GET, RequestMethod.POST })
	public ResponseEntity<ResponseStatus> submitQuestion(@RequestBody QuestionSubmitRequest questionSubmitRequest) {
		HttpEntity<QuestionSubmitRequest> request = new HttpEntity<>(questionSubmitRequest, uiUtil.getHeader());
		ResponseEntity<ResponseStatus> response = restTemplate.postForEntity(
				ApiConstant.URL_FEEDBACK_SURVEY_SERVICE.getValue() + "/submit-question", request, ResponseStatus.class);
		return response;
	}

	@RequestMapping(value = "/dashboard/mailtemplate", method = { RequestMethod.GET, RequestMethod.POST })
	public String mailTemplate(Model model) {
		String encryptedUserId = sessionUtil.getEncryptedUserId();
		if (encryptedUserId == null) {
			return "redirect:/";
		}
		updateModel(model);
		return "mailTemplateForm";
	}

	@RequestMapping(value = "/dashboard/get-vendor-events/{vendorId}", method = { RequestMethod.GET })
	public ResponseEntity<VendorsAndEventsResponse> getVendor(@PathVariable("vendorId") String vendorId) {
		HttpEntity<String> entity = new HttpEntity<>("body", uiUtil.getHeader());
		ResponseEntity<VendorsAndEventsResponse> response = restTemplate.exchange(
				ApiConstant.URL_API_SERVICE.getValue() + "/common/get-vendor-events/" + vendorId, HttpMethod.GET, entity,
				VendorsAndEventsResponse.class);
		return response;
	}

	@RequestMapping(value = "/dashboard/get-template/{vendorId}/{eventId}/{templateType}", method = {RequestMethod.GET })
	public ResponseEntity<TemplateResponse> getTemplateByVendorAndEventAndTemplateType(
			@PathVariable("vendorId") String vendorId, @PathVariable("eventId") String eventId,
			@PathVariable("templateType") Integer templateType) {
		HttpEntity<String> entity = new HttpEntity<>("body", uiUtil.getHeader());
		ResponseEntity<TemplateResponse> response = restTemplate.exchange(
				ApiConstant.URL_API_SERVICE.getValue() + "/communication/get-template/" + vendorId + "/" + eventId + "/" + templateType, HttpMethod.POST, entity,
				TemplateResponse.class);
		return response;
	}

	@RequestMapping(value = "/dashboard/save-communication-template", method = {RequestMethod.POST })
	public ResponseEntity<TemplateResponse> getSaveCommunicationTemplate(@RequestBody TemplateSaveRequest templateSaveRequest) {
		HttpEntity<TemplateSaveRequest> request = new HttpEntity<>(templateSaveRequest, uiUtil.getHeader());
		ResponseEntity<TemplateResponse> response = restTemplate.exchange(
				ApiConstant.URL_API_SERVICE.getValue() + "/communication/save-communication-template", HttpMethod.POST, request,
				TemplateResponse.class);
		return response;
	}
	
	
	@RequestMapping(value = "/dashboard/crons-listing", method = { RequestMethod.GET, RequestMethod.POST })
	public String getCronsListing(Model model, @RequestBody CronRequest cronRequest) {
		HttpEntity<CronRequest> request = new HttpEntity<>(cronRequest, uiUtil.getHeader());
		ResponseEntity<CronsResponse> response = restTemplate.postForEntity(ApiConstant.URL_API_SERVICE.getValue() + "/communication/get-cron-list", request,
				CronsResponse.class);
		CronsResponse cronsResponse= response.getBody();
		log.info("cronsResponse"+cronsResponse);
		
		List<CronsListDTO> cronsListDTO=null;
		ResponseStatus responseStatus = cronsResponse.getResponseStatus();
		if(responseStatus.getStatus().equalsIgnoreCase(ApiConstant.STATUS_MSG_SUCCESS.getValue())) {
			cronsListDTO =cronsResponse.getCronsListDTO();
		}
		model.addAttribute("cronsListDto",cronsListDTO);
		updateModel(model);
		return "cronsListingForm";
	}
}