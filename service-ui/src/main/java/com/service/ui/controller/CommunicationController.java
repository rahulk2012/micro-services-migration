package com.service.ui.controller;

import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.service.constant.ApiConstant;
import com.service.dto.request.UpdateFeedbackDetailsRequest;
import com.service.dto.response.FeedbackDetailResponse;
import com.service.dto.response.UpdateFeedbackDetailResponse;
import com.service.ui.session.SessionUtil;
import com.service.ui.session.UiUtil;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class CommunicationController {

	@Autowired
	protected SessionUtil sessionUtil;
	
	@Autowired
	private RestTemplate restTemplate;
	
	@Autowired
	protected UiUtil uiUtil;

	@RequestMapping(value = "/dashboard/template", method = { RequestMethod.GET })
	public String template() {
		return "template";
	}
	
	@RequestMapping(value = "/feedback/{feedbackId}", method = { RequestMethod.GET, RequestMethod.POST })
	public String feedback(Model model, @PathVariable("feedbackId") String feedbackId) {
		
		String encryptedUserId = sessionUtil.getEncryptedUserId();
		log.info("questions getEncryptedUserId " + encryptedUserId);
		model.addAttribute("encryptedUserId", encryptedUserId);
		model.addAttribute("feedbackId", feedbackId);
		
		HttpEntity<String> entity = new HttpEntity<>("body", uiUtil.getHeader());
		
		UpdateFeedbackDetailsRequest updateFeedbackRequest = UpdateFeedbackDetailsRequest.builder()
				.key("isClicked")
				.value(feedbackId)
				.date((new Date()).toString())
				.build();
		
		ResponseEntity<UpdateFeedbackDetailResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_API_SERVICE.getValue() + "/feedback/update-feedback-details", updateFeedbackRequest,
				UpdateFeedbackDetailResponse.class);
		
		ResponseEntity<FeedbackDetailResponse> response1 = restTemplate.exchange(
				ApiConstant.URL_API_SERVICE.getValue() + "/feedback/get-feedback-detail/" + feedbackId, HttpMethod.GET, entity,
				FeedbackDetailResponse.class);
		model.addAttribute("feedbackDetail", response1.getBody());
		return "feedback";
	}

	@RequestMapping(value = "/dashboard/feedback-send-report", method = { RequestMethod.GET, RequestMethod.POST })
	public String feedbackReport() {
		return "feedback-send-report";
	}
	
}
