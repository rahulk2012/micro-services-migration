package com.service.ui.controller;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.client.RestTemplate;

import com.service.constant.ApiConstant;
import com.service.ui.session.SessionUtil;
import com.service.ui.session.UiUtil;
import com.servicer.dto.use.LoginRequest;
import com.servicer.dto.use.LoginResponse;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Controller
public class LoginController {

	@Autowired
	private RestTemplate restTemplate;

	@Autowired
	protected SessionUtil sessionUtil;

	@Autowired
	protected UiUtil uiUtil;

	@RequestMapping(value = "/index")
	public String index() {
		return "index";
	}

	@RequestMapping(value = "/login", method = { RequestMethod.POST })
	public ResponseEntity<LoginResponse> login(@RequestBody LoginRequest loginRequest) {

		HttpEntity<LoginRequest> request = new HttpEntity<>(loginRequest, uiUtil.getHeader());

		ResponseEntity<LoginResponse> response = restTemplate.postForEntity(
				ApiConstant.URL_API_SERVICE.getValue() + "/authenticate", request, LoginResponse.class);
		LoginResponse res = response.getBody();
		if (ApiConstant.STATUS_MSG_SUCCESS.getValue().equalsIgnoreCase(res.getStatus())) {
			sessionUtil.setToken(res.getToken());
			sessionUtil.setEncryptedUserId(res.getEncryptedUserId());
		}
		return response;
	}

	@GetMapping("/logout")
	public String logout(Model model, HttpSession session) {
		// MAKE ENTRY IN LOGIN HISTORY
		try {
			sessionUtil.setToken(null);
			sessionUtil.setEncryptedUserId(null);
//			@TODO THE ABOVE CODE HANDLED BY SESSIONEVENT LISTNER
			session.invalidate();
		} catch (Exception e) {
			log.info("exception caught while trying to logout :: ", e);
		}
		return "redirect:/";
	}
}
